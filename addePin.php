<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$asd = $userDetails->getEpin();

// $userEPin = getuser($conn," WHERE uid = ? ",array("epin"),array($_SESSION['uid']),"s");
// $userEPinUpdate = $userEPin[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/editPassword.php" />
    <meta property="og:title" content="Add ePin | DCK Supreme" />
    <title>Add ePin | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck,  engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/editPassword.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <!-- <form class="edit-profile-div2" action="utilities/changePasswordFunction.php" method="POST"> -->
    <!-- <form class="edit-profile-div2" onsubmit="return createAccountValidation();" action="utilities/addePinFunction.php" method="POST"> -->
    <form class="edit-profile-div2" onsubmit="return ePinValidation();" action="utilities/addePinFunction.php" method="POST">    
    <!-- <h1 class="username"><?php //echo $userDetails->getUsername();?></h1> -->

    <div class="left-div">
        <p class="continue-shopping pointer continue2"><a href="profile.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back" > Back To Profile</a></p>
    </div>


        <h2 class="profile-title">E-Pin</h2>
        
        <?php
        if($asd == "")
        {
        ?>
            <table class="edit-profile-table password-table">
                <tr class="profile-tr">
                    <td class="profile-td1">Add New E-Pin</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input required name="register_epin" id="register_epin" class="clean edit-profile-input" type="password">
                        <span class="visible-span2">
                            <img src="img/visible.png" class="login-input-icon" alt="View E-Pin" title="View E-Pin" id="editPassword_new_img">
                        </span>
                    </td>
                </tr>            
                <tr class="profile-tr">
                    <td class="profile-td1">Retype E-Pin</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input required name="register_epin_reenter" id="register_epin_reenter" class="clean edit-profile-input"type="password">
                        <span class="visible-span2">
                            <img src="img/visible.png" class="login-input-icon" alt="View E-Pin" title="View E-Pin" id="editPassword_reenter_img">
                        </span>
                    </td>
                </tr>      
            </table>

            <button class="confirm-btn text-center white-text clean black-button">Confirm</button>

        <?php
        }
        else
        { ?>
           <h3>E-Pin Added Successfully!</h3>
           <p><br></p>
           <a href="editePin.php" class="confirm-btn text-center white-text clean black-button">Edit E-Pin</a>
           

        <?php
        }
        ?>


            


    </form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Your current password does not match! <br>Please try again.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Your new password must be more than 5 characters";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Password does not match with retype password. <br>Please try again.";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "Server problem. <br>Please try again later.";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Password successfully updated!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
  viewPassword( document.getElementById('editPassword_new_img'), document.getElementById('register_epin'));
  viewPassword( document.getElementById('editPassword_reenter_img'), document.getElementById('register_epin_reenter'));
</script>

<script>
  function checkIfVariableIsNullOrEmptyString(field,isValidate)
  {
    if(field == null || field == "" || field.length == 0)
    {
      isValidate += 1;
      return isValidate;
    }
    else
    {
      return isValidate;
    }
  }
  function ePinValidation()
  {
    // First Level Validation
    let isValidatedAndCanProceedToNextLevel = 0;

    let register_epin = $('#register_epin').val();
    let register_epin_reenter = $('#register_epin_reenter').val();

    // console.log('Initial Counter = '+isValidatedAndCanProceedToNextLevel);
  
    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_epin,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);
    
    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_epin_reenter,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    // Second Level Validation
    if(isValidatedAndCanProceedToNextLevel == 0)
    {
      // alert('can continue');
      if(register_epin.length >= 6)
      {
        if(register_epin == register_epin_reenter)
        {        }
        else
        {
          alert('E-Pin does not match ! Please try again ! ');
          event.preventDefault();
        }
      }
      else
      {
        alert('E-Pin must be more than 5 ! Please try again ! ');
        event.preventDefault();
      }
    }
    else
    {
      alert('Please enter all fields required ! ');
      event.preventDefault();
    }
  }
</script>

</body>
</html>