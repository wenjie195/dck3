<style>
.imagetj
{
  opacity: 1;
  display: block;
  width: 98%;
  height:98%;
  transition: .5s ease;
  backface-visibility: hidden;
}
.edit-profile-div-smalltj
{
    width: 200px;
	height:200px;
}
.profile-divtj 
{
    width: 200px;
	height:200px;
	background-size:cover;
}
</style>

<div class="edit-profile-div1"> 
        <?php
        if($userProPic == "")
        {
        ?>
            <div class="profile-div edit-profile-div-small">
                <a href="uploadProfilePicture.php">
                    <button class="edit-profile-pic-btn text-center white-text clean"><img src="img/camera.png" class="camera-icon" alt="Update Profile Picture" title="Update Profile Picture"> UPDATE</button>  
                </a>
            </div>      
        <?php
        }
        else
        {   ?>
            <div class="edit-profile-divtj">
                <div class="profile-divtj edit-profile-div-small">
                    <a href="uploadProfilePicture.php">
                    <img src="upload/<?php echo $userProPic->getFilename();?>" style="width:100%" class="imagetj" alt="Update Profile Picture" title="Update Profile Picture">
        </a>
                </div>      
            </div>
        <?php
        }
        ?>
</div>