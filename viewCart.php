<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Cart.php';

require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    createOrder($conn,$uid);
    header('Location: ./checkout.php');

}


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/product.php" />
<meta property="og:title" content="DCK® Engine Oil Booster | DCK Supreme" />
<title>DCK® Engine Oil Booster | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/product.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">

<!-- Start Menu -->

<div class="yellow-body padding-from-menu same-padding">
	<?php include 'header-sherry.php'; ?>

    <h1 class="cart-h1">Your Cart</h1>

    <form method="POST">


        <table class="cart-table">
            <th>Product</th>
            <th></th>
            <th class="quantity-td">QUANTITY</th>
            <th>PRICE</th>
            <th>TOTAL</th>
        </table>

        <?php
            $conn = connDB();
            if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
            {
                $productListHtml = getShoppingCart($conn,2);
                echo $productListHtml;
            }
            else
            {
                echo " <h3> YOUR CART IS EMPTY </h3>";
            }


            if(array_key_exists('xclearCart', $_POST))
            {
                xclearCart();
            }
            else
            {
            // code...
                unset($productListHtml);
            }

            $conn->close();
        ?>

    <div class="cart-bottom-div">
        <div class="left-cart-bottom-div">
            <p class="continue-shopping pointer continue2">
                <a href="product.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back"> Continue Shopping</a>
            </p>
        </div>
    </div>

    </form>

</div>

<?php include 'js.php'; ?>

</body>
</html>