<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$productsOrders =  getProductOrders($conn);

$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/shippingOut.php" />
    <meta property="og:title" content="Shipping Out | DCK Supreme" />
    <title>Shipping Out | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/shippingOut.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="details-h1" onclick="goBack()">
        <a class="black-white-link2 hover1">
            <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Order Number : #<?php echo $_POST['order_id'];?>
        </a>
    </h1>

    <table class="details-table">
    	<tbody>
        <?php
        if(isset($_POST['order_id']))
        {
             $conn = connDB();
            //Order
            $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
            //OrderProduct
            $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
            //$orderDetails = $orderArray[0];

            if($orderArray != null)
            {?>

                <tr>
                    <td>Name</td>
                    <td>:</td>
                    <td><?php echo $orderArray[0]->getName()?></td>
                </tr>
                <tr>
                    <td>Contact</td>
                    <td>:</td>
                    <td><?php echo $orderArray[0]->getContactNo()?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>:</td>
                    <td><?php echo $orderArray[0]->getShippingStatus()?></td>
                </tr>
                <tr>
                    <td>Reason</td>
                    <td>:</td>
                    <td><?php echo $orderArray[0]->getRefundReason()?></td>
                </tr>



                <?php

            }
        }
        else
        {}
        $conn->close();
        ?>
        </tbody>
    </table>




    <div class="clear"></div>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>
