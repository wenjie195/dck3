<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';


$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}

$products = getProduct($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),"s");
$userDetails = $userRows[0];

// $rem = strtotime('2019-09-01 14:00:00') - time();
// $day = floor($rem / 86400);
// $hr  = floor(($rem % 86400) / 3600);
// $min = floor(($rem % 3600) / 60);
// $sec = ($rem % 60);
// if($day) echo "$day Days ";
// if($hr) echo "$hr Hours ";
// if($min) echo "$min Minutes ";
// if($sec) echo "$sec Seconds ";
// echo "Remaining...";

$currentOrders = getOrders($conn," WHERE shipping_status = 'SHIPPED' ",array("shipping_status"),"s");
$finalOrders = array();
foreach ($currentOrders as $order) {
  $allProductOrders = getProductOrders($conn, " WHERE order_id = ? ",array("order_id"),array($order->getId()),"i");
  foreach ($allProductOrders as $thisProductOrder) {
    $tempProducts = getProduct($conn," WHERE id = ? ",array("id"),array($thisProductOrder->getProductId()),"i");
    $thisProduct = $tempProducts[0];

    $thisFinalOrder = array();
    $thisFinalOrder['product_name'] = $thisProduct->getName();
    $thisFinalOrder['quantity'] = $thisProductOrder->getQuantity();
    $thisFinalOrder['price'] = $thisProductOrder->getTotalProductPrice();
    array_push($finalOrders,$thisFinalOrder);
  }

}




$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminDashboard.php" />
    <meta property="og:title" content="Admin Dashboard | DCK Supreme" />
    <title>Admin Dashboard | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminDashboard.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
    
    <h1 class="h1-title h1-before-border shipping-h1 right-info"><a href="adminDashboard.php?lang=en" class="white-text title-tab-a">EN</a> | <a href="adminDashboard.php?lang=ch" class="white-text title-tab-a">中文</a></h1>
    <!-- <h1 class="h1-title h1-before-border">Dashboard</h1> -->
    <h1 class="h1-title h1-before-border"><?php echo _MAINJS_ADMDASH_DASHBOARD ?></h1>
    <div class="border-top100 four-div-container">
    	<a href="adminProduct.php" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/product1.png" class="four-img hover1a" alt="Products" title="Products">
                <img src="img/product2.png" class="four-img hover1b" alt="Products" title="Products">
                <!-- <p class="four-div-p"><b><?php //echo totalProductDashboard(); ?> Total Product</b></p> -->
                <p class="four-div-p"><b><?php echo totalProductDashboard(); ?> <?php echo _MAINJS_ADMDASH_TOTAL_PRODUCT ?> </b></p>
            </div>
        </a>
        <!-- <a href="adminShpping.php" class="black-text"> -->
        <a href="adminShipping.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div1 white-div-yellow">
                <img src="img/shipping1.png" class="four-img hover1a" alt="Shipping" title="Shipping">
                <img src="img/shipping2.png" class="four-img hover1b" alt="Shipping" title="Shipping">
                <!-- <p class="four-div-p"><b><?php //echo shippingRequest() ?> Shipping Requests</b></p> -->
                <p class="four-div-p"><b><?php echo shippingRequest() ?> <?php echo _MAINJS_ADMDASH_SHIPPING_REQUEST ?> </b></p>
            </div>
        </a>
        <a href="adminWithdrawal.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div2 white-div-yellow">
                <img src="img/withdraw1.png" class="four-img hover1a" alt="Withdrawal" title="Withdrawal">
                <img src="img/withdraw2.png" class="four-img hover1b" alt="Withdrawal" title="Withdrawal">
                <!-- <p class="four-div-p"><b><?php //echo withdrawalRequest() ?> Withdrawal Requests</b></p> -->
                <p class="four-div-p"><b><?php echo withdrawalRequest() ?> <?php echo _MAINJS_ADMDASH_WITHDRAWAL_REQUEST ?> </b></p>
            </div>
        </a>
        <a href="adminMember.php" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/member1.png" class="four-img hover1a" alt="New Joined Members" title="New Joined Members">
                <img src="img/member2.png" class="four-img hover1b" alt="New Joined Members" title="New Joined Members">
                <!-- <p class="four-div-p"><b><?php //echo totalMemberJoined()?> Total Members</b></p> -->
                <p class="four-div-p"><b><?php echo totalMemberJoined()?> <?php echo _MAINJS_ADMDASH_TOTAL_MEMBER ?> </b></p>
            </div>
        </a>
    </div>
    <div class="clear"></div>
    <!-- <h1 class="h1-title extra-mtop2">Sales of This Week</h1> -->
    <h1 class="h1-title extra-mtop2"><?php echo _MAINJS_ADMDASH_SALES_OF_THIS_WEEK ?></h1>
    <div class="with100">
    	<table class="sales-table">
        	<thead>
            	<!-- <tr class="sales-th-tr">
                	<th></th>
                    <th>PRODUCT</th>
                    <th>QUANTITY</th>
                    <th class="right-cell">TOTAL (RM)</th>
                </tr> -->
                <tr class="sales-th-tr">
                    <th></th>
                    <th><?php echo _MAINJS_ADMDASH_PRODUCT ?></th>
                    <th><?php echo _MAINJS_ADMDASH_QUANTITY ?></th>
                    <th class="right-cell"><?php echo _MAINJS_ADMDASH_TOTAL_IN_RM ?></th>
                </tr>
            </thead>

            <?php
          //  foreach ($finalOrders as $finalOrder) {
          // if(isset($_POST['order_id']))
          // {
              $conn = connDB();
              //Order
              $orderArray = getOrders($conn," WHERE shipping_status = 'SHIPPED' ",array("shipping_status"),"s");
              if ($orderArray) {


              //OrderProduct
              $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($orderArray[0]->getId()),"i");
              //$orderDetails = $orderArray[0];
}else {}
              //Product Details
              //$abc = getProduct($conn,"WHERE product_id = ? ", array("product_id") ,array($_POST['order_id']),"i");

              if($orderArray != null)
              {
                  for($cntAA = 0;$cntAA < count($orderProductArray) ;$cntAA++)
                  {

                  $product = getProduct($conn ,"WHERE id = ? ", array("name"),array($orderProductArray[$cntAA]->getProductId()),"i");
                      ?>
                  <!-- <tr> -->
                      <!-- <input type="hidden" name="order_id" value="<?php //echo $_POST['order_id'];?>">  -->
                      <!-- <td><?php // ($cntAA+1)?></td> -->
                      <!-- <td><?php //echo $product[0] -> getName(); ?></td> -->
                      <!-- <td><?php //echo $orderProductArray[$cntAA]->getQuantity();?></td> -->
                  <!-- </tr> -->
                  <?php

                  $buyQuantity = $orderProductArray[$cntAA]->getQuantity();
                  $productName = $product[0] -> getName();
                  $currentQuantity = $product[0] -> getBuyStock();
                  $currentStock = $product[0] -> getStock();
                  $totalPriceBuy = $product[0] -> getTotalPrice();

                  ?><tr>
                  	<td></td>
                      <td><?php echo $productName; ?></td>
                      <td><?php echo $currentQuantity; ?></td>
                      <td class="right-cell"><?php echo $totalPriceBuy; ?></td>
                  </tr><?php

                  }
              }
          //}


             ?>


        <?php //} ?>
            <!-- <tr>
            	<td>3.</td>
                <td>DCK Fuel Booster</td>
                <td>2,000</td>
                <td class="right-cell">300,000.00</td>
            </tr>
            <tr>
            	<td>4.</td>
                <td>DCK Engine Oil Booster</td>
                <td>3,000</td>
                <td class="right-cell">300,000.00</td>
            </tr> -->
            <tr class="double-border">
            	<td></td>
                <td>TOTAL</td>
                <td><?php
                if (!$orderArray) {
                  echo 0;
                }else {
                   echo totalQuantity(); ?></td>
                <?php } ?>

                <td class="right-cell"><?php
                if (!$orderArray) {
                  echo 0;
                }else {
                    echo totalPrice() ?></td>
                <?php } ?>


            </tr>
        </table>
    </div>
</div>

<?php


function totalPrice(){
  $conn = connDB();
  $result1 = mysqli_query($conn,"SELECT sum(total_price) AS productno1 FROM `product`");

  if (mysqli_num_rows($result1) > 0) {
  ?>  <table>


  <div class="clear"></div>

  <div class="with100">
   <table class="sales-table">
       <thead>

              <?php
              $i=0;
              while($row = mysqli_fetch_array($result1)) {

              ?>

          </thead>
          <?php

           ?>



              <?php echo $row["productno1"]; ?>




              <?php
              $i++;
              }

              ?>

        </table>



         <?php
        }
        else{
            echo "No result found";
        }

}

 ?>

 <?php


 function totalQuantity(){
   $conn = connDB();
   $result1 = mysqli_query($conn,"SELECT sum(buy_stock) AS productno1 FROM `product`");

   if (mysqli_num_rows($result1) > 0) {
   ?>  <table>


   <div class="clear"></div>

   <div class="with100">
    <table class="sales-table">
        <thead>

               <?php
               $i=0;
               while($row = mysqli_fetch_array($result1)) {

               ?>

           </thead>
           <?php

            ?>



               <?php echo $row["productno1"]; ?>




               <?php
               $i++;
               }

               ?>

         </table>



          <?php
         }
         else{
             echo "No result found";
         }

 }

  ?>

 <?php


 function totalMemberJoined(){
   $conn = connDB();
   $result1 = mysqli_query($conn,"SELECT count(uid) AS totalMemberJoined FROM user");

   if (mysqli_num_rows($result1) > 0) {
   ?>  <table>


   <div class="clear"></div>

   <div class="with100">
    <table class="sales-table">
        <thead>

               <?php
               $i=0;
               while($row = mysqli_fetch_array($result1)) {

               ?>

           </thead>
           <?php

            ?>



               <?php echo $row["totalMemberJoined"]; ?>




               <?php
               $i++;
               }

               ?>

         </table>



          <?php
         }
         else{
             echo "No result found";
         }

 }

  ?>

  <?php


  function shippingRequest(){
    $conn = connDB();
    $result1 = mysqli_query($conn,"SELECT count(uid) AS shippingRequest FROM `orders` WHERE shipping_status = 'PENDING'");

    if (mysqli_num_rows($result1) > 0) {
    ?>  <table>


    <div class="clear"></div>

    <div class="with100">
     <table class="sales-table">
         <thead>

                <?php
                $i=0;
                while($row = mysqli_fetch_array($result1)) {

                ?>

            </thead>
            <?php

             ?>



                <?php echo $row["shippingRequest"]; ?>




                <?php
                $i++;
                }

                ?>

          </table>



           <?php
          }
          else{
              echo "No result found";
          }

  }

   ?>

   <?php


   function withdrawalRequest(){
     $conn = connDB();
     $result1 = mysqli_query($conn,"SELECT count(uid) AS withdrawalRequest FROM `withdrawal` WHERE withdrawal_status = 'PENDING'");

     if (mysqli_num_rows($result1) > 0) {
     ?>  <table>


     <div class="clear"></div>

     <div class="with100">
      <table class="sales-table">
          <thead>

                 <?php
                 $i=0;
                 while($row = mysqli_fetch_array($result1)) {

                 ?>

             </thead>
             <?php

              ?>



                 <?php echo $row["withdrawalRequest"]; ?>




                 <?php
                 $i++;
                 }

                 ?>

           </table>



            <?php
           }
           else{
               echo "0";
           }

   }

    ?>

   <?php


   function totalProductDashboard(){
     $conn = connDB();
     $result1 = mysqli_query($conn,"SELECT count(display) AS totalProductDashboard FROM product  WHERE display = 1");

     if (mysqli_num_rows($result1) > 0) {
     ?>  <table>


     <div class="clear"></div>

     <div class="with100">
      <table class="sales-table">
          <thead>

                 <?php
                 $i=0;
                 while($row = mysqli_fetch_array($result1)) {

                 ?>

             </thead>
             <?php

              ?>



                 <?php echo $row["totalProductDashboard"]; ?>




                 <?php
                 $i++;
                 }

                 ?>

           </table>



            <?php
           }
           else{
               echo "No result found";
           }

   }

    ?>



<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
