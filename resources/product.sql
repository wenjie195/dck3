-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2019 at 04:16 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_oilxag`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(5000) NOT NULL,
  `price` decimal(50,0) NOT NULL COMMENT 'With MYR as base and is in point system (RM1 = 100 point)',
  `stock` int(255) NOT NULL,
  `buy_stock` int(255) NOT NULL,
  `total_price` int(255) NOT NULL,
  `display` int(20) DEFAULT 1,
  `type` int(255) NOT NULL DEFAULT 1 COMMENT '1 = normal product',
  `description` varchar(10000) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `stock`, `buy_stock`, `total_price`, `display`, `type`, `description`, `images`, `date_created`, `date_updated`) VALUES
(1, 'Oil Booster 3pcs (1 set)', '300', 1000, 0, 0, 1, 1, 'OIL BOOSTER 3PCS (1 SET)', 'DCK-Engine-Oil-Booster.png', '2019-07-24 09:17:04', '2019-12-19 02:38:18'),
(2, 'Motorcycle Oil Booster 15pcs (1 set)', '300', 1000, 0, 0, 0, 1, 'Motorcycle Oil Booster 15pcs (1 set)', '', '2019-12-19 02:38:39', '2019-12-19 03:01:12'),
(3, 'Synthetic Plus Engine Oil 5w-30 (1 set)', '300', 1000, 0, 0, 1, 1, 'SYNTHETIC PLUS ENGINE OIL 5W-30 (1 SET)', '5w.png', '2019-07-25 04:11:21', '2019-12-19 02:40:11'),
(4, 'Synthetic Plus Engine Oil 10w-40  (1 set)', '300', 1000, 0, 0, 1, 1, 'SYNTHETIC PLUS ENGINE OIL 10W-40  (1 SET)', '10w.png', '2019-10-29 06:18:28', '2019-12-19 02:40:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9223372036854775807;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
