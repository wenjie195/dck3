-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2019 at 04:22 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_oilxag`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `announce_id` int(255) NOT NULL,
  `announce_message` varchar(25000) NOT NULL,
  `announce_showThis` int(2) NOT NULL,
  `announce_dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `announce_lastUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`announce_id`, `announce_message`, `announce_showThis`, `announce_dateCreated`, `announce_lastUpdated`) VALUES
(1, 'Thanks for joining us, We welcome u to our journey', 1, '2019-08-14 04:17:07', '2019-08-14 06:58:37'),
(2, 'Have A nice day ! Fizo', 1, '2019-08-14 04:29:46', '2019-08-14 06:58:21'),
(3, 'Welcome To Oilxag People', 1, '2019-08-14 06:24:43', '2019-08-15 02:01:14'),
(4, 'Mun cun  eeedsafafasdfasd', 0, '2019-08-15 02:01:49', '2019-08-15 02:24:18'),
(5, 'asdsdaasdccc vvvvvvvv', 0, '2019-08-15 02:26:06', '2019-08-15 02:26:17'),
(6, 'lyon is noob team', 1, '2019-08-15 02:26:24', '2019-08-28 06:27:42'),
(7, 'GuangZhou Evergrande BIG BIG!!', 1, '2019-08-28 06:27:16', '2019-09-11 14:57:28'),
(8, 'lalala', 0, '2019-09-10 09:23:39', '2019-09-10 09:23:47'),
(9, 'dadada', 0, '2019-09-10 09:24:04', '2019-09-10 09:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referrer_name` varchar(255) DEFAULT NULL,
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `signupby` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `register_downline_no` int(255) DEFAULT NULL,
  `amount` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonus`
--

INSERT INTO `bonus` (`id`, `referrer_id`, `referrer_name`, `referral_id`, `referral_name`, `signupby`, `current_level`, `top_referrer_id`, `register_downline_no`, `amount`, `date_created`, `date_updated`) VALUES
(1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'ef087c3d140821babc712b7094f53684', 'busquest', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 1, 150, '2019-11-29 06:04:01', '2019-11-29 06:04:01'),
(2, 'e809ed5b38535157bf9a163fa1225ade', 'martial', 'ef087c3d140821babc712b7094f53684', 'busquest', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 0, 0, '2019-11-29 06:04:01', '2019-11-29 06:04:01'),
(3, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 2, 150, '2019-11-29 06:05:06', '2019-11-29 06:05:06'),
(4, 'e809ed5b38535157bf9a163fa1225ade', 'martial', 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 0, 0, '2019-11-29 06:05:06', '2019-11-29 06:05:06'),
(5, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 3, 50, '2019-11-29 06:05:25', '2019-11-29 06:05:25'),
(6, 'e809ed5b38535157bf9a163fa1225ade', 'martial', 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 0, 0, '2019-11-29 06:05:25', '2019-11-29 06:05:25'),
(7, 'ef087c3d140821babc712b7094f53684', 'busquest', 'df4fe5576da5c64c8103857cd7df757b', 'suarez', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 1, 150, '2019-11-29 06:06:25', '2019-11-29 06:06:25'),
(8, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'df4fe5576da5c64c8103857cd7df757b', 'suarez', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:06:25', '2019-11-29 06:06:25'),
(9, 'ef087c3d140821babc712b7094f53684', 'busquest', 'def0bb9754e106d0ca569c262ac3b655', 'dembele', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 2, 150, '2019-11-29 06:07:12', '2019-11-29 06:07:12'),
(10, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'def0bb9754e106d0ca569c262ac3b655', 'dembele', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:07:12', '2019-11-29 06:07:12'),
(11, 'ef087c3d140821babc712b7094f53684', 'busquest', 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 50, '2019-11-29 06:07:52', '2019-11-29 06:07:52'),
(12, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:07:52', '2019-11-29 06:07:52'),
(13, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', 'a6d41a96d021e1509c7ee16aa7f8ab26', 'rakitic', 'pique', 3, 'e809ed5b38535157bf9a163fa1225ade', 1, 150, '2019-11-29 06:08:23', '2019-11-29 06:08:23'),
(14, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'a6d41a96d021e1509c7ee16aa7f8ab26', 'rakitic', 'pique', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:08:23', '2019-11-29 06:08:23'),
(15, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', '9a74193dc0cc524453d2772417af3c0d', 'arthur', 'pique', 3, 'e809ed5b38535157bf9a163fa1225ade', 2, 150, '2019-11-29 06:08:46', '2019-11-29 06:08:46'),
(16, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '9a74193dc0cc524453d2772417af3c0d', 'arthur', 'pique', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:08:46', '2019-11-29 06:08:46'),
(17, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', '10686d6d80b70cc994f7bae05a28f6b2', 'alena', 'pique', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 50, '2019-11-29 06:09:24', '2019-11-29 06:09:24'),
(18, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '10686d6d80b70cc994f7bae05a28f6b2', 'alena', 'pique', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:09:24', '2019-11-29 06:09:24'),
(19, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', '75d7c0ea4bf9fa0c91cd890a30f5f65c', 'dejong', 'pique', 3, 'e809ed5b38535157bf9a163fa1225ade', 4, 50, '2019-11-29 06:09:44', '2019-11-29 06:09:44'),
(20, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '75d7c0ea4bf9fa0c91cd890a30f5f65c', 'dejong', 'pique', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:09:44', '2019-11-29 06:09:44'),
(21, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', 'c34fdaf91eebf2aa115d1ca8d17b6ed4', 'vidal', 'pique', 3, 'e809ed5b38535157bf9a163fa1225ade', 5, 50, '2019-11-29 06:10:13', '2019-11-29 06:10:13'),
(22, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'c34fdaf91eebf2aa115d1ca8d17b6ed4', 'vidal', 'pique', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:10:13', '2019-11-29 06:10:13'),
(23, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '339ddead7419d1b70fe07b833182fe92', 'terstegen', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 4, 50, '2019-11-29 06:10:46', '2019-11-29 06:10:46'),
(24, 'e809ed5b38535157bf9a163fa1225ade', 'martial', '339ddead7419d1b70fe07b833182fe92', 'terstegen', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 0, 0, '2019-11-29 06:10:46', '2019-11-29 06:10:46'),
(25, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '48a48715ca08f5eacb5c616351ba9220', 'neto', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 5, 50, '2019-11-29 06:11:32', '2019-11-29 06:11:32'),
(26, 'e809ed5b38535157bf9a163fa1225ade', 'martial', '48a48715ca08f5eacb5c616351ba9220', 'neto', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 0, 0, '2019-11-29 06:11:32', '2019-11-29 06:11:32'),
(27, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'a1cba8835408386d48bf0b01ab7a57a3', 'semedo', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 1, 150, '2019-11-29 06:13:04', '2019-11-29 06:13:04'),
(28, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'a1cba8835408386d48bf0b01ab7a57a3', 'semedo', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 06:13:04', '2019-11-29 06:13:04'),
(29, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', '4095edfbc7b3e30174bb8707d84e0119', 'todibo', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 2, 150, '2019-11-29 06:13:28', '2019-11-29 06:13:28'),
(30, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '4095edfbc7b3e30174bb8707d84e0119', 'todibo', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 06:13:28', '2019-11-29 06:13:28'),
(31, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'd59bff2cc57dc3b48be3c53dbf96735d', 'lenglet', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 3, 50, '2019-11-29 06:13:53', '2019-11-29 06:13:53'),
(32, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'd59bff2cc57dc3b48be3c53dbf96735d', 'lenglet', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 06:13:53', '2019-11-29 06:13:53'),
(33, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'a83067dabe368e9d48d5ac578eb53b96', 'wague', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 4, 50, '2019-11-29 06:14:13', '2019-11-29 06:14:13'),
(34, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'a83067dabe368e9d48d5ac578eb53b96', 'wague', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 06:14:13', '2019-11-29 06:14:13'),
(35, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', '045384a79e7cdda062ae1716730d3434', 'alba', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 5, 50, '2019-11-29 06:14:33', '2019-11-29 06:14:33'),
(36, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '045384a79e7cdda062ae1716730d3434', 'alba', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 06:14:33', '2019-11-29 06:14:33'),
(37, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', '6135518fd0850893ff83caee61edce93', 'umtiti', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 6, 50, '2019-11-29 06:14:56', '2019-11-29 06:14:56'),
(38, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '6135518fd0850893ff83caee61edce93', 'umtiti', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 06:14:56', '2019-11-29 06:14:56'),
(39, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'bc7f79073712cb70eac8f4662aad476b', 'firpo', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 7, 80, '2019-11-29 06:15:14', '2019-11-29 06:15:14'),
(40, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'bc7f79073712cb70eac8f4662aad476b', 'firpo', 'roberto', 3, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 06:15:14', '2019-11-29 06:15:14'),
(41, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '732a673bd0b13f8d11eee452d3350b61', 'aguero', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 6, 50, '2019-11-29 06:17:01', '2019-11-29 06:17:01'),
(42, 'e809ed5b38535157bf9a163fa1225ade', 'martial', '732a673bd0b13f8d11eee452d3350b61', 'aguero', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 0, 0, '2019-11-29 06:17:01', '2019-11-29 06:17:01'),
(43, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '04bb6ca95fadf5296af7e5605c31e251', 'martinez', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 7, 80, '2019-11-29 06:17:31', '2019-11-29 06:17:31'),
(44, 'e809ed5b38535157bf9a163fa1225ade', 'martial', '04bb6ca95fadf5296af7e5605c31e251', 'martinez', 'messi', 2, 'e809ed5b38535157bf9a163fa1225ade', 0, 0, '2019-11-29 06:17:31', '2019-11-29 06:17:31'),
(45, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '1e586a9d860f507633417b6f20e5f404', 'lemar', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 1, 150, '2019-11-29 06:33:29', '2019-11-29 06:33:29'),
(46, 'ef087c3d140821babc712b7094f53684', 'busquest', '1e586a9d860f507633417b6f20e5f404', 'lemar', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:33:29', '2019-11-29 06:33:29'),
(47, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '032111d99e62c8cfa6df496e1e8abce3', 'giroud', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 2, 150, '2019-11-29 06:33:45', '2019-11-29 06:33:45'),
(48, 'ef087c3d140821babc712b7094f53684', 'busquest', '032111d99e62c8cfa6df496e1e8abce3', 'giroud', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:33:45', '2019-11-29 06:33:45'),
(49, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '57a287c2e6abba677a74323553233922', 'mbappe', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 3, 50, '2019-11-29 06:34:00', '2019-11-29 06:34:00'),
(50, 'ef087c3d140821babc712b7094f53684', 'busquest', '57a287c2e6abba677a74323553233922', 'mbappe', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:34:00', '2019-11-29 06:34:00'),
(51, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', 'd922522d8a45daa000c8ffe4ef401e50', 'coman', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 4, 50, '2019-11-29 06:34:18', '2019-11-29 06:34:18'),
(52, 'ef087c3d140821babc712b7094f53684', 'busquest', 'd922522d8a45daa000c8ffe4ef401e50', 'coman', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:34:18', '2019-11-29 06:34:18'),
(53, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '9e0cfb3ed64d1ec730f7cc1675074dcd', 'fekir', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 5, 50, '2019-11-29 06:34:33', '2019-11-29 06:34:33'),
(54, 'ef087c3d140821babc712b7094f53684', 'busquest', '9e0cfb3ed64d1ec730f7cc1675074dcd', 'fekir', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:34:33', '2019-11-29 06:34:33'),
(55, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '9c2593f8f987185da63ecb6e47143c80', 'yedder', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 6, 50, '2019-11-29 06:34:55', '2019-11-29 06:34:55'),
(56, 'ef087c3d140821babc712b7094f53684', 'busquest', '9c2593f8f987185da63ecb6e47143c80', 'yedder', 'griezmann', 4, 'e809ed5b38535157bf9a163fa1225ade', 3, 0, '2019-11-29 06:34:55', '2019-11-29 06:34:55'),
(57, 'ef087c3d140821babc712b7094f53684', 'busquest', '6cf790523be5a7b0761ca3cb7b7b051f', 'navas', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 4, 50, '2019-11-29 06:41:03', '2019-11-29 06:41:03'),
(58, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '6cf790523be5a7b0761ca3cb7b7b051f', 'navas', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 7, 30, '2019-11-29 06:41:03', '2019-11-29 06:41:03'),
(59, 'ef087c3d140821babc712b7094f53684', 'busquest', '37707d7e1f99de1abb4caec943909e46', 'bernat', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 5, 50, '2019-11-29 06:41:34', '2019-11-29 06:41:34'),
(60, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '37707d7e1f99de1abb4caec943909e46', 'bernat', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 7, 30, '2019-11-29 06:41:34', '2019-11-29 06:41:34'),
(61, 'ef087c3d140821babc712b7094f53684', 'busquest', 'e10702bc81c2c7235cc5d847b8c9ecf9', 'carvajal', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 6, 50, '2019-11-29 06:41:57', '2019-11-29 06:41:57'),
(62, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'e10702bc81c2c7235cc5d847b8c9ecf9', 'carvajal', 'busquest', 3, 'e809ed5b38535157bf9a163fa1225ade', 7, 30, '2019-11-29 06:41:57', '2019-11-29 06:41:57'),
(63, '57a287c2e6abba677a74323553233922', 'mbappe', '1d4b527c436b211d12aa099543ddf651', 'neymar', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 1, 150, '2019-11-29 06:43:39', '2019-11-29 06:43:39'),
(64, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '1d4b527c436b211d12aa099543ddf651', 'neymar', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 06:43:40', '2019-11-29 06:43:40'),
(65, '57a287c2e6abba677a74323553233922', 'mbappe', '88be5665f6b84f174474b489dbfab274', 'icardi', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 2, 150, '2019-11-29 06:44:18', '2019-11-29 06:44:18'),
(66, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '88be5665f6b84f174474b489dbfab274', 'icardi', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 06:44:18', '2019-11-29 06:44:18'),
(67, '57a287c2e6abba677a74323553233922', 'mbappe', '5e761a85525cc1ea7a1e555f1c1f05a6', 'cavani', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 3, 50, '2019-11-29 06:44:44', '2019-11-29 06:44:44'),
(68, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '5e761a85525cc1ea7a1e555f1c1f05a6', 'cavani', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 06:44:44', '2019-11-29 06:44:44'),
(69, '57a287c2e6abba677a74323553233922', 'mbappe', '46636898c58a74a046ec10d2bd551120', 'maria', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 4, 50, '2019-11-29 06:45:47', '2019-11-29 06:45:47'),
(70, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '46636898c58a74a046ec10d2bd551120', 'maria', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 06:45:47', '2019-11-29 06:45:47'),
(71, '57a287c2e6abba677a74323553233922', 'mbappe', '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 5, 50, '2019-11-29 06:46:25', '2019-11-29 06:46:25'),
(72, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 06:46:25', '2019-11-29 06:46:25'),
(73, '57a287c2e6abba677a74323553233922', 'mbappe', '00cba3f7907d3d6b7e6114536cd46f7e', 'silva', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 6, 50, '2019-11-29 06:46:44', '2019-11-29 06:46:44'),
(74, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '00cba3f7907d3d6b7e6114536cd46f7e', 'silva', 'mbappe', 5, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 06:46:44', '2019-11-29 06:46:44'),
(75, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '5369c3bd5d853e898324d0b633a85cd1', 'kimmich', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 1, 150, '2019-11-29 07:52:07', '2019-11-29 07:52:07'),
(76, '57a287c2e6abba677a74323553233922', 'mbappe', '5369c3bd5d853e898324d0b633a85cd1', 'kimmich', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 07:52:07', '2019-11-29 07:52:07'),
(77, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', 'ed3b4fb32872ad093139167be8657970', 'kroos', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 2, 150, '2019-11-29 07:52:27', '2019-11-29 07:52:27'),
(78, '57a287c2e6abba677a74323553233922', 'mbappe', 'ed3b4fb32872ad093139167be8657970', 'kroos', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 07:52:27', '2019-11-29 07:52:27'),
(79, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '9badb66c2b36a6466c6301f4cd8e9fed', 'brandt', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 3, 50, '2019-11-29 07:52:47', '2019-11-29 07:52:47'),
(80, '57a287c2e6abba677a74323553233922', 'mbappe', '9badb66c2b36a6466c6301f4cd8e9fed', 'brandt', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 07:52:47', '2019-11-29 07:52:47'),
(81, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', 'bf34de0101450f0170addc5de18548c2', 'amiri', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 4, 50, '2019-11-29 08:09:10', '2019-11-29 08:09:10'),
(82, '57a287c2e6abba677a74323553233922', 'mbappe', 'bf34de0101450f0170addc5de18548c2', 'amiri', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 08:09:10', '2019-11-29 08:09:10'),
(83, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '754f798686c8884c3634f8282d9107c1', 'serder', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 5, 50, '2019-11-29 08:09:31', '2019-11-29 08:09:31'),
(84, '57a287c2e6abba677a74323553233922', 'mbappe', '754f798686c8884c3634f8282d9107c1', 'serder', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 08:09:31', '2019-11-29 08:09:31'),
(85, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '5b8a48e35b7618a9b8c89ceb07a171f1', 'rudy', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 6, 50, '2019-11-29 08:09:53', '2019-11-29 08:09:53'),
(86, '57a287c2e6abba677a74323553233922', 'mbappe', '5b8a48e35b7618a9b8c89ceb07a171f1', 'rudy', 'draxler', 6, 'e809ed5b38535157bf9a163fa1225ade', 5, 0, '2019-11-29 08:09:53', '2019-11-29 08:09:53'),
(87, 'ed3b4fb32872ad093139167be8657970', 'kroos', '91fef392a2b723f933814d014d06579a', 'ramos', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 1, 150, '2019-11-29 08:13:22', '2019-11-29 08:13:22'),
(88, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '91fef392a2b723f933814d014d06579a', 'ramos', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:13:22', '2019-11-29 08:13:22'),
(89, 'ed3b4fb32872ad093139167be8657970', 'kroos', 'e393d75096300eff3f573b453e043eb3', 'varane', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 2, 150, '2019-11-29 08:13:48', '2019-11-29 08:13:48'),
(90, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', 'e393d75096300eff3f573b453e043eb3', 'varane', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:13:48', '2019-11-29 08:13:48'),
(91, 'ed3b4fb32872ad093139167be8657970', 'kroos', '2cb58fb99c7066d9d1d2910ca02d167c', 'benzema', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 3, 50, '2019-11-29 08:14:09', '2019-11-29 08:14:09'),
(92, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '2cb58fb99c7066d9d1d2910ca02d167c', 'benzema', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:14:09', '2019-11-29 08:14:09'),
(93, 'ed3b4fb32872ad093139167be8657970', 'kroos', '33a18846dc5953b1f0fa1e0a2890be42', 'modric', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 4, 50, '2019-11-29 08:14:39', '2019-11-29 08:14:39'),
(94, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '33a18846dc5953b1f0fa1e0a2890be42', 'modric', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:14:39', '2019-11-29 08:14:39'),
(95, 'ed3b4fb32872ad093139167be8657970', 'kroos', '0ee4baed4a1ce967f86a2b7ca8d53c28', 'bale', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 5, 50, '2019-11-29 08:15:52', '2019-11-29 08:15:52'),
(96, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '0ee4baed4a1ce967f86a2b7ca8d53c28', 'bale', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:15:52', '2019-11-29 08:15:52'),
(97, 'ed3b4fb32872ad093139167be8657970', 'kroos', '0118132cc1a056cd15b9c8944be0a40e', 'james', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 6, 50, '2019-11-29 08:16:42', '2019-11-29 08:16:42'),
(98, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '0118132cc1a056cd15b9c8944be0a40e', 'james', 'kroos', 7, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:16:42', '2019-11-29 08:16:42'),
(99, '0118132cc1a056cd15b9c8944be0a40e', 'james', 'c7fd51dec157bcaff0475c314712c12d', 'zapata', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 1, 150, '2019-11-29 08:20:35', '2019-11-29 08:20:35'),
(100, 'ed3b4fb32872ad093139167be8657970', 'kroos', 'c7fd51dec157bcaff0475c314712c12d', 'zapata', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:20:35', '2019-11-29 08:20:35'),
(101, '0118132cc1a056cd15b9c8944be0a40e', 'james', '0573e82d5020ded400e799170e947467', 'franco', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 2, 150, '2019-11-29 08:20:56', '2019-11-29 08:20:56'),
(102, 'ed3b4fb32872ad093139167be8657970', 'kroos', '0573e82d5020ded400e799170e947467', 'franco', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:20:56', '2019-11-29 08:20:56'),
(103, '0118132cc1a056cd15b9c8944be0a40e', 'james', '4ada769fe9a06812873c42caaa33151e', 'arias', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 3, 50, '2019-11-29 08:21:15', '2019-11-29 08:21:15'),
(104, 'ed3b4fb32872ad093139167be8657970', 'kroos', '4ada769fe9a06812873c42caaa33151e', 'arias', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:21:15', '2019-11-29 08:21:15'),
(105, '0118132cc1a056cd15b9c8944be0a40e', 'james', 'a4e2202c9e72b361caba9a6eb64e82b8', 'valencia', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 4, 50, '2019-11-29 08:22:03', '2019-11-29 08:22:03'),
(106, 'ed3b4fb32872ad093139167be8657970', 'kroos', 'a4e2202c9e72b361caba9a6eb64e82b8', 'valencia', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:22:03', '2019-11-29 08:22:03'),
(107, '0118132cc1a056cd15b9c8944be0a40e', 'james', '9ffec7a0823a30544d5ea04219e4cc55', 'sanchez', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 5, 50, '2019-11-29 08:22:21', '2019-11-29 08:22:21'),
(108, 'ed3b4fb32872ad093139167be8657970', 'kroos', '9ffec7a0823a30544d5ea04219e4cc55', 'sanchez', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:22:21', '2019-11-29 08:22:21'),
(109, '0118132cc1a056cd15b9c8944be0a40e', 'james', '97f250ed395ed79c01b335f8eb2c5678', 'falcao', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 6, 50, '2019-11-29 08:22:58', '2019-11-29 08:22:58'),
(110, 'ed3b4fb32872ad093139167be8657970', 'kroos', '97f250ed395ed79c01b335f8eb2c5678', 'falcao', 'james', 8, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-11-29 08:22:58', '2019-11-29 08:22:58'),
(111, '1d4b527c436b211d12aa099543ddf651', 'neymar', '35785d3e80c8d9d2b8a8511698c834da', 'allison', 'neymar', 6, 'e809ed5b38535157bf9a163fa1225ade', 1, 150, '2019-12-04 06:23:30', '2019-12-04 06:23:30'),
(112, '57a287c2e6abba677a74323553233922', 'mbappe', '35785d3e80c8d9d2b8a8511698c834da', 'allison', 'neymar', 6, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-12-04 06:23:30', '2019-12-04 06:23:30'),
(113, '1d4b527c436b211d12aa099543ddf651', 'neymar', '76a7e9111582c2f386b8e302f5879f94', 'ederson', 'neymar', 6, 'e809ed5b38535157bf9a163fa1225ade', 2, 150, '2019-12-04 06:26:46', '2019-12-04 06:26:46'),
(114, '57a287c2e6abba677a74323553233922', 'mbappe', '76a7e9111582c2f386b8e302f5879f94', 'ederson', 'neymar', 6, 'e809ed5b38535157bf9a163fa1225ade', 6, 30, '2019-12-04 06:26:46', '2019-12-04 06:26:46'),
(115, '1d4b527c436b211d12aa099543ddf651', 'neymar', '6ea0d772f06cee30142b86217ea29613', 'jesus', 'neymar', 6, 'e809ed5b38535157bf9a163fa1225ade', 3, 50, '2019-12-04 06:28:30', '2019-12-04 06:28:30');

-- --------------------------------------------------------

--
-- Table structure for table `cash_to_point`
--

CREATE TABLE `cash_to_point` (
  `id` int(11) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `point` int(255) NOT NULL,
  `date_create` timestamp(3) NOT NULL DEFAULT current_timestamp(3) ON UPDATE current_timestamp(3),
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 672),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `group_commission`
--

CREATE TABLE `group_commission` (
  `id` int(255) NOT NULL,
  `commission` decimal(30,2) NOT NULL,
  `level` int(3) DEFAULT NULL,
  `count` int(3) DEFAULT NULL COMMENT 'This is for number of referrals that the user has referred and has purchased a product',
  `description` varchar(5000) DEFAULT NULL,
  `transaction_type_id` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_commission`
--

INSERT INTO `group_commission` (`id`, `commission`, `level`, `count`, `description`, `transaction_type_id`, `date_created`, `date_updated`) VALUES
(1, '8.00', 1, NULL, 'in percent', 2, '2019-07-25 09:17:37', '2019-07-25 09:25:28'),
(2, '8.00', 2, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:13'),
(3, '6.00', 3, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:15'),
(4, '6.00', 4, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:17'),
(5, '4.00', 5, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:19'),
(6, '4.00', 6, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:20'),
(7, '2.00', 7, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:31'),
(8, '2.00', 8, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:33'),
(9, '1.00', 9, NULL, 'in percent', 2, '2019-07-25 09:18:43', '2019-07-25 09:25:36'),
(10, '5000.00', NULL, 1, 'in points (RM1 = 100).first referral.', 1, '2019-07-25 09:50:48', '2019-07-25 09:50:58'),
(11, '10000.00', NULL, 2, 'in points (RM1 = 100).second referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:36'),
(12, '15000.00', NULL, 3, 'in points (RM1 = 100).third referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:39'),
(13, '5000.00', NULL, 0, 'in points (RM1 = 100).subsequent referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:43'),
(14, '1.00', 10, NULL, 'in percent', 2, '2019-08-08 06:06:46', '2019-08-08 06:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `pid` int(255) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`pid`, `filename`, `uploaded`, `status`) VALUES
(130, 'messi1571735979.jpg', '2019-10-22 09:19:39', '1'),
(131, 'messi1572311393.jpg', '2019-10-29 01:09:53', '1'),
(132, 'TJ WenJie1573286223.jpg', '2019-11-09 07:57:03', '1');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `uid`, `username`, `fullname`, `place`, `date`, `time`, `date_created`, `date_updated`) VALUES
(1, 'b28efa7574466508acc7b553566375ec', 'tjiayu', 'Teh Jia Yu', 'aaa', '2019-11-05', '0858', '2019-11-10 08:40:13', '2019-11-10 08:40:13'),
(2, 'b28efa7574466508acc7b553566375ec', 'tjiayu', 'Teh Jia Yu', 'asd', 'asd', 'asd', '2019-11-10 08:52:41', '2019-11-10 08:52:41');

-- --------------------------------------------------------

--
-- Table structure for table `money_type`
--

CREATE TABLE `money_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `money_type`
--

INSERT INTO `money_type` (`id`, `name`, `description`, `date_created`, `date_updated`) VALUES
(1, 'user_real', 'this is user\'s real money. can withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00'),
(2, 'voucher', 'this money is gotten from certain commissions. cannot withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `nextlevel`
--

CREATE TABLE `nextlevel` (
  `id` bigint(20) NOT NULL,
  `couple_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `uid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nextlevel`
--

INSERT INTO `nextlevel` (`id`, `couple_id`, `uid`, `status`, `username`, `fullname`, `date_created`, `date_updated`) VALUES
(1, '9dc4937f65ecc19062f98f0b9c999833', '7da984ec4697918cd5e0068b5b7acb9f', 'ACCEPTED', 'TJ WenJie', 'WenJie', '2019-11-09 15:18:38', '2019-11-09 15:18:38'),
(2, 'fb03655129d7e83c73834b200a3aaa87', '7da984ec4697918cd5e0068b5b7acb9f', 'ACCEPTED', 'TJ WenJiesad', 'WenJieasd', '2019-11-09 16:07:01', '2019-11-09 16:07:01'),
(3, '25fa2790d079e42e339b7b26dd2eb28b', 'b78ebeec22c211db362599c66d64b13b', 'ACCEPTED', 'asd', 'asdasd', '2019-11-10 08:04:45', '2019-11-10 08:04:45'),
(4, '0511f20e8e38e0994252f24dc853697d', 'b78ebeec22c211db362599c66d64b13b', 'ACCEPTED', 'asd', 'asdasd', '2019-11-10 08:10:24', '2019-11-10 08:10:24');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(5000) NOT NULL,
  `price` decimal(50,0) NOT NULL COMMENT 'With MYR as base and is in point system (RM1 = 100 point)',
  `stock` int(255) NOT NULL,
  `buy_stock` int(255) NOT NULL,
  `total_price` int(255) NOT NULL,
  `display` int(20) DEFAULT 1,
  `type` int(255) NOT NULL DEFAULT 1 COMMENT '1 = normal product',
  `description` varchar(10000) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `stock`, `buy_stock`, `total_price`, `display`, `type`, `description`, `images`, `date_created`, `date_updated`) VALUES
(1, 'Oil Booster 3pcs (1 set)', '300', 1000, 0, 0, 1, 1, 'OIL BOOSTER 3PCS (1 SET)', 'DCK-Engine-Oil-Booster.png', '2019-07-24 09:17:04', '2019-12-19 02:38:18'),
(2, 'Motorcycle Oil Booster 15pcs (1 set)', '300', 1000, 0, 0, 0, 1, 'Motorcycle Oil Booster 15pcs (1 set)', '', '2019-12-19 02:38:39', '2019-12-19 03:01:12'),
(3, 'Synthetic Plus Engine Oil 5w-30 (1 set)', '300', 1000, 0, 0, 1, 1, 'SYNTHETIC PLUS ENGINE OIL 5W-30 (1 SET)', '5w.png', '2019-07-25 04:11:21', '2019-12-19 02:40:11'),
(4, 'Synthetic Plus Engine Oil 10w-40  (1 set)', '300', 1000, 0, 0, 1, 1, 'SYNTHETIC PLUS ENGINE OIL 10W-40  (1 SET)', '10w.png', '2019-10-29 06:18:28', '2019-12-19 02:40:14');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `productid` int(255) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` int(10) NOT NULL,
  `final_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `original_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `discount_given` decimal(50,0) NOT NULL COMMENT 'in points',
  `totalProductPrice` decimal(50,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `referral_bonus` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `conversion_point` int(11) NOT NULL,
  `charges_withdraw` int(11) NOT NULL,
  `point_voucher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `uid`, `referral_bonus`, `commission`, `conversion_point`, `charges_withdraw`, `point_voucher`) VALUES
(1, '', 20, 20, 20, 10, 100);

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, 'e809ed5b38535157bf9a163fa1225ade', 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:03:49', '2019-11-29 06:03:49'),
(2, 'e809ed5b38535157bf9a163fa1225ade', 'dabdeb40d42d9d281ae442fcccd93895', 'young', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:03:49', '2019-11-29 06:03:49'),
(3, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'ef087c3d140821babc712b7094f53684', 'busquest', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:04:01', '2019-11-29 06:04:01'),
(4, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:05:06', '2019-11-29 06:05:06'),
(5, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:05:25', '2019-11-29 06:05:25'),
(6, 'ef087c3d140821babc712b7094f53684', 'df4fe5576da5c64c8103857cd7df757b', 'suarez', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:06:25', '2019-11-29 06:06:25'),
(7, 'ef087c3d140821babc712b7094f53684', 'def0bb9754e106d0ca569c262ac3b655', 'dembele', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:07:12', '2019-11-29 06:07:12'),
(8, 'ef087c3d140821babc712b7094f53684', 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:07:52', '2019-11-29 06:07:52'),
(9, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'a6d41a96d021e1509c7ee16aa7f8ab26', 'rakitic', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:08:23', '2019-11-29 06:08:23'),
(10, 'c4a22bf1f4eb8837d8b5b1080f51e579', '9a74193dc0cc524453d2772417af3c0d', 'arthur', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:08:46', '2019-11-29 06:08:46'),
(11, 'c4a22bf1f4eb8837d8b5b1080f51e579', '10686d6d80b70cc994f7bae05a28f6b2', 'alena', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:09:24', '2019-11-29 06:09:24'),
(12, 'c4a22bf1f4eb8837d8b5b1080f51e579', '75d7c0ea4bf9fa0c91cd890a30f5f65c', 'dejong', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:09:44', '2019-11-29 06:09:44'),
(13, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'c34fdaf91eebf2aa115d1ca8d17b6ed4', 'vidal', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:10:13', '2019-11-29 06:10:13'),
(14, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '339ddead7419d1b70fe07b833182fe92', 'terstegen', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:10:46', '2019-11-29 06:10:46'),
(15, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '48a48715ca08f5eacb5c616351ba9220', 'neto', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:11:32', '2019-11-29 06:11:32'),
(16, 'b2743fde93dbfeae47115ef22a92d2bd', 'a1cba8835408386d48bf0b01ab7a57a3', 'semedo', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:13:04', '2019-11-29 06:13:04'),
(17, 'b2743fde93dbfeae47115ef22a92d2bd', '4095edfbc7b3e30174bb8707d84e0119', 'todibo', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:13:28', '2019-11-29 06:13:28'),
(18, 'b2743fde93dbfeae47115ef22a92d2bd', 'd59bff2cc57dc3b48be3c53dbf96735d', 'lenglet', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:13:53', '2019-11-29 06:13:53'),
(19, 'b2743fde93dbfeae47115ef22a92d2bd', 'a83067dabe368e9d48d5ac578eb53b96', 'wague', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:14:13', '2019-11-29 06:14:13'),
(20, 'b2743fde93dbfeae47115ef22a92d2bd', '045384a79e7cdda062ae1716730d3434', 'alba', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:14:33', '2019-11-29 06:14:33'),
(21, 'b2743fde93dbfeae47115ef22a92d2bd', '6135518fd0850893ff83caee61edce93', 'umtiti', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:14:56', '2019-11-29 06:14:56'),
(22, 'b2743fde93dbfeae47115ef22a92d2bd', 'bc7f79073712cb70eac8f4662aad476b', 'firpo', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:15:14', '2019-11-29 06:15:14'),
(23, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '732a673bd0b13f8d11eee452d3350b61', 'aguero', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:17:01', '2019-11-29 06:17:01'),
(24, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '04bb6ca95fadf5296af7e5605c31e251', 'martinez', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:17:31', '2019-11-29 06:17:31'),
(25, 'bf7f773e30cc5e703b5d432dc42725a6', '1e586a9d860f507633417b6f20e5f404', 'lemar', 4, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:33:29', '2019-11-29 06:33:29'),
(26, 'bf7f773e30cc5e703b5d432dc42725a6', '032111d99e62c8cfa6df496e1e8abce3', 'giroud', 4, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:33:45', '2019-11-29 06:33:45'),
(27, 'bf7f773e30cc5e703b5d432dc42725a6', '57a287c2e6abba677a74323553233922', 'mbappe', 4, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:34:00', '2019-11-29 06:34:00'),
(28, 'bf7f773e30cc5e703b5d432dc42725a6', 'd922522d8a45daa000c8ffe4ef401e50', 'coman', 4, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:34:18', '2019-11-29 06:34:18'),
(29, 'bf7f773e30cc5e703b5d432dc42725a6', '9e0cfb3ed64d1ec730f7cc1675074dcd', 'fekir', 4, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:34:33', '2019-11-29 06:34:33'),
(30, 'bf7f773e30cc5e703b5d432dc42725a6', '9c2593f8f987185da63ecb6e47143c80', 'yedder', 4, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:34:55', '2019-11-29 06:34:55'),
(31, 'ef087c3d140821babc712b7094f53684', '6cf790523be5a7b0761ca3cb7b7b051f', 'navas', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:41:03', '2019-11-29 06:41:03'),
(32, 'ef087c3d140821babc712b7094f53684', '37707d7e1f99de1abb4caec943909e46', 'bernat', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:41:34', '2019-11-29 06:41:34'),
(33, 'ef087c3d140821babc712b7094f53684', 'e10702bc81c2c7235cc5d847b8c9ecf9', 'carvajal', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:41:57', '2019-11-29 06:41:57'),
(34, '57a287c2e6abba677a74323553233922', '1d4b527c436b211d12aa099543ddf651', 'neymar', 5, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:43:39', '2019-11-29 06:43:39'),
(35, '57a287c2e6abba677a74323553233922', '88be5665f6b84f174474b489dbfab274', 'icardi', 5, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:44:18', '2019-11-29 06:44:18'),
(36, '57a287c2e6abba677a74323553233922', '5e761a85525cc1ea7a1e555f1c1f05a6', 'cavani', 5, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:44:44', '2019-11-29 06:44:44'),
(37, '57a287c2e6abba677a74323553233922', '46636898c58a74a046ec10d2bd551120', 'maria', 5, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:45:47', '2019-11-29 06:45:47'),
(38, '57a287c2e6abba677a74323553233922', '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', 5, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:46:25', '2019-11-29 06:46:25'),
(39, '57a287c2e6abba677a74323553233922', '00cba3f7907d3d6b7e6114536cd46f7e', 'silva', 5, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:46:44', '2019-11-29 06:46:44'),
(40, '76fbc85cd055a85e5ee6c737e8df2116', '5369c3bd5d853e898324d0b633a85cd1', 'kimmich', 6, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 07:52:07', '2019-11-29 07:52:07'),
(41, '76fbc85cd055a85e5ee6c737e8df2116', 'ed3b4fb32872ad093139167be8657970', 'kroos', 6, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 07:52:27', '2019-11-29 07:52:27'),
(42, '76fbc85cd055a85e5ee6c737e8df2116', '9badb66c2b36a6466c6301f4cd8e9fed', 'brandt', 6, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 07:52:47', '2019-11-29 07:52:47'),
(43, '76fbc85cd055a85e5ee6c737e8df2116', 'bf34de0101450f0170addc5de18548c2', 'amiri', 6, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:09:10', '2019-11-29 08:09:10'),
(44, '76fbc85cd055a85e5ee6c737e8df2116', '754f798686c8884c3634f8282d9107c1', 'serder', 6, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:09:31', '2019-11-29 08:09:31'),
(45, '76fbc85cd055a85e5ee6c737e8df2116', '5b8a48e35b7618a9b8c89ceb07a171f1', 'rudy', 6, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:09:53', '2019-11-29 08:09:53'),
(46, 'ed3b4fb32872ad093139167be8657970', '91fef392a2b723f933814d014d06579a', 'ramos', 7, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:13:22', '2019-11-29 08:13:22'),
(47, 'ed3b4fb32872ad093139167be8657970', 'e393d75096300eff3f573b453e043eb3', 'varane', 7, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:13:48', '2019-11-29 08:13:48'),
(48, 'ed3b4fb32872ad093139167be8657970', '2cb58fb99c7066d9d1d2910ca02d167c', 'benzema', 7, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:14:09', '2019-11-29 08:14:09'),
(49, 'ed3b4fb32872ad093139167be8657970', '33a18846dc5953b1f0fa1e0a2890be42', 'modric', 7, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:14:39', '2019-11-29 08:14:39'),
(50, 'ed3b4fb32872ad093139167be8657970', '0ee4baed4a1ce967f86a2b7ca8d53c28', 'bale', 7, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:15:52', '2019-11-29 08:15:52'),
(51, 'ed3b4fb32872ad093139167be8657970', '0118132cc1a056cd15b9c8944be0a40e', 'james', 7, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:16:42', '2019-11-29 08:16:42'),
(52, '0118132cc1a056cd15b9c8944be0a40e', 'c7fd51dec157bcaff0475c314712c12d', 'zapata', 8, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:20:35', '2019-11-29 08:20:35'),
(53, '0118132cc1a056cd15b9c8944be0a40e', '0573e82d5020ded400e799170e947467', 'franco', 8, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:20:56', '2019-11-29 08:20:56'),
(54, '0118132cc1a056cd15b9c8944be0a40e', '4ada769fe9a06812873c42caaa33151e', 'arias', 8, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:21:15', '2019-11-29 08:21:15'),
(55, '0118132cc1a056cd15b9c8944be0a40e', 'a4e2202c9e72b361caba9a6eb64e82b8', 'valencia', 8, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:22:03', '2019-11-29 08:22:03'),
(56, '0118132cc1a056cd15b9c8944be0a40e', '9ffec7a0823a30544d5ea04219e4cc55', 'sanchez', 8, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:22:21', '2019-11-29 08:22:21'),
(57, '0118132cc1a056cd15b9c8944be0a40e', '97f250ed395ed79c01b335f8eb2c5678', 'falcao', 8, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 08:22:58', '2019-11-29 08:22:58'),
(58, '1d4b527c436b211d12aa099543ddf651', '35785d3e80c8d9d2b8a8511698c834da', 'allison', 6, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-04 06:23:30', '2019-12-04 06:23:30'),
(59, '1d4b527c436b211d12aa099543ddf651', '76a7e9111582c2f386b8e302f5879f94', 'ederson', 6, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-04 06:26:46', '2019-12-04 06:26:46'),
(60, '1d4b527c436b211d12aa099543ddf651', '6ea0d772f06cee30142b86217ea29613', 'jesus', 6, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-04 06:28:30', '2019-12-04 06:28:30');

-- --------------------------------------------------------

--
-- Table structure for table `signup_product`
--

CREATE TABLE `signup_product` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referrer_name` varchar(255) DEFAULT NULL,
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `referral_fullname` varchar(255) NOT NULL,
  `product` varchar(255) NOT NULL,
  `price` int(255) NOT NULL DEFAULT 300,
  `quantity` int(255) NOT NULL DEFAULT 1,
  `total` int(255) NOT NULL DEFAULT 300,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `signup_product`
--

INSERT INTO `signup_product` (`id`, `referrer_id`, `referrer_name`, `referral_id`, `referral_name`, `referral_fullname`, `product`, `price`, `quantity`, `total`, `date_created`, `date_updated`) VALUES
(1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'ef087c3d140821babc712b7094f53684', 'busquest', 'Sergio Busquest', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:04:01', '2019-11-29 06:04:01'),
(2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', 'Gerard Pique', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:05:06', '2019-11-29 06:05:06'),
(3, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'Sergi Roberto', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 06:05:25', '2019-11-29 06:05:25'),
(4, 'ef087c3d140821babc712b7094f53684', 'busquest', 'df4fe5576da5c64c8103857cd7df757b', 'suarez', 'Luis Suarez', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:06:25', '2019-11-29 06:06:25'),
(5, 'ef087c3d140821babc712b7094f53684', 'busquest', 'def0bb9754e106d0ca569c262ac3b655', 'dembele', 'Ousmane Dembee', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:07:12', '2019-11-29 06:07:12'),
(6, 'ef087c3d140821babc712b7094f53684', 'busquest', 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', 'Antonie Griezmann', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 06:07:52', '2019-11-29 06:07:52'),
(7, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', 'a6d41a96d021e1509c7ee16aa7f8ab26', 'rakitic', 'Ivan Rakitic', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:08:23', '2019-11-29 06:08:23'),
(8, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', '9a74193dc0cc524453d2772417af3c0d', 'arthur', 'Arthur Melo', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:08:46', '2019-11-29 06:08:46'),
(9, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', '10686d6d80b70cc994f7bae05a28f6b2', 'alena', 'Carles Alena', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 06:09:24', '2019-11-29 06:09:24'),
(10, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', '75d7c0ea4bf9fa0c91cd890a30f5f65c', 'dejong', 'Frankie de Jong', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-29 06:09:44', '2019-11-29 06:09:44'),
(11, 'c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', 'c34fdaf91eebf2aa115d1ca8d17b6ed4', 'vidal', 'Arturo Vidal', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:10:13', '2019-11-29 06:10:13'),
(12, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '339ddead7419d1b70fe07b833182fe92', 'terstegen', 'Marc Andre ter Stegen', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-29 06:10:46', '2019-11-29 06:10:46'),
(13, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '48a48715ca08f5eacb5c616351ba9220', 'neto', 'Norberto Murara Neto', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:11:32', '2019-11-29 06:11:32'),
(14, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'a1cba8835408386d48bf0b01ab7a57a3', 'semedo', 'Nelson Semedo', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:13:04', '2019-11-29 06:13:04'),
(15, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', '4095edfbc7b3e30174bb8707d84e0119', 'todibo', 'Jean Clair Todibo', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:13:28', '2019-11-29 06:13:28'),
(16, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'd59bff2cc57dc3b48be3c53dbf96735d', 'lenglet', 'Clement Lenglet', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 06:13:53', '2019-11-29 06:13:53'),
(17, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'a83067dabe368e9d48d5ac578eb53b96', 'wague', 'Moussa Wague', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-29 06:14:13', '2019-11-29 06:14:13'),
(18, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', '045384a79e7cdda062ae1716730d3434', 'alba', 'Jordi Alba', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:14:33', '2019-11-29 06:14:33'),
(19, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', '6135518fd0850893ff83caee61edce93', 'umtiti', 'Samuel Umtiti', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:14:56', '2019-11-29 06:14:56'),
(20, 'b2743fde93dbfeae47115ef22a92d2bd', 'roberto', 'bc7f79073712cb70eac8f4662aad476b', 'firpo', 'Junior Firpo', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 06:15:14', '2019-11-29 06:15:14'),
(21, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '732a673bd0b13f8d11eee452d3350b61', 'aguero', 'Sergio Aguero', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:17:01', '2019-11-29 06:17:01'),
(22, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '04bb6ca95fadf5296af7e5605c31e251', 'martinez', 'Lautaro Martinez', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:17:31', '2019-11-29 06:17:31'),
(23, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '1e586a9d860f507633417b6f20e5f404', 'lemar', 'Thomas Lemar', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:33:29', '2019-11-29 06:33:29'),
(24, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '032111d99e62c8cfa6df496e1e8abce3', 'giroud', 'Oliver Giroud', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:33:45', '2019-11-29 06:33:45'),
(25, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '57a287c2e6abba677a74323553233922', 'mbappe', 'Kylian Mbappe', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 06:34:00', '2019-11-29 06:34:00'),
(26, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', 'd922522d8a45daa000c8ffe4ef401e50', 'coman', 'Kingsley Coman', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-29 06:34:18', '2019-11-29 06:34:18'),
(27, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '9e0cfb3ed64d1ec730f7cc1675074dcd', 'fekir', 'Nabil Fekir', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:34:33', '2019-11-29 06:34:33'),
(28, 'bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', '9c2593f8f987185da63ecb6e47143c80', 'yedder', 'Wissam Ben Yedder', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:34:55', '2019-11-29 06:34:55'),
(29, 'ef087c3d140821babc712b7094f53684', 'busquest', '6cf790523be5a7b0761ca3cb7b7b051f', 'navas', 'Jesus Navas', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:41:03', '2019-11-29 06:41:03'),
(30, 'ef087c3d140821babc712b7094f53684', 'busquest', '37707d7e1f99de1abb4caec943909e46', 'bernat', 'Juan Bernat', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 06:41:34', '2019-11-29 06:41:34'),
(31, 'ef087c3d140821babc712b7094f53684', 'busquest', 'e10702bc81c2c7235cc5d847b8c9ecf9', 'carvajal', 'Dani Carvajal', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 06:41:57', '2019-11-29 06:41:57'),
(32, '57a287c2e6abba677a74323553233922', 'mbappe', '1d4b527c436b211d12aa099543ddf651', 'neymar', 'Neymar Jr', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:43:39', '2019-11-29 06:43:39'),
(33, '57a287c2e6abba677a74323553233922', 'mbappe', '88be5665f6b84f174474b489dbfab274', 'icardi', 'Mauro Icardi', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:44:18', '2019-11-29 06:44:18'),
(34, '57a287c2e6abba677a74323553233922', 'mbappe', '5e761a85525cc1ea7a1e555f1c1f05a6', 'cavani', 'Edison Cavani', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 06:44:44', '2019-11-29 06:44:44'),
(35, '57a287c2e6abba677a74323553233922', 'mbappe', '46636898c58a74a046ec10d2bd551120', 'maria', 'Angel Di Maria', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-29 06:45:47', '2019-11-29 06:45:47'),
(36, '57a287c2e6abba677a74323553233922', 'mbappe', '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', 'Julian Draxler', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:46:25', '2019-11-29 06:46:25'),
(37, '57a287c2e6abba677a74323553233922', 'mbappe', '00cba3f7907d3d6b7e6114536cd46f7e', 'silva', 'Thiago Silva', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 06:46:44', '2019-11-29 06:46:44'),
(38, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '5369c3bd5d853e898324d0b633a85cd1', 'kimmich', 'Joshua Kimmich', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 07:52:07', '2019-11-29 07:52:07'),
(39, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', 'ed3b4fb32872ad093139167be8657970', 'kroos', 'Toni Kroos', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 07:52:27', '2019-11-29 07:52:27'),
(40, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '9badb66c2b36a6466c6301f4cd8e9fed', 'brandt', 'Julian Brandt', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 07:52:47', '2019-11-29 07:52:47'),
(41, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', 'bf34de0101450f0170addc5de18548c2', 'amiri', 'Nadiem Amiri', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-29 08:09:10', '2019-11-29 08:09:10'),
(42, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '754f798686c8884c3634f8282d9107c1', 'serder', 'Suat Serder', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-29 08:09:31', '2019-11-29 08:09:31'),
(43, '76fbc85cd055a85e5ee6c737e8df2116', 'draxler', '5b8a48e35b7618a9b8c89ceb07a171f1', 'rudy', 'Sebastian Rudy', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 08:09:53', '2019-11-29 08:09:53'),
(44, 'ed3b4fb32872ad093139167be8657970', 'kroos', '91fef392a2b723f933814d014d06579a', 'ramos', 'Sergio Ramos', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-29 08:13:22', '2019-11-29 08:13:22'),
(45, 'ed3b4fb32872ad093139167be8657970', 'kroos', 'e393d75096300eff3f573b453e043eb3', 'varane', 'Raphael Varane', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 08:13:48', '2019-11-29 08:13:48'),
(46, 'ed3b4fb32872ad093139167be8657970', 'kroos', '2cb58fb99c7066d9d1d2910ca02d167c', 'benzema', 'Karim Benzema', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 08:14:09', '2019-11-29 08:14:09'),
(47, 'ed3b4fb32872ad093139167be8657970', 'kroos', '33a18846dc5953b1f0fa1e0a2890be42', 'modric', 'Luka Modric', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 08:14:39', '2019-11-29 08:14:39'),
(48, 'ed3b4fb32872ad093139167be8657970', 'kroos', '0ee4baed4a1ce967f86a2b7ca8d53c28', 'bale', 'Gareth Bale', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 08:15:52', '2019-11-29 08:15:52'),
(49, 'ed3b4fb32872ad093139167be8657970', 'kroos', '0118132cc1a056cd15b9c8944be0a40e', 'james', 'James Rodriguez', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 08:16:42', '2019-11-29 08:16:42'),
(50, '0118132cc1a056cd15b9c8944be0a40e', 'james', 'c7fd51dec157bcaff0475c314712c12d', 'zapata', 'Cristan Zapata', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-29 08:20:35', '2019-11-29 08:20:35'),
(51, '0118132cc1a056cd15b9c8944be0a40e', 'james', '0573e82d5020ded400e799170e947467', 'franco', 'Pedro Franco', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-29 08:20:56', '2019-11-29 08:20:56'),
(52, '0118132cc1a056cd15b9c8944be0a40e', 'james', '4ada769fe9a06812873c42caaa33151e', 'arias', 'Santiago Arias', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 08:21:15', '2019-11-29 08:21:15'),
(53, '0118132cc1a056cd15b9c8944be0a40e', 'james', 'a4e2202c9e72b361caba9a6eb64e82b8', 'valencia', 'Edwin Valencia', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 08:22:03', '2019-11-29 08:22:03'),
(54, '0118132cc1a056cd15b9c8944be0a40e', 'james', '9ffec7a0823a30544d5ea04219e4cc55', 'sanchez', 'Carlos Sanchez', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-29 08:22:21', '2019-11-29 08:22:21'),
(55, '0118132cc1a056cd15b9c8944be0a40e', 'james', '97f250ed395ed79c01b335f8eb2c5678', 'falcao', 'Radamel Falcao', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-29 08:22:58', '2019-11-29 08:22:58'),
(56, '1d4b527c436b211d12aa099543ddf651', 'neymar', '35785d3e80c8d9d2b8a8511698c834da', 'allison', 'Allison Becker', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-12-04 06:23:30', '2019-12-04 06:23:30'),
(57, '1d4b527c436b211d12aa099543ddf651', 'neymar', '76a7e9111582c2f386b8e302f5879f94', 'ederson', 'Ederson Moraes', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-12-04 06:26:46', '2019-12-04 06:26:46'),
(58, '1d4b527c436b211d12aa099543ddf651', 'neymar', '6ea0d772f06cee30142b86217ea29613', 'jesus', 'Gabriel Jesus', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-12-04 06:28:30', '2019-12-04 06:28:30');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE `transaction_history` (
  `id` bigint(20) NOT NULL,
  `money_in` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'in points',
  `money_out` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'in points',
  `uid` varchar(255) NOT NULL,
  `target_uid` varchar(255) DEFAULT NULL COMMENT 'this is the uid for the targeted user. it can be a downline''s uid because upline can get commissions from his downline. therefore this target_uid shall be downline''s uid while the uid field above will be upline''s uid',
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'The percentage assigned to this transaction. Usually for commissions use only',
  `original_value` decimal(50,0) DEFAULT NULL COMMENT '(in point)the original value that has been given before calculating the percentage. Usually for commissions use only',
  `status` int(3) DEFAULT NULL COMMENT '1 = pending, 2 = accepted/completed, 3 = rejected, NULL = nothing',
  `level` int(10) DEFAULT NULL COMMENT 'can be group sales commission given by a downline at a level',
  `order_id` bigint(20) DEFAULT NULL COMMENT 'refers to a particular order in order table and it refers to another table that lists out all the products and quantity purchased in that table with order_id as foreign key',
  `transaction_type_id` int(255) NOT NULL,
  `money_type_id` int(255) DEFAULT NULL,
  `source_transaction_id` bigint(20) DEFAULT NULL COMMENT 'this is referring to this table''s own id. Might be needed when some previous transaction is triggering new transactions',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE `transaction_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'Percentage of the transaction, for example if transaction is withdrawal type, user need give 0.5% of total',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`id`, `name`, `description`, `percentage`, `date_created`, `date_updated`) VALUES
(1, 'referral', 'user can get commission by referring people and the referred person must at least buy a product for the referral to get this commission', NULL, '2019-07-25 08:46:07', '2019-07-25 08:46:07'),
(2, 'group_sales', 'This is when the user\'s downline bought a product, he will get a percentage from it. up to 9 levels with each level having different percentage. please refer to group_commission table', NULL, '2019-07-25 09:15:08', '2019-07-25 09:15:08'),
(3, 'buy_product', 'This is when user purchase a product', NULL, '2019-07-26 01:35:16', '2019-07-26 01:35:16'),
(4, 'transfer', 'point transfer between members', '0.25', '2019-07-31 02:46:22', '2019-07-31 02:46:22'),
(5, 'withdraw', 'withdraw money and transfer to bank', '0.50', '2019-07-31 02:46:22', '2019-07-31 02:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_point`
--

CREATE TABLE `transfer_point` (
  `id` int(11) NOT NULL,
  `send_uid` varchar(100) NOT NULL,
  `send_name` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `receive_name` varchar(100) NOT NULL,
  `receive_uid` varchar(100) NOT NULL,
  `create_date` timestamp(3) NOT NULL DEFAULT current_timestamp(3),
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transfer_point`
--

INSERT INTO `transfer_point` (`id`, `send_uid`, `send_name`, `amount`, `receive_name`, `receive_uid`, `create_date`, `status`) VALUES
(1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 6000, 'griezmann', 'bf7f773e30cc5e703b5d432dc42725a6', '2019-11-29 06:31:21.677', 'RECEIVED'),
(2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 250, 'busquest', 'ef087c3d140821babc712b7094f53684', '2019-12-09 03:18:04.010', 'RECEIVED');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `epin` char(64) DEFAULT NULL,
  `salt_epin` char(64) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `downline_accumulated_points` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'RM1 = 100 point',
  `can_send_newsletter` tinyint(1) NOT NULL DEFAULT 0,
  `is_referred` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `car_model` varchar(200) DEFAULT NULL,
  `car_year` int(20) DEFAULT NULL,
  `picture_id` varchar(50) DEFAULT NULL,
  `register_downline_no` varchar(255) DEFAULT '0',
  `bonus` varchar(255) DEFAULT NULL,
  `final_amount` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `country_id`, `full_name`, `epin`, `salt_epin`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `login_type`, `user_type`, `downline_accumulated_points`, `can_send_newsletter`, `is_referred`, `date_created`, `date_updated`, `address`, `birthday`, `gender`, `bank_name`, `bank_account_holder`, `bank_account_no`, `car_model`, `car_year`, `picture_id`, `register_downline_no`, `bonus`, `final_amount`, `point`) VALUES
('00cba3f7907d3d6b7e6114536cd46f7e', 'silva', NULL, 'ad32fe34cdaa83244b5a63653a43e79e511ecc7446470ed203bf6f7520c1b172', '31baa4d11aca90e868a196bf133a2cd747098fc4', NULL, '020303', NULL, 'Thiago Silva', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:46:44', '2019-11-29 06:46:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('0118132cc1a056cd15b9c8944be0a40e', 'james', NULL, '7db9329fc1bf25c40251216ecc3b9e93e00db59e32246f426d6dd307d29dfdbe', '7790c1b026ab650e0934cc87c8868c67736b6550', '098765', '14151016', NULL, 'James Rodriguez', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:16:42', '2019-11-29 09:27:37', NULL, NULL, NULL, 'HONG LEONG BANK BERHAD', 'James Rodriguez', 32322222, NULL, NULL, NULL, '6', '500', '500', '600'),
('032111d99e62c8cfa6df496e1e8abce3', 'giroud', NULL, '6c1f80c2c940bf5c55293f8a6fb6b9a2333f6c09fd39f1540c8fd4fa4d6e6e3c', '58f82fe247b5a779ab52e93b41f6455b02a06539', NULL, '110909', NULL, 'Oliver Giroud', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:33:45', '2019-11-29 06:33:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('045384a79e7cdda062ae1716730d3434', 'alba', NULL, '258af93284bc2fcccbc567b0ae9396de1cd8948ef707e40dfabfd4a03a6cd91e', '9c90b4dc4e339e9ea53720f0c65fb3d8e9d937df', NULL, '1818111888', NULL, 'Jordi Alba', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:14:33', '2019-11-29 06:14:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('04bb6ca95fadf5296af7e5605c31e251', 'martinez', NULL, '3f44acafc03d55642a235eaad9c1beebf36b7eac12f1c922f004c2eadd70f24a', '84c18c7a592c37abfd7e4ca3ee51f57195eb389e', NULL, '10102222', NULL, 'Lautaro Martinez', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:17:31', '2019-11-29 06:17:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('0573e82d5020ded400e799170e947467', 'franco', NULL, '447e0f9d3297401831135ceedbd1043bd986723b87be4a077cf52d2214d15760', 'bd40040571ceee22e24eb44dde52af88037541cd', NULL, '15030303', NULL, 'Pedro Franco', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:20:56', '2019-11-29 08:20:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('0ee4baed4a1ce967f86a2b7ca8d53c28', 'bale', NULL, '21b92f33815af8db221083b3d3379b9ed26c75289127219eaf8bb5ae8faf45d2', 'bea8d11abcbc1b47806da346ead9376193d651ae', NULL, '15161711', NULL, 'Gareth Bale', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:15:52', '2019-11-29 08:15:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('10686d6d80b70cc994f7bae05a28f6b2', 'alena', NULL, '272306cdddb5a83c0bbcf92351650e1b366ec48f27fffbc84c13f771f6f40c38', '16f9816ecc603088a0adffd7f7ac8f2b2e14076a', NULL, '1919111999', NULL, 'Carles Alena', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:09:24', '2019-11-29 06:09:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('1d4b527c436b211d12aa099543ddf651', 'neymar', NULL, 'f55a6ed69ec4390574df4145ab26fec795239008b8aceabb68ffcf1df4f70447', '5e74e0b722c2287353ef72cf003dee36b63432e8', NULL, '021010', NULL, 'Neymar Jr', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:43:39', '2019-12-04 06:28:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3', '350', '350', NULL),
('1e586a9d860f507633417b6f20e5f404', 'lemar', NULL, 'd4033d284aa15575d1d2db2b8fd0d46406cf0fff3f911fbf8805003b79e391c4', 'da1a2b0f9533be5b30bbefd8b4cbcf0b91cdd271', NULL, '110808', NULL, 'Thomas Lemar', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:33:29', '2019-11-29 06:33:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('2cb58fb99c7066d9d1d2910ca02d167c', 'benzema', NULL, '615beed0a4b4424369898160dde13b65115e20ed792a710f1c66e8107e3f6a58', '20b18f6613dd8e92a93d822391bd9562756a6450', NULL, '180909', NULL, 'Karim Benzema', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:14:09', '2019-11-29 08:14:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('339ddead7419d1b70fe07b833182fe92', 'terstegen', NULL, '098e89d037268efa11086c48c635472e5e943735e2afc37f52bf01469cd0b3c4', '3ca8f7ce7fc7ad6d5c1ad278625c609193a4a5de', NULL, '01011313', NULL, 'Marc Andre ter Stegen', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:10:46', '2019-11-29 06:10:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('33a18846dc5953b1f0fa1e0a2890be42', 'modric', NULL, '999e1697836206ad2c41f377d51f931c52acf0e6e732cc968f0e3844b875bd62', 'ad198877862138f0951bcbffeacbbba46b7b4455', NULL, '18021010', NULL, 'Luka Modric', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:14:39', '2019-11-29 08:14:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('35785d3e80c8d9d2b8a8511698c834da', 'allison', NULL, '362cce959921d5433a815e3061ddd4b96e815ef0f0e0132733a4f9ac74aeb6a5', '5d9494a220a8060127bb63d45ccad4a2507538df', NULL, '13130101', NULL, 'Allison Becker', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-04 06:23:30', '2019-12-04 06:23:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('37707d7e1f99de1abb4caec943909e46', 'bernat', NULL, 'ed2a73689def641f5603e6309a972b0f4934555dd63fd6dca5d8c2ab6e2c1718', '7858353b4d5a42f033ccf0c360805e2386e520ae', NULL, '101414', NULL, 'Juan Bernat', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:41:34', '2019-11-29 06:41:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('4095edfbc7b3e30174bb8707d84e0119', 'todibo', NULL, '516bc734b74489da0ced4591b8585bed46201111701e9ec9dc250a8097ed7870', 'a58668e340ec810cc3643be713eca7c82d51d3de', NULL, '0606000666', NULL, 'Jean Clair Todibo', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:13:28', '2019-11-29 06:13:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('46636898c58a74a046ec10d2bd551120', 'maria', NULL, '0866e6d336b1c32a2b0dd7e19d6e1d372b31b7bb24144972699e1c9ad137f0ed', '624e93569eb91ad4b049e0597c24f2dadf0723c2', NULL, '140707', NULL, 'Angel Di Maria', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:45:47', '2019-11-29 06:45:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('48a48715ca08f5eacb5c616351ba9220', 'neto', NULL, '934febbf3dc58b7d2e29c6da2f53850da7018554146043e7fcc90f7cbc16c91d', '139dea36da25a71a16bfdd381b2b052e273b5442', NULL, '1313111333', NULL, 'Norberto Murara Neto', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:11:32', '2019-11-29 06:11:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('4ada769fe9a06812873c42caaa33151e', 'arias', NULL, '7e73da2c670d3d55f4369256a94af3368746bba47d100dde1db81691b7527513', 'e54b9e6fb08bc3c358c4f87f529c62ad20cdf2a5', NULL, '15040404', NULL, 'Santiago Arias', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:21:15', '2019-11-29 08:21:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('5369c3bd5d853e898324d0b633a85cd1', 'kimmich', NULL, '915e369135c9d9ec021ac9d24dccd213466d06d0f3285bf8e083e317ffdc0ade', '7bdd5204cf7a4b11ac0da414de3d89a85f504a47', NULL, '140606', NULL, 'Joshua Kimmich', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 07:52:07', '2019-11-29 07:52:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('57a287c2e6abba677a74323553233922', 'mbappe', NULL, '1055cb55e490289dd7eb72bc468d0b2ef05e17a0c50c8d4dca8da27f59e2f00b', 'd58942b3c90283774c43518cbc32c345c7c7db02', '33445577', '111010', NULL, 'Kylian Mbappe', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:34:00', '2019-12-04 06:28:30', NULL, NULL, NULL, 'HSBC BANK MALAYSIA BERHAD', 'Kylian Mbappe', 3434567, NULL, NULL, NULL, '6', '590', '590', '2100'),
('5b8a48e35b7618a9b8c89ceb07a171f1', 'rudy', NULL, '085fd129645f85439959474a1e41a1b51d1fbb126b745d0807b8896ece5f0261', 'ca6a37d8db719cd571bf0747ca02b80b2575d5d1', NULL, '141616', NULL, 'Sebastian Rudy', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:09:53', '2019-11-29 08:09:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('5e761a85525cc1ea7a1e555f1c1f05a6', 'cavani', NULL, '460e1328facd69946e34f3ec3139469699a93f7016641594389298fedb65dde0', 'bd8f95a1ce450c39ec55a6b89d87968c01b63afe', NULL, '300909', NULL, 'Edison Cavani', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:44:44', '2019-11-29 06:44:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('6135518fd0850893ff83caee61edce93', 'umtiti', NULL, 'b9f8941699c2efdf7ca64d799e034d02e0e59beeca6fca6da998ea31bb8921d2', 'c8a6893eae6621cd44358281b4a95a13e864d7a6', NULL, '2323222333', NULL, 'Samuel Umtiti', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:14:56', '2019-11-29 06:14:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('6cf790523be5a7b0761ca3cb7b7b051f', 'navas', NULL, '94784d3687b7e23552d55d6367b08d20cc8c8cc1320e5215583f1cee74c7d152', 'd38b33f63ef3dedea28da60c2be62b5046c69c94', NULL, '102222', NULL, 'Jesus Navas', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:41:03', '2019-11-29 06:41:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('6ea0d772f06cee30142b86217ea29613', 'jesus', 'jesus31.mc@gmail.com', '9a17594989fc2989f8a55c5a656decd0e534980cb01a8098fe92ccd5601ed9be', '7fdc35aad2de9cea936aab7dafa1c2ac5a4ebaf9', NULL, '33330909', NULL, 'Gabriel Jesus', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-04 06:28:30', '2019-12-04 06:28:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('732a673bd0b13f8d11eee452d3350b61', 'aguero', NULL, '4b7f4008c53e969852a998df73f5bb42fbb62c75cf0b31941463041c35bfd21c', '997c26ab292bfffb821072e7bd9f372800fd9ec1', NULL, '16160909', NULL, 'Sergio Aguero', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:17:00', '2019-11-29 06:17:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('754f798686c8884c3634f8282d9107c1', 'serder', NULL, '33642ff7409cc6f0e833b3f4780edf4aaa709f25c77b5a528e36a706a40fe6d9', '0f0e75db7f74e22d3657413374bcac2ece049a3b', NULL, '141515', NULL, 'Suat Serder', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:09:31', '2019-11-29 08:09:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('75d7c0ea4bf9fa0c91cd890a30f5f65c', 'dejong', NULL, '6ad9027ecacf84a26d27657d710ecbc3e39258028961adaa9ad5bef9af785a01', 'f060c4e0fb862f931c622e28668aacca9fd3604f', NULL, '2121222111', NULL, 'Frankie de Jong', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:09:44', '2019-11-29 06:09:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('76a7e9111582c2f386b8e302f5879f94', 'ederson', NULL, '55991a938e45a64d756597629bedb65177e1d74778b2873d55a48f1e85ca11cb', 'e53907cc18deb248186099dde7809167a08bdca0', NULL, '31311313', NULL, 'Ederson Moraes', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-04 06:26:46', '2019-12-04 06:26:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('76fbc85cd055a85e5ee6c737e8df2116', 'draxler', NULL, 'd9f8e53ab09f0ff85cb372d5110aaf9caffbca69b1ef26904b7150a480288bde', 'fa38170c43df3aef332b94fa6b425f983cdea4a7', NULL, '141111', NULL, 'Julian Draxler', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:46:25', '2019-11-29 09:40:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6', '680', '680', '3000'),
('88be5665f6b84f174474b489dbfab274', 'icardi', NULL, 'f69766d06849568c0b0a25df73d3c0828b47b1ee2b073da757f66c38c507ffb3', '66fdbcbdbe413a555982fe215b85592234f2878d', NULL, '940909', NULL, 'Mauro Icardi', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:44:18', '2019-11-29 06:44:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('91fef392a2b723f933814d014d06579a', 'ramos', NULL, 'bc3d0e533e8ecc5350ccd4dc87814570582d30d4382f693488b6368ab12ef91e', '7b0e6dd9da4a64d76d061e7aefd6d277a629b2aa', NULL, '100404', NULL, 'Sergio Ramos', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:13:22', '2019-11-29 08:13:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('97f250ed395ed79c01b335f8eb2c5678', 'falcao', NULL, '5be4c9353ca7c12781c3989c25023af67b98147c2cddaf5281b8843f24306054', 'e24b8a8d041e8effdceaaba4c9666a6f798be26e', NULL, '15090909', NULL, 'Radamel Falcao', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:22:58', '2019-11-29 08:22:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('9a74193dc0cc524453d2772417af3c0d', 'arthur', NULL, 'e395f9e7ed61804e379a58d98c20b3c6213a8b9d2d40bff94eeb2efd4d3b82c4', 'f6944e8c59c5d1e24addb8dbf6986c1fedb9379c', NULL, '08080088', NULL, 'Arthur Melo', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:08:46', '2019-11-29 06:08:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('9badb66c2b36a6466c6301f4cd8e9fed', 'brandt', NULL, 'b1d47f9217acb955d3a5c6892072884fce9aa594a8bf1cd82ee6ad9505791e0b', 'f42b4c7f64831948ff2fddc013eacfcbb1415668', NULL, '14101010', NULL, 'Julian Brandt', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 07:52:47', '2019-11-29 07:52:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('9c2593f8f987185da63ecb6e47143c80', 'yedder', NULL, '4bbbc453d58baa09ad19f7393bc2ec1c27a468099657e101b334fd85d13f89b6', '7c8c6b87a17c050db6aa48d0cb206915489485e0', NULL, '112020', NULL, 'Wissam Ben Yedder', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:34:55', '2019-11-29 06:34:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('9e0cfb3ed64d1ec730f7cc1675074dcd', 'fekir', NULL, 'b70bcccd783e74069f923bf92be3a4141814cad46b5c7f3c6fb18922b277d785', '34e093eb2ac0faf9bed22ed64dac53d5c8c88506', NULL, '111818', NULL, 'Nabil Fekir', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:34:33', '2019-11-29 06:34:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('9ffec7a0823a30544d5ea04219e4cc55', 'sanchez', NULL, 'dbdce555be691094345240e97d2e04b5ce2ac072b4d85302965c0196d88e662d', 'd5bb83f740c95e8c5aa0015cc26d0392f7b47b93', NULL, '15060606', NULL, 'Carlos Sanchez', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:22:21', '2019-11-29 08:22:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', NULL, '346396240a5ba76a2b58278bedf6b350bd61da1f1ef5185ab5229694dcc8b24b', '6fe0d900fe88adbaac672b410d82963198d132da', '10101100', '10101010', NULL, 'Lionel Messi', '3666c07731b2ae30f16a0a2810a3b1634059f252de7e6ba3e3d7932ac869be2a', 'cb575a5bb774351b556b9f37e90ca611246fd857', NULL, 1, 0, 1, 1, '0', 0, 0, '2019-10-08 06:09:15', '2019-12-09 03:18:04', NULL, NULL, NULL, 'CITIBANK', 'Lionel Messi', 1010110010, NULL, NULL, NULL, '7', '670', '3670', '26650'),
('a1cba8835408386d48bf0b01ab7a57a3', 'semedo', NULL, 'ef01c8ea8293188a2d15bc7a141a8ff53fcfa33ae14d77ae55600b07a643ec47', '4e87a1876d2556e8ffb05035c7f9a57edb0ef1d5', NULL, '0202000222', NULL, 'Nelson Semedo', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:13:04', '2019-11-29 06:13:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('a4e2202c9e72b361caba9a6eb64e82b8', 'valencia', NULL, 'a4a319a2e9d72073da1b510cf69d4e9943ec3a763215c272d8247695e3bc0f4c', '7c2485b62ee6fa1ba6e0d8a7b9e4e3e17dd1f371', NULL, '15050505', NULL, 'Edwin Valencia', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:22:03', '2019-11-29 08:22:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('a6d41a96d021e1509c7ee16aa7f8ab26', 'rakitic', NULL, 'e7b780765cce7c22e8b80b427ac0b682c5e4e19969561050525b362591bfdae3', '31fbb6eadf1195be8c5873183d14656317bf6b3f', NULL, '04040044', NULL, 'Ivan Rakitic', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:08:23', '2019-11-29 06:08:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('a83067dabe368e9d48d5ac578eb53b96', 'wague', NULL, '67307248490edfbe88880de3ebb955f7110a1e57e440f458aff2ca1d4063b5c6', '886d0a6285b8d14892ce233f42f45f0c86dbcabf', NULL, '1616111666', NULL, 'Moussa Wague', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:14:13', '2019-11-29 06:14:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('b2743fde93dbfeae47115ef22a92d2bd', 'roberto', NULL, 'a207924fa8045efe335b02ebe929c8c30fd4404340329fd2a3ec7bd8ab59584f', '8a3229a66a5ee7498186deaa68f4698bf3324656', NULL, '2020222000', NULL, 'Sergi Roberto', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:05:25', '2019-11-29 06:15:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7', '580', '580', NULL),
('bc7f79073712cb70eac8f4662aad476b', 'firpo', NULL, '49048b9846eb8108e2d85ddeea2aac5af9a4763948613a00159ffbf70cdd9e75', '5f8dd681336d8e235ed49ec7688a8e40fc341277', NULL, '2424222444', NULL, 'Junior Firpo', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:15:14', '2019-11-29 06:15:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('bf34de0101450f0170addc5de18548c2', 'amiri', NULL, '8f823746ca77523dd7875398aba9cada8cd7b5579d21b071f786af407dfda8db', '6e0fda8a314256b3797624d89eaaef167500f6f3', NULL, '14111111', NULL, 'Nadiem Amiri', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:09:10', '2019-11-29 08:09:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('bf7f773e30cc5e703b5d432dc42725a6', 'griezmann', NULL, '28ef4e14769e8d0ae44aeefff3d8e932dd322a7449bdb763fc09f7d9fee68f46', '1ed82f453fb1d973ca9384f553fd5d28cb18d28a', '1717070718', '17170707', NULL, 'Antonie Griezmann', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:07:52', '2019-11-29 08:40:50', NULL, NULL, NULL, 'HONG LEONG BANK BERHAD', 'Antonie Griezmann', 12345123, NULL, NULL, NULL, '6', '680', '680', '6000'),
('c34fdaf91eebf2aa115d1ca8d17b6ed4', 'vidal', NULL, '76cb41672f68312d32983d4dd5737149a10ab4cf442e143f2b727fe87c3cd549', '84d3ba856e5b4cdc3fafe1f24864a9513bcfc344', NULL, '22222222', NULL, 'Arturo Vidal', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:10:13', '2019-11-29 06:10:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('c4a22bf1f4eb8837d8b5b1080f51e579', 'pique', NULL, 'ec3ede0e4d4606e0212354950b27a8f6cd59351f387ddf71bf2e0bd8deb68bcf', '305c9d507db04e566b240c46f6b17f1d7c6b87f8', '01271287687', '03030033', NULL, 'Gerard Pique', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:05:06', '2019-12-19 01:18:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '450', '450', NULL),
('c7fd51dec157bcaff0475c314712c12d', 'zapata', NULL, '6d543b2b84c684d6b90dde39e31eb31d3822d8d9e27a2155c311d067627c33af', 'd49e82bb4831a169d3289b1448f81305afd955af', NULL, '15020202', NULL, 'Cristan Zapata', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:20:35', '2019-11-29 08:20:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('d59bff2cc57dc3b48be3c53dbf96735d', 'lenglet', NULL, '2c5cf5112f35fd09a82a0a8e20712aaa6ad58bad68887416ca60936944657a1a', '1698a945693dda4212eef06ec3690f57f7253395', NULL, '1515111555', NULL, 'Clement Lenglet', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:13:53', '2019-11-29 06:13:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('d922522d8a45daa000c8ffe4ef401e50', 'coman', NULL, 'ab6dca39a7944dd4762ce56a8483b864864a09b612a84ee4c9b9be1a1fc8f643', 'bc22ab2384eeee81e8b5540e9e05a746a340f55a', NULL, '111111', NULL, 'Kingsley Coman', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:34:18', '2019-11-29 06:34:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('dabdeb40d42d9d281ae442fcccd93895', 'young', NULL, '23325e027fae92f650a4f62e5659f583f847c71fb21418b88a96f63bbc068135', 'f6961038e9a2512d5c04a57df376278051819302', '18181188', '18181818', NULL, 'Ashley Young', '4b597fdac28fd6ab069a81e7afd5c36d8f8c1208ccbecde3db4edba06341f3e0', '0545f0eab435277a668ea1c8cb80f09ca92815b0', NULL, 1, 0, 1, 1, '0', 0, 0, '2019-10-08 06:09:50', '2019-11-29 05:59:59', NULL, NULL, NULL, 'BANK KERJASAMA RAKYAT MALAYSIA BERHAD', 'Ashley Young', 1818118818, NULL, NULL, NULL, '0', '0', '3000', '30000'),
('def0bb9754e106d0ca569c262ac3b655', 'dembele', NULL, '091d8bb34c85caf521e072e70b7a9339120b902421fc327c3d80fa75b94f5dba', 'ff5882fb5329e4fa4b4a13ca605407662ee5ef8e', NULL, '11111111', NULL, 'Ousmane Dembee', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:07:12', '2019-11-29 06:07:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('df4fe5576da5c64c8103857cd7df757b', 'suarez', NULL, 'fcb65f5ea5090caa40adf70fa902e8e5a7b98a1033c69ac992d1ea00b467ea93', 'e2da2251539a7f502fc8393ee14f9d0425c2ebd3', NULL, '09092121', NULL, 'Luis Suarez', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:06:25', '2019-11-29 06:06:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('e10702bc81c2c7235cc5d847b8c9ecf9', 'carvajal', NULL, '689e2fa421c09c083ac06ff21b33618590dbf312794bc070d498174e55d2bc7f', '1fb23d8d8bf859e5a9986eca9f7266413c660f21', NULL, '100202', NULL, 'Dani Carvajal', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:41:57', '2019-11-29 06:41:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('e393d75096300eff3f573b453e043eb3', 'varane', NULL, '7794b2e81683f1c8d46425d529f98a7114bc112724ed6a56fb6ab74b3dfd0b76', '6b207a35ef316abad72d7948d906ced41639f27a', NULL, '180505', NULL, 'Raphael Varane', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 08:13:48', '2019-11-29 08:13:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('e809ed5b38535157bf9a163fa1225ade', 'saf', '0167226357 vector hao', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '09091111', NULL, 'Alex Ferguson', NULL, NULL, NULL, 1, 0, 1, 0, '0', 0, 0, '2019-10-08 06:08:10', '2019-12-18 02:42:20', NULL, NULL, NULL, 'CIMB BANK BERHAD', 'Alex Ferguson', 2147483647, NULL, NULL, NULL, '2', NULL, NULL, '0'),
('ed3b4fb32872ad093139167be8657970', 'kroos', NULL, '430b92a88e6bab37ce7666e39e7a53cd6dac04adce721ed12cf27d6a12dce7fe', '49c8c0cf5b59c0d97bd72b1c783787ec19f4d2cd', '0258654854', '1408088', NULL, 'Toni Kroos', '918a7a2c9b3b43a0ae5766f754c655ee9acd1c130c51fa1e1056c606b82c430b', '98ab2739b26d1bb9f8bc368e5f9bd681f8c6d36d', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 07:52:27', '2019-11-29 09:26:54', NULL, NULL, NULL, 'BANK MUALAMAT MALAYSIA BERHAD', 'Toni Kroos', 55776689, NULL, NULL, NULL, '5', '680', '680', '900'),
('ef087c3d140821babc712b7094f53684', 'busquest', NULL, 'f83a03028a7f9ae46eba0cd4449a81bd74fa4fb03ea5e08cdce1a05678c133b7', '695f47f382ea8c4c06c872bb941a6d50b4092a84', '123123334455', '05051616', NULL, 'Sergio Busquest', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-29 06:04:01', '2019-12-09 03:18:04', NULL, NULL, NULL, 'HSBC BANK MALAYSIA BERHAD', 'Sergio Busquest', 22334455, NULL, NULL, NULL, '6', '500', '500', '1250');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `uid` varchar(255) CHARACTER SET latin1 NOT NULL,
  `withdrawal_number` int(255) NOT NULL,
  `withdrawal_status` text CHARACTER SET latin1 NOT NULL,
  `contact` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `amount` int(255) NOT NULL,
  `final_amount` int(255) NOT NULL,
  `withdrawal_method` varchar(255) CHARACTER SET latin1 NOT NULL,
  `withdrawal_amount` int(255) NOT NULL DEFAULT 0,
  `withdrawal_note` varchar(255) CHARACTER SET latin1 NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `bank_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `acc_number` int(255) NOT NULL,
  `point` int(255) NOT NULL,
  `owner` varchar(255) CHARACTER SET latin1 NOT NULL,
  `receipt` varchar(200) CHARACTER SET latin1 NOT NULL,
  `name` varchar(11) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`announce_id`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdBonus_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdBonus_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdBonus_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `cash_to_point`
--
ALTER TABLE `cash_to_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactionTypeIdGroupCommission_relateTo_transactionTypeId` (`transaction_type_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `money_type`
--
ALTER TABLE `money_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nextlevel`
--
ALTER TABLE `nextlevel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`productid`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productIdTransactionHistory_relateTo_productId` (`product_id`),
  ADD KEY `orderIdProductOrders_relateTo_orderId` (`order_id`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdReferralHistory_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdReferralHistory_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdReferralHistory_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `signup_product`
--
ALTER TABLE `signup_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referrerIdSignUpProduct_To_userId` (`referrer_id`),
  ADD KEY `referralIdSignUpProduct_To_userId` (`referral_id`);

--
-- Indexes for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uidTransactionHistory_relateTo_userId` (`uid`),
  ADD KEY `targetUidTransactionHistory_relateTo_userId` (`target_uid`),
  ADD KEY `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` (`transaction_type_id`),
  ADD KEY `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` (`money_type_id`),
  ADD KEY `sourceTransactionIdTransactionHistory_relateTo_self` (`source_transaction_id`),
  ADD KEY `orderIdTransactionHistory_relateTo_orderId` (`order_id`);

--
-- Indexes for table `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_point`
--
ALTER TABLE `transfer_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `countryIdUser_relateTo_countryId` (`country_id`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`withdrawal_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `announce_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `cash_to_point`
--
ALTER TABLE `cash_to_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `group_commission`
--
ALTER TABLE `group_commission`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `pid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `money_type`
--
ALTER TABLE `money_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nextlevel`
--
ALTER TABLE `nextlevel`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9223372036854775807;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `signup_product`
--
ALTER TABLE `signup_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `transaction_history`
--
ALTER TABLE `transaction_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction_type`
--
ALTER TABLE `transaction_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transfer_point`
--
ALTER TABLE `transfer_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `withdrawal_number` int(255) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bonus`
--
ALTER TABLE `bonus`
  ADD CONSTRAINT `referralIdBonus_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`),
  ADD CONSTRAINT `referrerIdBonus_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`),
  ADD CONSTRAINT `topReferrerIdBonus_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`);

--
-- Constraints for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD CONSTRAINT `transactionTypeIdGroupCommission_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD CONSTRAINT `orderIdProductOrders_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `productIdTransactionHistory_relateTo_productId` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD CONSTRAINT `referralIdReferralHistory_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `signup_product`
--
ALTER TABLE `signup_product`
  ADD CONSTRAINT `referralIdSignUpProduct_To_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdSignUpProduct_To_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD CONSTRAINT `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` FOREIGN KEY (`money_type_id`) REFERENCES `money_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orderIdTransactionHistory_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sourceTransactionIdTransactionHistory_relateTo_self` FOREIGN KEY (`source_transaction_id`) REFERENCES `transaction_history` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `targetUidTransactionHistory_relateTo_userId` FOREIGN KEY (`target_uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `uidTransactionHistory_relateTo_userId` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `countryIdUser_relateTo_countryId` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
