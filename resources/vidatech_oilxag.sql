-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2019 at 05:40 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_oilxag`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `announce_id` int(255) NOT NULL,
  `announce_message` varchar(25000) NOT NULL,
  `announce_showThis` int(2) NOT NULL,
  `announce_dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `announce_lastUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`announce_id`, `announce_message`, `announce_showThis`, `announce_dateCreated`, `announce_lastUpdated`) VALUES
(1, 'Thanks for joining us, We welcome u to our journey', 1, '2019-08-14 04:17:07', '2019-08-14 06:58:37'),
(2, 'Have A nice day ! Fizo', 1, '2019-08-14 04:29:46', '2019-08-14 06:58:21'),
(3, 'Welcome To Oilxag People', 1, '2019-08-14 06:24:43', '2019-08-15 02:01:14'),
(4, 'Mun cun  eeedsafafasdfasd', 0, '2019-08-15 02:01:49', '2019-08-15 02:24:18'),
(5, 'asdsdaasdccc vvvvvvvv', 0, '2019-08-15 02:26:06', '2019-08-15 02:26:17'),
(6, 'lyon is noob team', 1, '2019-08-15 02:26:24', '2019-08-28 06:27:42'),
(7, 'GuangZhou Evergrande BIG BIG!!', 1, '2019-08-28 06:27:16', '2019-09-11 14:57:28'),
(8, 'lalala', 0, '2019-09-10 09:23:39', '2019-09-10 09:23:47'),
(9, 'dadada', 0, '2019-09-10 09:24:04', '2019-09-10 09:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referrer_name` varchar(255) DEFAULT NULL,
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `register_downline_no` int(255) DEFAULT NULL,
  `amount` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonus`
--

INSERT INTO `bonus` (`id`, `referrer_id`, `referrer_name`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `register_downline_no`, `amount`, `date_created`, `date_updated`) VALUES
(1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'f44cf4e1a621cb3f2ba58bf0c53bd23a', 'busquest', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 1, 150, '2019-10-30 06:15:34', '2019-10-30 06:15:34'),
(2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '9a89604be10ea4a137cb1aa15307d848', 'pique', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 2, 150, '2019-10-30 06:15:50', '2019-10-30 06:15:50'),
(3, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '616b77a3947d25c3fa691ecc57235e47', 'roberto', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 3, 50, '2019-10-30 06:16:07', '2019-10-30 06:16:07'),
(4, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '10c89f0425c005c269880e3e2538f900', 'alba', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 4, 50, '2019-10-30 06:16:31', '2019-10-30 06:16:31'),
(5, 'dabdeb40d42d9d281ae442fcccd93895', 'young', '48f51646e84248e8869fe156b72a852d', 'degea', 1, 'dabdeb40d42d9d281ae442fcccd93895', 1, 150, '2019-10-30 06:20:54', '2019-10-30 06:20:54'),
(6, 'dabdeb40d42d9d281ae442fcccd93895', 'young', '0e119c4db48e6265de6709772c35e708', 'pogba', 1, 'dabdeb40d42d9d281ae442fcccd93895', 2, 150, '2019-10-30 06:21:10', '2019-10-30 06:21:10'),
(7, 'dabdeb40d42d9d281ae442fcccd93895', 'young', '97303dfde663e717accacc52819bffce', 'mata', 1, 'dabdeb40d42d9d281ae442fcccd93895', 3, 50, '2019-10-30 06:21:24', '2019-10-30 06:21:24'),
(8, '9a89604be10ea4a137cb1aa15307d848', 'pique', 'd13208abcaaa5d8ac5b60376b423d692', 'rakitic', 2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 1, 150, '2019-10-30 06:24:35', '2019-10-30 06:24:35'),
(9, '9a89604be10ea4a137cb1aa15307d848', 'pique', '153dbb616a21600e1acecee4797dbd49', 'suarez', 2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 2, 150, '2019-10-30 06:24:54', '2019-10-30 06:24:54'),
(10, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '07def429f2afed02949c523987bad862', 'marcelo', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 5, 50, '2019-11-12 05:50:56', '2019-11-12 05:50:56'),
(11, 'f44cf4e1a621cb3f2ba58bf0c53bd23a', 'busquest', '350798e219572c33c4734748e7aca21d', 'ramos', 2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 1, 150, '2019-11-12 05:51:38', '2019-11-12 05:51:38'),
(37, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '980b26c6f982a0799c9d3eedc0728b0c', 'frankie', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 6, 50, '2019-11-21 02:53:34', '2019-11-21 02:53:34'),
(38, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '91223e8d9421836cd9667573249a9362', 'umtiti', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 7, 50, '2019-11-21 02:58:22', '2019-11-21 02:58:22'),
(39, '9a89604be10ea4a137cb1aa15307d848', 'pique', '123c2661ea1355724651122c7c54c086', 'arthur', 2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 3, 50, '2019-11-21 03:00:34', '2019-11-21 03:00:34'),
(40, '153dbb616a21600e1acecee4797dbd49', 'suarez', '1710c73387899b5aead20fac6eceeeeb', 'vidal', 3, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 1, 150, '2019-11-21 03:04:01', '2019-11-21 03:05:21');

-- --------------------------------------------------------

--
-- Table structure for table `cash_to_point`
--

CREATE TABLE `cash_to_point` (
  `id` int(11) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `point` int(255) NOT NULL,
  `date_create` timestamp(3) NOT NULL DEFAULT current_timestamp(3) ON UPDATE current_timestamp(3),
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_to_point`
--

INSERT INTO `cash_to_point` (`id`, `uid`, `name`, `point`, `date_create`, `status`) VALUES
(1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 3000, '2019-10-30 06:14:20.345', 'COMPLETED'),
(2, 'dabdeb40d42d9d281ae442fcccd93895', 'young', 1800, '2019-10-30 06:17:00.838', 'COMPLETED'),
(3, '9a89604be10ea4a137cb1aa15307d848', 'pique', 600, '2019-10-30 06:22:43.112', 'COMPLETED');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 672),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `group_commission`
--

CREATE TABLE `group_commission` (
  `id` int(255) NOT NULL,
  `commission` decimal(30,2) NOT NULL,
  `level` int(3) DEFAULT NULL,
  `count` int(3) DEFAULT NULL COMMENT 'This is for number of referrals that the user has referred and has purchased a product',
  `description` varchar(5000) DEFAULT NULL,
  `transaction_type_id` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_commission`
--

INSERT INTO `group_commission` (`id`, `commission`, `level`, `count`, `description`, `transaction_type_id`, `date_created`, `date_updated`) VALUES
(1, '8.00', 1, NULL, 'in percent', 2, '2019-07-25 09:17:37', '2019-07-25 09:25:28'),
(2, '8.00', 2, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:13'),
(3, '6.00', 3, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:15'),
(4, '6.00', 4, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:17'),
(5, '4.00', 5, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:19'),
(6, '4.00', 6, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:20'),
(7, '2.00', 7, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:31'),
(8, '2.00', 8, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:33'),
(9, '1.00', 9, NULL, 'in percent', 2, '2019-07-25 09:18:43', '2019-07-25 09:25:36'),
(10, '5000.00', NULL, 1, 'in points (RM1 = 100).first referral.', 1, '2019-07-25 09:50:48', '2019-07-25 09:50:58'),
(11, '10000.00', NULL, 2, 'in points (RM1 = 100).second referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:36'),
(12, '15000.00', NULL, 3, 'in points (RM1 = 100).third referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:39'),
(13, '5000.00', NULL, 0, 'in points (RM1 = 100).subsequent referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:43'),
(14, '1.00', 10, NULL, 'in percent', 2, '2019-08-08 06:06:46', '2019-08-08 06:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `pid` int(255) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`pid`, `filename`, `uploaded`, `status`) VALUES
(130, 'messi1571735979.jpg', '2019-10-22 09:19:39', '1'),
(131, 'messi1572311393.jpg', '2019-10-29 01:09:53', '1'),
(132, 'TJ WenJie1573286223.jpg', '2019-11-09 07:57:03', '1');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `uid`, `username`, `fullname`, `place`, `date`, `time`, `date_created`, `date_updated`) VALUES
(1, 'b28efa7574466508acc7b553566375ec', 'tjiayu', 'Teh Jia Yu', 'aaa', '2019-11-05', '0858', '2019-11-10 08:40:13', '2019-11-10 08:40:13'),
(2, 'b28efa7574466508acc7b553566375ec', 'tjiayu', 'Teh Jia Yu', 'asd', 'asd', 'asd', '2019-11-10 08:52:41', '2019-11-10 08:52:41');

-- --------------------------------------------------------

--
-- Table structure for table `money_type`
--

CREATE TABLE `money_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `money_type`
--

INSERT INTO `money_type` (`id`, `name`, `description`, `date_created`, `date_updated`) VALUES
(1, 'user_real', 'this is user\'s real money. can withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00'),
(2, 'voucher', 'this money is gotten from certain commissions. cannot withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `nextlevel`
--

CREATE TABLE `nextlevel` (
  `id` bigint(20) NOT NULL,
  `couple_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `uid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nextlevel`
--

INSERT INTO `nextlevel` (`id`, `couple_id`, `uid`, `status`, `username`, `fullname`, `date_created`, `date_updated`) VALUES
(1, '9dc4937f65ecc19062f98f0b9c999833', '7da984ec4697918cd5e0068b5b7acb9f', 'ACCEPTED', 'TJ WenJie', 'WenJie', '2019-11-09 15:18:38', '2019-11-09 15:18:38'),
(2, 'fb03655129d7e83c73834b200a3aaa87', '7da984ec4697918cd5e0068b5b7acb9f', 'ACCEPTED', 'TJ WenJiesad', 'WenJieasd', '2019-11-09 16:07:01', '2019-11-09 16:07:01'),
(3, '25fa2790d079e42e339b7b26dd2eb28b', 'b78ebeec22c211db362599c66d64b13b', 'ACCEPTED', 'asd', 'asdasd', '2019-11-10 08:04:45', '2019-11-10 08:04:45'),
(4, '0511f20e8e38e0994252f24dc853697d', 'b78ebeec22c211db362599c66d64b13b', 'ACCEPTED', 'asd', 'asdasd', '2019-11-10 08:10:24', '2019-11-10 08:10:24'),
(5, '454b00bb22395dc01842d21372f888f9', 'b28efa7574466508acc7b553566375ec', 'ACCEPTED', 'tjiayu', 'Teh Jia Yu', '2019-11-10 08:29:12', '2019-11-10 08:29:12'),
(6, '1fbc78e240534c289b60a4a9f9e3a4e1', 'b28efa7574466508acc7b553566375ec', 'ACCEPTED', 'tjiayu', 'Teh Jia Yu', '2019-11-10 08:52:32', '2019-11-10 08:52:32');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `address_line_3`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_date`, `payment_time`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `reject_reason`, `refund_method`, `refund_amount`, `refund_note`, `refund_reason`, `date_created`, `date_updated`) VALUES
(1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 'BANK OF CHINA (MALAYSIA) BERHAD', 'Lionel Messi', 2147483647, 'Leo Messi', '10101100', NULL, 'Barca', 'Barcelona', NULL, 'FC Barca', '1001', 'FC Barcelona', 'Forca Barca', '1800', '1830', 'Online Banking', 1830, 'QA34ED66CC8', '2019-11-06', '20:18', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-10', 'ASD123456', NULL, NULL, NULL, NULL, NULL, '2019-11-06 07:18:53', '2019-11-07 07:44:33'),
(2, '48f51646e84248e8869fe156b72a852d', 'degea', 'CIMB BANK BERHAD', 'David De Gea', 1011313, 'David DDG', '01011323', NULL, 'MU', 'Man Utd', NULL, 'Manchester', '0110', 'Manchester United', 'Great Britian', '3600', '3630', 'Online Banking', 3630, 'TGB8976MUY46', '2019-11-03', '15:33', 'ACCEPTED', 'REJECT AND REFUND', NULL, '2019-11-07', NULL, NULL, 'iPay88', 3630, 'Reject Testing', 'Running Out Of Stock', '2019-11-06 07:32:46', '2019-11-07 07:51:08'),
(3, '97303dfde663e717accacc52819bffce', 'mata', 'BANK OF AMERICA (MALAYSIA) BHD', 'Juan Mata', 8081212, 'Juan Mata', '08081313', NULL, 'Man Utd', 'Chelsea FC', NULL, 'Red Blues', '0808', 'MU CFC', 'Spain', '900', '930', 'Online Banking', 930, 'WER56GH8KM', '2019-11-20', '15:44', 'REJECT', 'REJECT', NULL, '2019-11-05', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-06 07:42:37', '2019-11-07 07:42:27'),
(4, '153dbb616a21600e1acecee4797dbd49', 'suarez', 'CIMB BANK BERHAD', 'Luis Suarez', 9090099, 'Luis Suarez', '0909000999', NULL, 'Ajax', 'Liverpool', NULL, 'ALB', '0909', 'Barcelona', 'Uruguay', '5400', '5430', 'Online Banking', 5430, '4RFVBH79M64', '2019-11-05', '09:22', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-06 19:03:59', '2019-11-07 07:40:22'),
(5, '9a89604be10ea4a137cb1aa15307d848', 'pique', 'HSBC BANK MALAYSIA BERHAD', 'Gerard Pique', 303000333, 'Gerard Pique', '03033300', NULL, 'MU', 'Man Utd', NULL, 'Barca', '03033333', 'FC Barca', 'Spain', '1800', '1830', 'Online Banking', 1830, 'WEQ56HN5368', '2019-11-08', '09:25', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-07 01:24:00', '2019-11-07 01:25:10'),
(6, 'a1a2e99ddbdd69b25d8cfa08b2af049e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-20 03:58:09', '2019-11-20 03:58:09');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(5000) NOT NULL,
  `price` decimal(50,0) NOT NULL COMMENT 'With MYR as base and is in point system (RM1 = 100 point)',
  `stock` int(255) NOT NULL,
  `buy_stock` int(255) NOT NULL,
  `total_price` int(255) NOT NULL,
  `display` int(20) DEFAULT 1,
  `type` int(255) NOT NULL DEFAULT 1 COMMENT '1 = normal product',
  `description` varchar(10000) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `stock`, `buy_stock`, `total_price`, `display`, `type`, `description`, `images`, `date_created`, `date_updated`) VALUES
(1, 'DCK Engine Oil Booster', '300', 1000, 0, 0, 1, 1, 'DCK ENGINE OIL BOOSTER', 'DCK-Engine-Oil-Booster.png', '2019-07-24 09:17:04', '2019-10-29 09:18:52'),
(2, 'DCK Synthetic Plus Motor Oil (SAE 5w – 30)', '300', 1000, 0, 0, 1, 1, 'DCK SYNTHETIC PLUS MOTOR OIL (SAE 5W – 30)', '5w.png', '2019-07-25 04:11:21', '2019-10-29 06:12:32'),
(5, 'DCK Synthetic Plus Motor Oil (SAE 10w – 40)', '300', 1000, 0, 0, 1, 1, 'DCK Synthetic Plus Motor Oil (SAE 10w – 40)', '10w.png', '2019-10-29 06:18:28', '2019-10-29 09:11:48');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `productid` int(255) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` int(10) NOT NULL,
  `final_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `original_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `discount_given` decimal(50,0) NOT NULL COMMENT 'in points',
  `totalProductPrice` decimal(50,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_orders`
--

INSERT INTO `product_orders` (`id`, `product_id`, `order_id`, `quantity`, `final_price`, `original_price`, `discount_given`, `totalProductPrice`, `date_created`, `date_updated`) VALUES
(1, 1, 1, 1, '300', '300', '0', '300', '2019-11-06 07:18:53', '2019-11-06 07:18:53'),
(2, 2, 1, 2, '300', '300', '0', '600', '2019-11-06 07:18:53', '2019-11-06 07:18:53'),
(3, 5, 1, 3, '300', '300', '0', '900', '2019-11-06 07:18:53', '2019-11-06 07:18:53'),
(4, 1, 2, 2, '300', '300', '0', '600', '2019-11-06 07:32:46', '2019-11-06 07:32:46'),
(5, 2, 2, 4, '300', '300', '0', '1200', '2019-11-06 07:32:46', '2019-11-06 07:32:46'),
(6, 5, 2, 6, '300', '300', '0', '1800', '2019-11-06 07:32:46', '2019-11-06 07:32:46'),
(7, 2, 3, 1, '300', '300', '0', '300', '2019-11-06 07:42:37', '2019-11-06 07:42:37'),
(8, 5, 3, 2, '300', '300', '0', '600', '2019-11-06 07:42:37', '2019-11-06 07:42:37'),
(9, 1, 4, 3, '300', '300', '0', '900', '2019-11-06 19:03:59', '2019-11-06 19:03:59'),
(10, 2, 4, 6, '300', '300', '0', '1800', '2019-11-06 19:03:59', '2019-11-06 19:03:59'),
(11, 5, 4, 9, '300', '300', '0', '2700', '2019-11-06 19:03:59', '2019-11-06 19:03:59'),
(12, 1, 5, 1, '300', '300', '0', '300', '2019-11-07 01:24:00', '2019-11-07 01:24:00'),
(13, 2, 5, 2, '300', '300', '0', '600', '2019-11-07 01:24:00', '2019-11-07 01:24:00'),
(14, 5, 5, 3, '300', '300', '0', '900', '2019-11-07 01:24:00', '2019-11-07 01:24:00');

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `referral_bonus` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `conversion_point` int(11) NOT NULL,
  `charges_withdraw` int(11) NOT NULL,
  `point_voucher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `uid`, `referral_bonus`, `commission`, `conversion_point`, `charges_withdraw`, `point_voucher`) VALUES
(1, '', 20, 20, 20, 10, 100);

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'f44cf4e1a621cb3f2ba58bf0c53bd23a', 'busquest', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-10-30 06:15:34', '2019-10-30 06:15:34'),
(2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '9a89604be10ea4a137cb1aa15307d848', 'pique', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-10-30 06:15:50', '2019-10-30 06:15:50'),
(3, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '616b77a3947d25c3fa691ecc57235e47', 'roberto', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-10-30 06:16:07', '2019-10-30 06:16:07'),
(4, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '10c89f0425c005c269880e3e2538f900', 'alba', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-10-30 06:16:31', '2019-10-30 06:16:31'),
(5, 'dabdeb40d42d9d281ae442fcccd93895', '48f51646e84248e8869fe156b72a852d', 'degea', 1, 'dabdeb40d42d9d281ae442fcccd93895', '2019-10-30 06:20:54', '2019-10-30 06:20:54'),
(6, 'dabdeb40d42d9d281ae442fcccd93895', '0e119c4db48e6265de6709772c35e708', 'pogba', 1, 'dabdeb40d42d9d281ae442fcccd93895', '2019-10-30 06:21:10', '2019-10-30 06:21:10'),
(7, 'dabdeb40d42d9d281ae442fcccd93895', '97303dfde663e717accacc52819bffce', 'mata', 1, 'dabdeb40d42d9d281ae442fcccd93895', '2019-10-30 06:21:24', '2019-10-30 06:21:24'),
(8, '9a89604be10ea4a137cb1aa15307d848', 'd13208abcaaa5d8ac5b60376b423d692', 'rakitic', 2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-10-30 06:24:35', '2019-10-30 06:24:35'),
(9, '9a89604be10ea4a137cb1aa15307d848', '153dbb616a21600e1acecee4797dbd49', 'suarez', 2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-10-30 06:24:54', '2019-10-30 06:24:54'),
(10, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '07def429f2afed02949c523987bad862', 'marcelo', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-11-12 05:50:56', '2019-11-12 05:50:56'),
(11, 'f44cf4e1a621cb3f2ba58bf0c53bd23a', '350798e219572c33c4734748e7aca21d', 'ramos', 2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-11-12 05:51:38', '2019-11-12 05:51:38'),
(37, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '980b26c6f982a0799c9d3eedc0728b0c', 'frankie', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-11-21 02:53:34', '2019-11-21 02:53:34'),
(38, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '91223e8d9421836cd9667573249a9362', 'umtiti', 1, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-11-21 02:58:22', '2019-11-21 02:58:22'),
(39, '9a89604be10ea4a137cb1aa15307d848', '123c2661ea1355724651122c7c54c086', 'arthur', 2, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-11-21 03:00:34', '2019-11-21 03:00:34'),
(40, '153dbb616a21600e1acecee4797dbd49', '1710c73387899b5aead20fac6eceeeeb', 'vidal', 3, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-11-21 03:04:01', '2019-11-21 03:04:01');

-- --------------------------------------------------------

--
-- Table structure for table `signup_product`
--

CREATE TABLE `signup_product` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referrer_name` varchar(255) DEFAULT NULL,
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `product` varchar(255) NOT NULL,
  `price` int(255) NOT NULL DEFAULT 300,
  `quantity` int(255) NOT NULL DEFAULT 1,
  `total` int(255) NOT NULL DEFAULT 300,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `signup_product`
--

INSERT INTO `signup_product` (`id`, `referrer_id`, `referrer_name`, `referral_id`, `referral_name`, `product`, `price`, `quantity`, `total`, `date_created`, `date_updated`) VALUES
(3, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '980b26c6f982a0799c9d3eedc0728b0c', 'frankie', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-21 02:53:34', '2019-11-21 02:53:34'),
(4, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '91223e8d9421836cd9667573249a9362', 'umtiti', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-21 02:58:22', '2019-11-21 02:58:22'),
(5, '9a89604be10ea4a137cb1aa15307d848', 'pique', '123c2661ea1355724651122c7c54c086', 'arthur', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-21 03:00:34', '2019-11-21 03:00:34'),
(6, '153dbb616a21600e1acecee4797dbd49', 'suarez', '1710c73387899b5aead20fac6eceeeeb', 'vidal', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-21 03:04:01', '2019-11-21 03:04:01');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE `transaction_history` (
  `id` bigint(20) NOT NULL,
  `money_in` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'in points',
  `money_out` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'in points',
  `uid` varchar(255) NOT NULL,
  `target_uid` varchar(255) DEFAULT NULL COMMENT 'this is the uid for the targeted user. it can be a downline''s uid because upline can get commissions from his downline. therefore this target_uid shall be downline''s uid while the uid field above will be upline''s uid',
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'The percentage assigned to this transaction. Usually for commissions use only',
  `original_value` decimal(50,0) DEFAULT NULL COMMENT '(in point)the original value that has been given before calculating the percentage. Usually for commissions use only',
  `status` int(3) DEFAULT NULL COMMENT '1 = pending, 2 = accepted/completed, 3 = rejected, NULL = nothing',
  `level` int(10) DEFAULT NULL COMMENT 'can be group sales commission given by a downline at a level',
  `order_id` bigint(20) DEFAULT NULL COMMENT 'refers to a particular order in order table and it refers to another table that lists out all the products and quantity purchased in that table with order_id as foreign key',
  `transaction_type_id` int(255) NOT NULL,
  `money_type_id` int(255) DEFAULT NULL,
  `source_transaction_id` bigint(20) DEFAULT NULL COMMENT 'this is referring to this table''s own id. Might be needed when some previous transaction is triggering new transactions',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE `transaction_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'Percentage of the transaction, for example if transaction is withdrawal type, user need give 0.5% of total',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`id`, `name`, `description`, `percentage`, `date_created`, `date_updated`) VALUES
(1, 'referral', 'user can get commission by referring people and the referred person must at least buy a product for the referral to get this commission', NULL, '2019-07-25 08:46:07', '2019-07-25 08:46:07'),
(2, 'group_sales', 'This is when the user\'s downline bought a product, he will get a percentage from it. up to 9 levels with each level having different percentage. please refer to group_commission table', NULL, '2019-07-25 09:15:08', '2019-07-25 09:15:08'),
(3, 'buy_product', 'This is when user purchase a product', NULL, '2019-07-26 01:35:16', '2019-07-26 01:35:16'),
(4, 'transfer', 'point transfer between members', '0.25', '2019-07-31 02:46:22', '2019-07-31 02:46:22'),
(5, 'withdraw', 'withdraw money and transfer to bank', '0.50', '2019-07-31 02:46:22', '2019-07-31 02:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_point`
--

CREATE TABLE `transfer_point` (
  `id` int(11) NOT NULL,
  `send_uid` varchar(100) NOT NULL,
  `send_name` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `receive_name` varchar(100) NOT NULL,
  `receive_uid` varchar(100) NOT NULL,
  `create_date` timestamp(3) NOT NULL DEFAULT current_timestamp(3),
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `epin` char(64) DEFAULT NULL,
  `salt_epin` char(64) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `downline_accumulated_points` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'RM1 = 100 point',
  `can_send_newsletter` tinyint(1) NOT NULL DEFAULT 0,
  `is_referred` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `car_model` varchar(200) DEFAULT NULL,
  `car_year` int(20) DEFAULT NULL,
  `picture_id` int(12) DEFAULT NULL,
  `register_downline_no` varchar(255) DEFAULT '0',
  `bonus` varchar(255) DEFAULT NULL,
  `final_amount` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `country_id`, `full_name`, `epin`, `salt_epin`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `login_type`, `user_type`, `downline_accumulated_points`, `can_send_newsletter`, `is_referred`, `date_created`, `date_updated`, `address`, `birthday`, `gender`, `bank_name`, `bank_account_holder`, `bank_account_no`, `car_model`, `car_year`, `picture_id`, `register_downline_no`, `bonus`, `final_amount`, `point`) VALUES
('07def429f2afed02949c523987bad862', 'marcelo', NULL, 'c57f54f3b8bf5d042715291a7ab48ebc0548b0035a65d72f884a031df124510b', '4b920f83d286c2af6b7a36302851889acd39d311', NULL, '1233211122', NULL, 'Marcelo AA', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-12 05:50:56', '2019-11-12 05:50:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('0e119c4db48e6265de6709772c35e708', 'pogba', NULL, 'b645edd637aabad6330e8732555664890282749d1d0e416771a172fc8432a3c6', 'a20c531d1bd89d9ad487c6a12729cc95184d91ce', NULL, '06066600', NULL, 'Paul Pogba', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-30 06:21:10', '2019-10-30 06:21:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('10c89f0425c005c269880e3e2538f900', 'alba', NULL, '53a422d26aa057c513ca54d8e339081e762f33a1bbf150dd5fbcaec5600a61d1', 'cca7f788e2268d0a0470265e6be7699956d477f4', NULL, '18181188', NULL, 'Jordi Alba', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-30 06:16:31', '2019-11-20 08:46:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', '300', '300', NULL),
('123c2661ea1355724651122c7c54c086', 'arthur', 'arthur08.barca@gmail.com', '319742ca5aee60e6335a52d0e07777c56fc389fae812b0deab67d2f447b053c5', '42a241a361173ef8c7d09f6447e1fca82eb67e5c', NULL, '08080088', NULL, 'Arthur Melo', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-21 03:00:34', '2019-11-21 03:00:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('153dbb616a21600e1acecee4797dbd49', 'suarez', NULL, 'b017e3badcc911a12bf686bf21ab8564ebcc6e650df0c2d0ab6c391c3d8d7452', '30ffc7631a6930506da22886554825779c898a58', NULL, '09099900', NULL, 'Luis Suarez', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-30 06:24:54', '2019-11-21 03:05:13', NULL, NULL, NULL, 'CIMB BANK BERHAD', 'Luis Suarez', 9090099, NULL, NULL, NULL, '1', '150', '150', NULL),
('1710c73387899b5aead20fac6eceeeeb', 'vidal', NULL, '00931a6eb4f992a213776e40c74749a84fe4fbab20375c4dd4ea4b199c3d3338', '215dc1847cc13b8a9fe89501fb0309bd782d438c', NULL, '2222000222', NULL, 'Arturo Vidal', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-21 03:04:01', '2019-11-21 03:04:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('350798e219572c33c4734748e7aca21d', 'ramos', NULL, '47d11c705ea159b9ff37e48d480722ef91752694b35c990492ea4080b963d7be', '15fe680c9e5618897b7c83777c20e7e286ce9045', NULL, '3344556677', NULL, 'Sergio Ramos', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-12 05:51:38', '2019-11-12 05:51:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('48f51646e84248e8869fe156b72a852d', 'degea', NULL, '4a95a7b5d21e8ec2261927e9f710df2e174c27fe9673e9a6175a08880a98160e', 'ec7fb07592f3bf0e0209dd62528be9df1f8c64f6', NULL, '01011100', NULL, 'David De Gea', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-30 06:20:54', '2019-11-06 07:32:27', NULL, NULL, NULL, 'CIMB BANK BERHAD', 'David De Gea', 1011313, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('616b77a3947d25c3fa691ecc57235e47', 'roberto', NULL, '25bf9149beb0bb543ccdb340ed082787c7fb2efab0e61d859e07ccefbbf59890', 'c5d8dffe2347c0295c4ff22434b5327200f3b30e', NULL, '20202200', NULL, 'Sergi Roberto', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-30 06:16:07', '2019-10-30 06:16:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('91223e8d9421836cd9667573249a9362', 'umtiti', NULL, '730ac3b3101fcb678c357323b4c40a162c6f19c5b0829daa6c819764885bc269', 'f39e1a3a95aa76402a2f318a68b6d7d331e2d05b', NULL, '23232233', NULL, 'Samuel Umtiti', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-21 02:58:22', '2019-11-21 02:58:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('97303dfde663e717accacc52819bffce', 'mata', NULL, 'c9cafa307753d057b8e5c49d64bb6b86fee4fc65d04048012b68af6bcdf380a8', '4c581e70e7c69b45dfbeb20a87da83eb0bafb328', NULL, '08081313', NULL, 'Juan Mata', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-30 06:21:24', '2019-11-06 07:42:12', NULL, NULL, NULL, 'BANK OF AMERICA (MALAYSIA) BHD', 'Juan Mata', 8081212, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('980b26c6f982a0799c9d3eedc0728b0c', 'frankie', NULL, 'beb2dde143c85d9f054d1d98e65cbcd921edac9f3d3b54a0dd68391a2e50a8ba', 'e6e8507cf59e02adad241d5e5abb64ba46a6414f', NULL, '21212211', NULL, 'Frankie de Jong', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-21 02:53:34', '2019-11-21 02:53:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('9a89604be10ea4a137cb1aa15307d848', 'pique', 'pique03.barca@gmail.com', 'df02019228e483bcef31a3b53ce3d1b474e3757a95f6f650ed76ed042a19baa7', '892416361fc6fb1373f349a5d1e5a0e6201d2474', '0303103103', '03030033', NULL, 'Gerard Pique', '1640dedd722bb199b46e63d8c036238baa4874f30e93ffc38a3220e69fc0f318', 'e07a585dc172634cb893db2ec5e5dd5eb56649aa', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-30 06:15:50', '2019-11-21 03:01:21', NULL, '1989-05-18', 'Male', 'HSBC BANK MALAYSIA BERHAD', 'Gerard Pique', 303000333, NULL, NULL, NULL, '3', '350', '350', '0'),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', NULL, '346396240a5ba76a2b58278bedf6b350bd61da1f1ef5185ab5229694dcc8b24b', '6fe0d900fe88adbaac672b410d82963198d132da', '123321', '10010011', NULL, 'Lionel Messi', 'e8dae4484d288a4d06768f40642e818aecdea1e7b51ac0f8a231ea722a93f2c0', '332cd0da02800f4ad5d0c1fb8e17bac97965f3ff', NULL, 1, 0, 1, 1, '0', 0, 0, '2019-10-08 06:09:15', '2019-11-21 03:04:01', NULL, NULL, NULL, 'CITIBANK', 'Lionel Messi', 102528687, NULL, NULL, NULL, '7', '550', '3550', '1800'),
('d13208abcaaa5d8ac5b60376b423d692', 'rakitic', NULL, '3c88ce6b73ba97d7b3cdde1ffb58812c51416d12c40db6d245be9cd7f7b1f637', '75f1e7b607857faa095786c2eb89d7d759516eb5', NULL, '04044400', NULL, 'Ivan Rakitic', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-30 06:24:35', '2019-10-30 06:24:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('dabdeb40d42d9d281ae442fcccd93895', 'young', NULL, '23325e027fae92f650a4f62e5659f583f847c71fb21418b88a96f63bbc068135', 'f6961038e9a2512d5c04a57df376278051819302', '18188181', '18188181', NULL, 'Ashley Young', '599e68f69c52b029dfe4f5b93993e7b195c79c56e519dc096f3e907e31fc25e8', 'c9a488ececf84b70f786f01beb325478369b657a', NULL, 1, 0, 1, 1, '0', 0, 0, '2019-10-08 06:09:50', '2019-10-30 06:21:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3', '350', '4550', '900'),
('e809ed5b38535157bf9a163fa1225ade', 'martial', 'martial09.mu@gmail.com', '33030009045ddab5244b47480e12c25eb7c8ca7db4251ae69d19fed7672cbbcb', '805949cc73f5a6e3846e737d56a1e8968dd0228a', NULL, '09091111', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, '0', 0, 0, '2019-10-08 06:08:10', '2019-10-30 04:26:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0'),
('f44cf4e1a621cb3f2ba58bf0c53bd23a', 'busquest', NULL, '2b7dca2d3c75009e0040a6a10177da2a5081e3182e7139a8264260476e3834f1', 'ef55e3c5e4238f19a8ccb7118cd94d2ff5e9c6bf', NULL, '05051616', NULL, 'Sergio Busquest', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-30 06:15:34', '2019-11-12 05:51:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '150', '150', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `uid` varchar(255) CHARACTER SET latin1 NOT NULL,
  `withdrawal_number` int(255) NOT NULL,
  `withdrawal_status` text CHARACTER SET latin1 NOT NULL,
  `contact` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `amount` int(255) NOT NULL,
  `final_amount` int(255) NOT NULL,
  `withdrawal_method` varchar(255) CHARACTER SET latin1 NOT NULL,
  `withdrawal_amount` int(255) NOT NULL DEFAULT 0,
  `withdrawal_note` varchar(255) CHARACTER SET latin1 NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `bank_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `acc_number` int(255) NOT NULL,
  `point` int(255) NOT NULL,
  `owner` varchar(255) CHARACTER SET latin1 NOT NULL,
  `receipt` varchar(200) CHARACTER SET latin1 NOT NULL,
  `name` varchar(11) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `withdrawal`
--

INSERT INTO `withdrawal` (`uid`, `withdrawal_number`, `withdrawal_status`, `contact`, `date_created`, `date_updated`, `amount`, `final_amount`, `withdrawal_method`, `withdrawal_amount`, `withdrawal_note`, `username`, `bank_name`, `acc_number`, `point`, `owner`, `receipt`, `name`) VALUES
('a1a2e99ddbdd69b25d8cfa08b2af049e', 5, 'PENDING', 123321, '2019-11-08 08:30:22', '2019-11-08 08:30:22', 40, 2400, '', 0, '', 'Lionel Messi', 'BANK OF CHINA (MALAYSIA) BERHAD', 2147483647, 0, 'messi', '', ''),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 6, 'REJECTED', 123321, '2019-11-08 08:30:28', '2019-11-14 04:07:33', 490, 2350, 'Online Banking', 490, '', 'Lionel Messi', 'BANK OF CHINA (MALAYSIA) BERHAD', 2147483647, 0, 'messi', '', ''),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 7, 'ACCEPTED', 123321, '2019-11-08 08:30:35', '2019-11-08 08:42:14', 90, 1850, 'iPay88', 90, 'GOOD', 'Lionel Messi', 'BANK OF CHINA (MALAYSIA) BERHAD', 2147483647, 0, 'messi', '', ''),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 8, 'PENDING', 123321, '2019-11-08 08:30:40', '2019-11-08 08:30:40', 140, 1750, '', 0, '', 'Lionel Messi', 'BANK OF CHINA (MALAYSIA) BERHAD', 2147483647, 0, 'messi', '', ''),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 9, 'PENDING', 123321, '2019-11-08 08:30:45', '2019-11-08 08:30:45', 190, 1600, 'iPay88', 190, 'OK', 'Lionel Messi', 'BANK OF CHINA (MALAYSIA) BERHAD', 2147483647, 0, 'messi', '', ''),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 10, 'PENDING', 123321, '2019-11-08 08:30:53', '2019-11-08 08:30:53', 190, 1400, '', 0, '', 'Lionel Messi', 'BANK OF CHINA (MALAYSIA) BERHAD', 2147483647, 0, 'messi', '', ''),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 11, 'PENDING', 123321, '2019-11-08 08:31:02', '2019-11-08 08:31:02', 240, 1200, '', 0, '', 'Lionel Messi', 'BANK OF CHINA (MALAYSIA) BERHAD', 2147483647, 0, 'messi', '', ''),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 12, 'PENDING', 123321, '2019-11-08 08:31:08', '2019-11-08 08:31:08', 140, 950, '', 0, '', 'Lionel Messi', 'BANK OF CHINA (MALAYSIA) BERHAD', 2147483647, 0, 'messi', '', ''),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 13, 'ACCEPTED', 123321, '2019-11-08 08:31:30', '2019-11-08 08:31:30', 240, 800, 'iPay88', 240, 'OK', 'Lionel Messi', 'BANK OF CHINA (MALAYSIA) BERHAD', 2147483647, 0, 'messi', '', ''),
('9a89604be10ea4a137cb1aa15307d848', 14, 'PENDING', 303103103, '2019-11-08 08:38:42', '2019-11-08 08:38:42', 30, 420, '', 0, '', 'Gerard Pique', 'HSBC BANK MALAYSIA BERHAD', 303000333, 0, 'pique', '', ''),
('9a89604be10ea4a137cb1aa15307d848', 15, 'PENDING', 303103103, '2019-11-08 08:38:47', '2019-11-08 08:38:47', 40, 380, '', 0, '', 'Gerard Pique', 'HSBC BANK MALAYSIA BERHAD', 303000333, 0, 'pique', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`announce_id`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdBonus_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdBonus_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdBonus_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `cash_to_point`
--
ALTER TABLE `cash_to_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactionTypeIdGroupCommission_relateTo_transactionTypeId` (`transaction_type_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `money_type`
--
ALTER TABLE `money_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nextlevel`
--
ALTER TABLE `nextlevel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`productid`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productIdTransactionHistory_relateTo_productId` (`product_id`),
  ADD KEY `orderIdProductOrders_relateTo_orderId` (`order_id`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdReferralHistory_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdReferralHistory_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdReferralHistory_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `signup_product`
--
ALTER TABLE `signup_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referrerIdSignUpProduct_To_userId` (`referrer_id`),
  ADD KEY `referralIdSignUpProduct_To_userId` (`referral_id`);

--
-- Indexes for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uidTransactionHistory_relateTo_userId` (`uid`),
  ADD KEY `targetUidTransactionHistory_relateTo_userId` (`target_uid`),
  ADD KEY `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` (`transaction_type_id`),
  ADD KEY `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` (`money_type_id`),
  ADD KEY `sourceTransactionIdTransactionHistory_relateTo_self` (`source_transaction_id`),
  ADD KEY `orderIdTransactionHistory_relateTo_orderId` (`order_id`);

--
-- Indexes for table `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_point`
--
ALTER TABLE `transfer_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `countryIdUser_relateTo_countryId` (`country_id`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`withdrawal_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `announce_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `cash_to_point`
--
ALTER TABLE `cash_to_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `group_commission`
--
ALTER TABLE `group_commission`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `pid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `money_type`
--
ALTER TABLE `money_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nextlevel`
--
ALTER TABLE `nextlevel`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9223372036854775807;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `signup_product`
--
ALTER TABLE `signup_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transaction_history`
--
ALTER TABLE `transaction_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction_type`
--
ALTER TABLE `transaction_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `withdrawal_number` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bonus`
--
ALTER TABLE `bonus`
  ADD CONSTRAINT `referralIdBonus_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`),
  ADD CONSTRAINT `referrerIdBonus_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`),
  ADD CONSTRAINT `topReferrerIdBonus_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`);

--
-- Constraints for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD CONSTRAINT `transactionTypeIdGroupCommission_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD CONSTRAINT `orderIdProductOrders_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `productIdTransactionHistory_relateTo_productId` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD CONSTRAINT `referralIdReferralHistory_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `signup_product`
--
ALTER TABLE `signup_product`
  ADD CONSTRAINT `referralIdSignUpProduct_To_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdSignUpProduct_To_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD CONSTRAINT `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` FOREIGN KEY (`money_type_id`) REFERENCES `money_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orderIdTransactionHistory_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sourceTransactionIdTransactionHistory_relateTo_self` FOREIGN KEY (`source_transaction_id`) REFERENCES `transaction_history` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `targetUidTransactionHistory_relateTo_userId` FOREIGN KEY (`target_uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `uidTransactionHistory_relateTo_userId` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `countryIdUser_relateTo_countryId` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
