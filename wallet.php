<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Rate.php';
require_once dirname(__FILE__) . '/classes/Images.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    createOrder($conn,$uid);
    header('Location: ./wallet.php');
}


$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];
$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$withdrawRate = getRate($conn," WHERE id = ? ",array("id"),array(1),"i");
$rateDetails = $withdrawRate[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/wallet.php" />
    <meta property="og:title" content="My Wallet | DCK Supreme" />
    <title>My Wallet | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/wallet.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <!--function to display profile picture-->
    <?php include 'profilePictureDispalyPartA.php'; ?>

    <div class="right-profile-div">
    	<div class="profile-tab width100">
        	<a href="profile.php" class="profile-tab-a">ABOUT</a>
            <a href="referee.php" class="profile-tab-a">MY REFEREE</a>
            <a href="#" class="profile-tab-a active-tab-a">MY WALLET</a>
        </div>

        <div class="left-profile-div-user">
            <h1 class="username"></h1>
        </div>

        <div class="clear"></div>

        <div class="width100 oveflow">
        	<div class="width50">
            	<div class="white50div">
                    <h2>Cash</h2>
                    <p>RM<?php echo $userDetails -> getWithdrawAmount(); ?></p>
                </div>
                <div class="open-withdraw withdraw-button">Withdraw</div>
                <div class="open-convert convert-button" id = "convertButton">Convert to Points</div>
            </div>
        	<div class="width50 second-width50">
				<div class="white50div">
                    <h2>Point</h2>
                    <p><?php echo $userDetails -> getUserPoint(); ?></p>
                </div>
                <div class="open-transfer transfer-button ">Transfer</div>
            </div>
        </div>

        <div class="clear"></div>

    </div>

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Withdraw Request Success!";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "The Withdraw Money Should Not Pass the Amount On The Wallet!!";
        }
        // if($_GET['type'] == 2)
        // {
        //     $messageType = "The Amount Should Atleast RM10 Should Left On The Wallet!!";
        // }
        if($_GET['type'] == 3)
        {
            $messageType = "Wrong E-Pin!";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "You Cant Proceed To Convert the Cash To Point Until Previous Withdraw Request Has Completed!!";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Transfer Cash To Point Success";
        }
        if($_GET['type'] == 6)
        {
            $messageType = "You Cant Proceed To Withdraw the Cash Until Previous Withdraw Request Has Completed!!";
        }
        if($_GET['type'] == 7)
        {
            $messageType = "No Existing IC Number!!";
        }
        if($_GET['type'] == 8)
        {
            $messageType = "The Point Succesfully Transfer!!";
        }
        if($_GET['type'] == 9)
        {
            $messageType = "Atleast 10 Points Should Left On The Wallet!!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Complete Your Profile To Withdraw The Cash!!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Withdraw Error!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Picture upload failed, please try again.";
        }
        if($_GET['type'] == 13)
        {
            $messageType = "Please select a picture.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>

<!-- Withdraw Points Modal -->
<div id="withdraw-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css login-modal-content withdraw-modal-content">
    <span class="close-css close-withdraw">&times;</span>
    <h1 class="white-h1">WITHDRAWAL</h1>
    <form method="POST"  action="utilities/withdrawAmountFunction.php">
    <!-- Wen Jie, help them fill in their bank details-->
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/bank.png" class="login-input-icon" alt="Bank" title="Bank"></span>
        	<input oninput="this.value = this.value.toUpperCase()" class="login-input name-input clean" type="text" id="bank_name" name="bank_name"  value="<?php echo $userDetails -> getBankName(); ?>" required>
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/bank-account-number.png" class="login-input-icon" alt="Account No." title="Account No."></span>
        	<input class="login-input name-input clean" type="text" id="acc_number" name="acc_number"  value="<?php echo $userDetails -> getBankAccountNo(); ?>" required>
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/user.png" class="login-input-icon" alt="Account No." title="Account No."></span>
        	<input oninput="this.value = this.value.toUpperCase()" class="login-input name-input clean" type="text" id="usernameaccount" name="usernameaccount"  value="<?php echo $userDetails -> getBankAccountHolder(); ?>" required>
        </div>
        <div class="input-grey-div">
        	<div class="left-points-div">
                <span class="input-span"><img src="img/coin.png" class="login-input-icon" alt="Points" title="Points"></span>
                <input class="login-input name-input clean" id="withdraw_amount" name="withdraw_amount" type="number" placeholder="Withdraw Amount" value="" >
            </div>

        </div>
				<div class="input-grey-div">
					<span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="E-Pin" title="E-Pin"></span>
				<input class="login-input password-input clean" id="withdraw_epin" type="password" placeholder="E-Pin" name="withdraw_epin"><span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" required></span>
				</div>

        <div class="clear"></div>
        <button class="yellow-button clean" type="submitwithdraw" value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST'){
				    createOrder($conn,$uid);} ?>" >Withdraw</button>

    </form>




  </div>

</div>


<!--- Modal Box --->
<script>

var withdrawmodal = document.getElementById("withdraw-modal");

var openwithdraw = document.getElementsByClassName("open-withdraw")[0];

var closewithdraw = document.getElementsByClassName("close-withdraw")[0];


if(openwithdraw){
openwithdraw.onclick = function() {
  withdrawmodal.style.display = "block";
}
}

if(closewithdraw){
closewithdraw.onclick = function() {
  withdrawmodal.style.display = "none";
}
}


window.onclick = function(event) {

  if (event.target == withdrawmodal) {
    withdrawmodal.style.display = "none";
  }

}
</script>