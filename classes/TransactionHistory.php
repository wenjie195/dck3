<?php
class TransactionHistory {
    /* Member variables */
    var $id,$moneyIn,$moneyOut,$uid,$targetUid,$percentage,$originalValue,$status,$level,$orderId,$transactionTypeId,$moneyTypeId,$sourceTransactionId,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMoneyIn()
    {
        return $this->moneyIn;
    }

    /**
     * @param mixed $moneyIn
     */
    public function setMoneyIn($moneyIn)
    {
        $this->moneyIn = $moneyIn;
    }

    /**
     * @return mixed
     */
    public function getMoneyOut()
    {
        return $this->moneyOut;
    }

    /**
     * @param mixed $moneyOut
     */
    public function setMoneyOut($moneyOut)
    {
        $this->moneyOut = $moneyOut;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getTargetUid()
    {
        return $this->targetUid;
    }

    /**
     * @param mixed $targetUid
     */
    public function setTargetUid($targetUid)
    {
        $this->targetUid = $targetUid;
    }

    /**
     * @return mixed
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param mixed $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * @return mixed
     */
    public function getOriginalValue()
    {
        return $this->originalValue;
    }

    /**
     * @param mixed $originalValue
     */
    public function setOriginalValue($originalValue)
    {
        $this->originalValue = $originalValue;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getTransactionTypeId()
    {
        return $this->transactionTypeId;
    }

    /**
     * @param mixed $transactionTypeId
     */
    public function setTransactionTypeId($transactionTypeId)
    {
        $this->transactionTypeId = $transactionTypeId;
    }

    /**
     * @return mixed
     */
    public function getMoneyTypeId()
    {
        return $this->moneyTypeId;
    }

    /**
     * @param mixed $moneyTypeId
     */
    public function setMoneyTypeId($moneyTypeId)
    {
        $this->moneyTypeId = $moneyTypeId;
    }

    /**
     * @return mixed
     */
    public function getSourceTransactionId()
    {
        return $this->sourceTransactionId;
    }

    /**
     * @param mixed $sourceTransactionId
     */
    public function setSourceTransactionId($sourceTransactionId)
    {
        $this->sourceTransactionId = $sourceTransactionId;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }
}

function getTransactionHistory($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","money_in","money_out","uid","target_uid","percentage","original_value","status","level","order_id","transaction_type_id","money_type_id","source_transaction_id","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"transaction_history");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$moneyIn,$moneyOut,$uid,$targetUid,$percentage,$originalValue,$status,$level,$orderId,$transactionTypeId,$moneyTypeId,$sourceTransactionId,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new TransactionHistory();
            $class->setId($id);
            $class->setMoneyIn($moneyIn);
            $class->setMoneyOut($moneyOut);
            $class->setUid($uid);
            $class->setTargetUid($targetUid);
            $class->setPercentage($percentage);
            $class->setOriginalValue($originalValue);
            $class->setStatus($status);
            $class->setLevel($level);
            $class->setOrderId($orderId);
            $class->setTransactionTypeId($transactionTypeId);
            $class->setMoneyTypeId($moneyTypeId);
            $class->setSourceTransactionId($sourceTransactionId);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function insertIntoTransactionHistory($conn,$moneyIn,$moneyOut,$uid,$targetUid,$percentage,$originalValue,$status,$level,
                                      $orderId,$transactionTypeId,$moneyTypeId,$sourceTransactionId){
    $tableName = "transaction_history";
    $columnNames = array("money_in","money_out","uid","target_uid","percentage","original_value","status","level","order_id","transaction_type_id","money_type_id","source_transaction_id");
    $columnValues = array($moneyIn,$moneyOut,$uid,$targetUid,$percentage,$originalValue,$status,$level,
                            $orderId,$transactionTypeId,$moneyTypeId,$sourceTransactionId);
    $columnTypes = "ddssddiiiiii";

    //this returns either null (error) or new row id (success)
    return insertDynamicData($conn,$tableName,$columnNames,$columnValues,$columnTypes);
}