<?php
class BonusReport {
    /* Member variables */
    var $referralName,$referralId,$referrerName,$referrerId,$currentLevel,$topReferrerId,$dateCreated,$registerDownlineNo,$amount,$signUpBy;

    /**
     * @return mixed
     */
    public function getReferralName()
    {
        return $this->referral_name;
    }

    /**
     * @param mixed $id
     */
    public function setReferralName($referralName)
    {
        $this->referral_name = $referralName;
    }

    /**
     * @return mixed
     */
    public function getSignUpBy()
    {
        return $this->signupby;
    }

    /**
     * @param mixed $id
     */
    public function setSignUpBy($signUpBy)
    {
        $this->signupby = $signUpBy;
    }

    /**
     * @return mixed
     */
    public function getReferralId()
    {
        return $this->referral_id;
    }

    /**
     * @param mixed $id
     */
    public function setReferralId($referralId)
    {
        $this->referral_id = $referralId;
    }

    /**
     * @return mixed
     */
    public function getReferrerName()
    {
        return $this->referrer_name;
    }

    /**
     * @param mixed $id
     */
    public function setReferrerName($referrerName)
    {
        $this->referrer_name = $referrerName;
    }

    /**
     * @return mixed
     */
    public function getReferrerId()
    {
        return $this->referrer_id;
    }

    /**
     * @param mixed $id
     */
    public function setReferrerId($referrerId)
    {
        $this->referrer_id = $referrerId;
    }

    /**
     * @return mixed
     */
    public function getCurrentLevel()
    {
        return $this->current_level;
    }

    /**
     * @param mixed $id
     */
    public function setCurrentLevel($currentLevel)
    {
        $this->current_level = $currentLevel;
    }

    /**
     * @return mixed
     */
    public function getTopReferrerId()
    {
        return $this->top_referrer_id;
    }

    /**
     * @param mixed $id
     */
    public function setTopReferrerId($topReferrerId)
    {
        $this->top_referrer_id = $topReferrerId;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $id
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getRegisterDownlineNo()
    {
        return $this->register_downline_no;
    }

    /**
     * @param mixed $id
     */
    public function setRegisterDownlineNo($registerDownlineNo)
    {
        $this->register_downline_no = $registerDownlineNo;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $id
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

}

function getBonusReport($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("referral_name","referral_id","referrer_name","referrer_id","current_level","top_referrer_id","date_created","register_downline_no","amount","signupby");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"bonus");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($referralName,$referralId,$referrerName,$referrerId,$currentLevel,$topReferrerId,$dateCreated,$registerDownlineNo,$amount,$signUpBy);
                    // array("id","withdrawal_number", "withdrawal_status", "final_amount","date_create");

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new BonusReport();
            $class->setReferralName($referralName);
            $class->setReferralId($referralId);
            $class->setReferrerName($referrerName);
            $class->setReferrerId($referrerId);
            $class->setCurrentLevel($currentLevel);
            $class->setTopReferrerId($topReferrerId);
            $class->setDateCreated($dateCreated);
            $class->setRegisterDownlineNo($registerDownlineNo);
            $class->setAmount($amount);
            $class->setSignUpBy($signUpBy);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}

            //todo this 2 code is AFTER payment successfully done then only execute
//            insertIntoTransactionHistory($conn,$totalPrice,0,$uid,null,null,null,2,null,$orderId,3,null,null);
//
