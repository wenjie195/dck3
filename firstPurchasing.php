<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/SignUpProduct.php';


require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$uid = $_SESSION['uid'];

// $adminList = getWithdrawReq($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

$signUpPro = getSignUpProduct($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/firstPurchasing.php" />
    <meta property="og:title" content="Sign Up Product  | DCK Supreme" />
    <title>Sign Up Product  | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/firstPurchasing.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <h1 class="h1-title h1-before-border shipping-h1">Sign Up Product</h1>

    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest</option>
    	<option class="filter-option">Oldest</option>
    </select> -->



    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>UID</th>
                        <th>NAME</th>
                        <th>PRICE (RM)</th>
                        <th>QUANTITY</th>
                        <th>TOTAL (RM)</th>
                        <th>DATE</th>
                        <th>DETAILS</th>
                    </tr>
                </thead>

                <tbody>

                <?php
                if($signUpPro)
                {
                    for($cnt = 0;$cnt < count($signUpPro) ;$cnt++)
                    {?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $signUpPro[$cnt]->getReferralUid();?></td>
                            <td><?php echo $signUpPro[$cnt]->getReferralName();?></td>
                            <td><?php echo $signUpPro[$cnt]->getPrice();?></td>
                            <td><?php echo $signUpPro[$cnt]->getQuantity();?></td>
                            <td><?php echo $signUpPro[$cnt]->getTotal();?></td>

                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($signUpPro[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>

                            <td>
                                <!-- <form action="withdrawalDetails.php" method="POST"> -->
                                <form action="signUpProductDetails.php" method="POST">
                                    <button class="clean edit-anc-btn hover1" type="submit" name="uid_signup" value="<?php echo $signUpPro[$cnt]->getReferralUid();?>">
                                        <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="Shipping Out" title="Details">
                                        <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="Shipping Out" title="Details">
                                    </button>
                                </form>
                            </td>
    
                    <?php
                    }?>
                        </tr>
                        <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="clear"></div>

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>
