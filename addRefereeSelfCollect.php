<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $register_username = rewrite($_POST['register_username']);
    $register_fullname = rewrite($_POST['register_fullname']);
    $register_ic_no = rewrite($_POST['register_ic_no']);

    $register_email_user = null;
        if(isset($_POST['register_email_user']))
        {
            $register_email_user = rewrite($_POST['register_email_user']);
            $register_email_user = filter_var($register_email_user, FILTER_SANITIZE_EMAIL);
        }

    $register_username_referrer = rewrite($_POST['register_username_referrer']);
    // $register_product = rewrite($_POST['product']);

    $register_product = null;
        if(isset($_POST['product']))
        {
            $register_product = rewrite($_POST['product']);
        }
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/addRefereeSelfCollect.php" />
    <meta property="og:title" content="Add Referee Product Self Collect | DCK Supreme" />
    <title>Add Referee Product Self Collect | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/addRefereeSelfCollect.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="username">Self Collect Location</h1>

    <?php
    if($register_product == "")
    {
    ?>

    <div class="right-profile-div">
        Please select a product
    </div>

    <?php
    }
    else
    { ?>

    <!-- <form  id="addRefereeForm" class="edit-profile-div2" onsubmit="return registerNewMemberAccountValidation();" action="utilities/addNewRefereeFunction.php" method="POST"> -->
    <!-- <form  class="edit-profile-div2" action="utilities/addNewRefereeFunction.php" method="POST"> -->

    <form  class="edit-profile-div2" action="utilities/addNewRefereeSelfCollectFunction.php" method="POST">

    <table class="edit-profile-table">
        <tr class="profile-tr">
            <td class="">Select Pick Up Location</td>
            <td class="profile-td2">:</td>
            <td class="profile-td3">
                <select class="edit-profile-input edit-profile-select clean" type="text" id="register_product_self_collect" name="register_product_self_collect" required>
                    <option value="" name=" ">PLEASE SELECT A LOCATION</option>
                    <option value="KL" name="KL">KL</option>
                    <option value="PG" name="PG">PG</option>
                    <option value="HQ" name="HQ">HQ</option>
                </select><img src="img/dropdown2.png" class="dropdown-png">
            </td>
        </tr>

        <tr class="profile-tr">
            <td class="">Username</td>
            <td class="profile-td2">:</td>
            <td class="profile-td3"><?php echo $register_username;?></td>
        </tr>

        <tr class="profile-tr">
            <td class="">Fullname</td>
            <td class="profile-td2">:</td>
            <td class="profile-td3"><?php echo $register_fullname;?></td>
        </tr>

        <tr class="profile-tr">
            <td class="">IC No.</td>
            <td class="profile-td2">:</td>
            <td class="profile-td3"><?php echo $register_ic_no;?></td>
        </tr>

        <tr class="profile-tr">
            <td class="">Email</td>
            <td class="profile-td2">:</td>
            <td class="profile-td3"><?php echo $register_email_user;?></td>
        </tr>

        <tr class="profile-tr">
            <td class="">Referrer's Username</td>
            <td class="profile-td2">:</td>
            <td class="profile-td3"><?php echo $register_username_referrer;?></td>
        </tr>

        <tr class="profile-tr">
            <td class="">Product</td>
            <td class="profile-td2">:</td>
            <td class="profile-td3"><?php echo $register_product;?></td>
        </tr>

    </table>

    <input class="clean white-input two-box-input" type="hidden" id="register_username" name="register_username" value="<?php echo $register_username;?>">
    <input class="clean white-input two-box-input" type="hidden" id="register_fullname" name="register_fullname" value="<?php echo $register_fullname;?>">
    <input class="clean white-input two-box-input" type="hidden" id="register_ic_no" name="register_ic_no" value="<?php echo $register_ic_no;?>">
    <input class="clean white-input two-box-input" type="hidden" id="register_email_user" name="register_email_user" value="<?php echo $register_email_user;?>">
    <input class="clean white-input two-box-input" type="hidden" id="register_username_referrer" name="register_username_referrer" value="<?php echo $register_username_referrer;?>">
    <input class="clean white-input two-box-input" type="hidden" id="register_product" name="register_product" value="<?php echo $register_product;?>">

    <input required class="login-input password-input clean" type="hidden" id="register_password" name="register_password" value="123321">
    <input required class="login-input password-input clean" type="hidden" id="register_retype_password" name="register_retype_password" value="123321">

        <div class="clear"></div>

        <!-- <button class="confirm-btn text-center white-text clean black-button"name="refereeButton"><?php //echo _MAINJS_ADD_REFEREE_ADD_REFEREE_BUTTON ?></button> -->
        <button class="confirm-btn text-center white-text clean black-button"name="refereeButton">Add Referee</button>

    </form>

    <?php
    }
    ?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>
