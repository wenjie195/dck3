<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/" />
<meta property="og:title" content="Engine Oil Booster | DCK Supreme" />
<title>Engine Oil Booster | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">

	<?php include 'header-sherry.php'; ?>
    
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
            <div>
                <img data-u="image" src="img/banner1.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/banner2.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/banner3.jpg" />
            </div>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>



<div class="about-div width100 same-padding text-center" id="about" data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "1.5"}'>
	<h1 class="title-h1" >ABOUT</h1>
    <p class="border-p"></p>
    <img src="img/dck.png" class="bold-logo" alt="DCK" title="DCK">
    <p class="about-p">Our products are designed to satisfy all of our customers automotive needs, anytime, anywhere.<br> 
						Customer satisfaction is our top priority.<br>
 						Want to learn more about how we do business? Give us a call.
    </p>
    <div class="left-phone-div float-left">
    	<img src="img/phone.png" class="phone-png" alt="contact us" title="contact us">
    </div>
    <div class="right-phone-div float-left">
    	<p class="phone-p1 phone-p1a">+6016-422 2586 (Penang)</p>
        <p class="phone-p1 call-a"><a href="tel:+6016-422 2586" class="call-a">+6016-422 2586 (Penang)</a></p>
        <p class="phone-p phone-p1a">+6012-490 2525 (KL)</p>
        <p class="phone-p call-a"><a href="tel:+6012-490 2525" class="call-a">+6012-490 2525 (KL)</a></p>	
    </div>
</div>
<div class="clear"></div>
<div class="width100 same-padding video-div">
	<h1 class="title-h1">OUR PRODUCT TEST RUN</h1>
    <p class="border-p benefit-separate"></p>
	<iframe class="dck-video" src="https://www.youtube.com/embed/ALKFEVuFSOE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <iframe  class="dck-video" src="https://www.youtube.com/embed/W16WDGfajdA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <iframe class="dck-video3" src="https://www.youtube.com/embed/1b8EIEM5gxg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <!--<video class="dck-video" controls>
      <source src="video/dck-no-engine-oil-test-run.mp4" type="video/mp4">
      Your browser does not support HTML5 video.
	</video>
	<video class="dck-video" controls>
      <source src="video/dck-no-engine-oil-test-run2.mp4" type="video/mp4">
      Your browser does not support HTML5 video.
	</video>    
	<video class="dck-video3" controls>
      <source src="video/dck.mp4" type="video/mp4">
      Your browser does not support HTML5 video.
	</video> -->    
</div>
<div class="clear"></div>
<div class="width100 benefit-div overflow same-padding"  id="products">
	<h1 class="title-h1">PRODUCT BENEFITS</h1>
    <p class="border-p benefit-separate"></p>
    
    <div class="one-box-div">
    	<div class="icon-div">
        	<img src="img/happy-family.png" class="icon-img" alt="Peace of Mind with Your Family" title="Peace of Mind with Your Family">
        </div>
        <div class="benefit-p-div">
        	<p class="benefit-p first-benefit-p">Enjoy a Safe and Uninterruptible Long Distance Journey with a Peace of Mind with Your Family Using DCK Engine Oil that Protects Your Vehicle Engines All the Way!</p>
        </div>
    </div>
    
    <div class="one-box-div">        
        <div class="icon-div">
        	<img src="img/prolongs-engine-lifespan.png" class="icon-img" alt="Prolongs Engine Lifespan" title="Prolongs Engine Lifespan">
        </div>
        <div class="benefit-p-div">
        	<p class="benefit-p first-benefit-p">Prolongs Engine Lifespan</p>
        </div>
	</div> 
	<div class="tempo-two-clear"></div>
    <div class="one-box-div">         
        <div class="icon-div">
        	<img src="img/reduce-cost.png" class="icon-img" alt="Reduces Maintenance Cost" title="Reduces Maintenance Cost">
        </div>
        <div class="benefit-p-div">
        	<p class="benefit-p">Prevent Wear and Tear on Components inside the Engine thus Reduces Maintenance Cost</p>
        </div>
	</div>   
    
    <div class="one-box-div">          
        <div class="icon-div">
        	<img src="img/extend-oil-change.png" class="icon-img" alt="Extends Engine Oil Change Interval by 50% (ie from 10,000 km to 15,000 km)" title="Extends Engine Oil Change Interval by 50% (ie from 10,000 km to 15,000 km)">
        </div>
        <div class="benefit-p-div">
        	<p class="benefit-p">Extends Engine Oil Change Interval by 50% (ie from 10,000 km to 15,000 km)</p>
        </div>        
    </div>
    <div class="tempo-two-clear"></div>
    <div class="one-box-div">          
        <div class="icon-div">
        	<img src="img/saves-fuel.png" class="icon-img" alt="Saves Fuel" title="Saves Fuel">
        </div>
        <div class="benefit-p-div">
        	<p class="benefit-p">Saves Fuel</p>
        </div>        
    </div>
    
    <div class="one-box-div">          
        <div class="icon-div">
        	<img src="img/improve-lifting-speed.png" class="icon-img" alt="Improves Mechanical and Lifting Speed" title="Improves Mechanical and Lifting Speed">
        </div>
        <div class="benefit-p-div">
        	<p class="benefit-p">Improves Mechanical and Lifting Speed</p>
        </div>        
    </div>
    <div class="tempo-two-clear"></div>
    <div class="one-box-div">          
        <div class="icon-div">
        	<img src="img/reduce-engine-vibration.png" class="icon-img" alt="Reduces Engine Vibration, Noisiness and Temperature" title="Reduces Engine Vibration, Noisiness and Temperature">
        </div>
        <div class="benefit-p-div">
        	<p class="benefit-p">Reduces Engine Vibration and Noise Instantly and Brings Down Engine Temperature by 5℃</p>
        </div>        
    </div>    
    
    <div class="one-box-div">          
        <div class="icon-div">
        	<img src="img/dry-cold-start.png" class="icon-img" alt="Dry Cold Start" title="Dry Cold Start">
        </div>
        <div class="benefit-p-div">
        	<p class="benefit-p">Reduces Wear and Tear during “Dry Cold Start”. Engine Wear and Tear Normally Occured during Cold Start after Engine was Rested for Many Hours.</p>
        </div>        
    </div>     
    <div class="tempo-two-clear"></div>
    <div class="one-box-div">          
        <div class="icon-div">
        	<img src="img/reduce-emission.png" class="icon-img" alt="Reduces Emission" title="Reduces Emission">
        </div>
        <div class="benefit-p-div">
        	<p class="benefit-p">Reduces Emission</p>
        </div>        
    </div> 
    
    <div class="one-box-div">          
        <div class="icon-div">
        	<img src="img/engine-oil-booster.png" class="icon-img" alt="Engine Oil Booster" title="Engine Oil Booster">
        </div>
        <div class="benefit-p-div">
        	<p class="benefit-p">Small Dosage Required: 130ml DCK Engine Oil Booster (3%): 1 liter Engine Oil</p>
        </div>        
    </div>    
    
       
</div>
<div class="clear"></div>
<!--- Contact Us --->
<div class="contact-us-div same-padding width100 text-center overflow" id="contact">
<div   data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "1.5"}'>	
    <h1 class="title-h1" >CONTACT US</h1>
    <p class="border-black benefit-separate"></p>	
    <p class="contact-p">Get in touch with us!</p>
    
	<div class="one-row-container width100 overflow">
    
    	<div class="left-contact-div">
        	<div class="contact-icon-div">
            	<img src="img/phone2.png" class="contact-icon-img" alt="Phone Number" title="Phone Number">
            </div>
            <p class="contact-p not-call">+604-323 4723</p>
            <p class="contact-p call-function"><a href="tel:+604-323 4723" class="call-function">+604-323 4723</a></p>
            <!--<p class="contact-p contact-p2 not-call">+604-323 4723</p>
            <p class="contact-p contact-p2 call-function"><a href="tel:+604-323 4723" class="call-function">+604-323 4723</a></p>-->
        </div>
        
    	<div class="middle-contact-div">
         	<div class="contact-icon-div">
            	<img src="img/location.png" class="contact-icon-img" alt="Address" title="Address">
            </div>
            <p class="contact-p">No.94B, Jalan Pusat Perniagaan Raja Uda 1,<br>
								12300 Butterworth, Penang,<br> 
								Malaysia
            </p>       
        </div>  
              
    	<div class="right-contact-div">
         	<div class="contact-icon-div">
            	<img src="img/email.png" class="contact-icon-img" alt="Email" title="Email">
            </div>
            <p class="contact-p">enquiry@dcksupreme.asia</p>
            
        </div>        
        
    </div>    
    </div>
</div>
<div class="clear"></div>
<!--- Contact Form --->

<div class="width100 benefit-div overflow same-padding margin-btm-0">
	<h1 class="title-h1">AUTHORISED STOCKIST</h1>
    <p class="border-p benefit-separate" ></p>
    
    <div class="one-box-div stockist">
		<p><b>Penang (Georgetown):</b><br><br>
            Chow Chean How<br>
            +6016-422 2586<br>
            No 13, Jalan Siam, Dato Keramat,<br>Georgetown, 10450, Penang</p>
    </div>
    
    <div class="one-box-div stockist">        
		<p><b>KL (Ara Damansara):</b><br><br>
        	Yusoff<br>
            +6012-490 2525<br>
            E-8-06 Capital 5, 
            Oasis Square, Ara Damansara<br>
            47301 Petaling Jaya, Selangor
        </p>
	</div> 
</div>

<div class="clear"></div>
<div class="same-padding width100 overflow contact-form-big-div">
	<p class="message-us-p"   data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "1.5"}'>
    	IF YOU HAVE ANY QUERIES,<br>
        PLEASE DO NOT HESITATE TO SEND US A MESSAGE.
    </p>

                 <form id="contactform" method="post" action="index.php" class="form-class extra-margin"   data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "1.5"}'>
                  
                  <input type="text" name="name" placeholder="Name" class="input-name clean" required >
                  
                  <input type="email" name="email" placeholder="Email" class="input-name clean" required >                  
                  
                  <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean" required >


                                  
                  <textarea name="comments" placeholder="Message" class="input-name input-message clean" ></textarea>
                  <div class="clear"></div>
                  <input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required><p class="opt-msg left"> I want to be updated with more information about your company's news and future promotions</p>
                  <div class="clear"></div>
                  <input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required><p class="opt-msg left"> I just want to be contacted based on my request/inquiry</p>
                  <div class="clear"></div>
                   
                  <div class="res-div"><input type="submit" name="send_email_button" value="SEND US A MESSAGE" class="input-submit white-text clean pointer"></div>
                </form> 
    
	<p class="contact-p text-center hide">Connect with us!</p>    
   	<div class="social-one-row-container hide">
    	<div class="fill-up-space"></div>
    	<div class="fb-icon-class hover1">
        	<a href="" class="" target="_blank">
                <img src="img/facebook.png" class="social-icon hover1a fb-icon" alt="Facebook" title="Facebook">
                <img src="img/facebook2.png" class="social-icon hover1b fb-icon" alt="Facebook" title="Facebook">
            </a>
        </div>
     	<div class="fb-icon-class hover1">
        	<a href="" class="" target="_blank">        
                <img src="img/instagram.png" class="social-icon hover1a insta-icon" alt="Instagram" title="Instagram">
                <img src="img/instagram2.png" class="social-icon hover1b insta-icon" alt="Instagram" title="Instagram">
            </a>
        </div>
        <div class="fill-up-space"></div>       
    </div>
</div>
<div class="clear"></div>

<!-- CSS -->
<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#7cd1d1;
  /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
  background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
  background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}
@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>

<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com, enquiry@dcksupreme.asia";
    $email_subject = "Contact Form via DCK Supreme website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            // $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            // $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "There are no user with this email ! Please try again.";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Successfully reset your password! Please check your email.";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "Successfully reset your password! ";
        }
        else if($_GET['type'] == 10)
        {
            $messageType = "Please confirm your registration inside your email! ";
        }
        else if($_GET['type'] == 11)
        {
            $messageType = "Incorrect email or password! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';   
}
?>
<script src="js/jssor.slider.min.js" type="text/javascript"></script>
<script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    
    
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->

</body>
</html>