<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/SignUpProduct.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $products = getProduct($conn);
$products = getSignUpProduct($conn, "ORDER BY date_created DESC");

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminSales.php" />
    <meta property="og:title" content="Sales | DCK Supreme" />
    <title>Sales | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminSales.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="h1-title h1-before-border">Sales (Total)</h1>

    <div class="with100">
    	<!-- <table class="sales-table"> -->
        <table class="shipping-table">
        	<thead>
            	<!-- <tr class="sales-th-tr"> -->
                <tr>
                	<!-- <th></th>
                    <th>PRODUCT</th>
                    <th>QUANTITY</th>
                    <th class="right-cell">TOTAL (RM)</th> -->

                    <th>NO.</th>
                    <th>USERNAME</th>
                    <th>FULLNAME</th>
                    <th>PRODUCT</th>
                    <th>QUANTITY</th>
                    <th>VALUE</th>
                    <th>DATE</th>
                </tr>
            </thead>

            <tbody>
                <?php
                if($products)
                {   
                    for($cnt = 0;$cnt < count($products) ;$cnt++)
                    {?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $products[$cnt]->getReferralName();?></td>
                            <td><?php echo $products[$cnt]->getReferralFullname();?></td>
                            <td><?php echo $products[$cnt]->getProduct();?></td>
                            <td><?php echo $products[$cnt]->getQuantity();?></td>
                            <td><?php echo $products[$cnt]->getPrice();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($products[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>
                    <?php
                    }?>
                        </tr>
                        <?php
                }
                ?>
            </tbody>

        </table>

        <table class="sales-table">
            <tr class="double-border">
                <td></td>
                <!-- <td>TOTAL</td> -->
                <td>TOTAL (RM)</td>
                <td></td>
                <td></td>
                <td></td>         
                <td>3000</td>    
                <!-- <td class="right-cell">3000</td> -->
                <td></td>
            </tr>
        </table>

    </div>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
