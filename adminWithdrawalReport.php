<?php


require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();
$post = array();
$start_date = date("Y-m-d");
$end_date = date("Y-m-d");
$username = "";
$withdrawalNumber = "";
$status  = "";
//$amount  = "";

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}


if (isset($_GET["start_date"]) && isset($_GET["end_date"]) && isset($_GET["username"]) && isset($_GET["withdrawal_number"]))
{
	$start_date = $_GET["start_date"];
	$end_date = $_GET["end_date"];
	$username = $_GET["username"];
  $withdrawalNumber = $_GET["withdrawal_number"];
  $status = $_GET["withdrawal_status"];
  //$amount = $_GET["amount"];
	$post = $_GET;
}

$products = getProduct($conn);
$list = GetList($post, $conn);

//echo json_encode($list);//exit;

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function GetList($post, $conn)
{
	$sql = "SELECT username,date_created,withdrawal_note,withdrawal_status, ";
	$sql .= "amount,withdrawal_amount,withdrawal_number ";
//$sql .= "CASE WHEN record_type_id = '1' THEN 'Direct Sponsor' ";
//	$sql .= "WHEN record_type_id = '2' THEN 'Sales Commission' ";
//	$sql .= "ELSE 'Annual Bonus' END AS 'type' ";
	$sql .= "FROM withdrawal ";
	$sql .= "WHERE withdrawal_number > '0' ";

  if (isset($post["reset"])) {
    if (isset($post["start_date"]) && strlen($post["start_date"]) < 0)
    {
      $sql .= "AND DATE(date_created) >= '" . $post["start_date"] . "' ";
    }

    if (isset($post["end_date"]) && strlen($post["end_date"]) < 0)
    {
      $sql .= "AND DATE(date_created) <= '" . $post["end_date"] . "' ";
    }

    if (isset($post["withdrawal_status"]) && strlen($post["withdrawal_status"]) < 0)
  	{
      $sql .= "AND withdrawal_status LIKE '%" . $post["withdrawal_status"] . "%' ";
  	}
    if (isset($post["username"]) && strlen($post["username"]) < 0)
  	{
      $sql .= "AND username LIKE '%" . $post["username"] . "%' ";
  	}
    if (isset($post["withdrawal_number"]) && strlen($post["withdrawal_number"]) < 0)
  	{
      $sql .= "AND withdrawal_number LIKE '%" . $post["withdrawal_number"] . "%' ";
  	}
    if (isset($post["amount"]) && strlen($post["amount"]) < 0)
  	{
      $sql .= "AND amount LIKE '%" . $post["amount"] . "%' ";
  	}


}else {


  if (isset($post["start_date"]))
  {
    $sql .= "AND DATE(date_created) >= '" . $post["start_date"] . "' ";
  }

  if (isset($post["end_date"]))
  {
    $sql .= "AND DATE(date_created) <= '" . $post["end_date"] . "' ";
  }

  if (isset($post["withdrawal_status"]) && strlen($post["withdrawal_status"]) > 0)
  {
    $sql .= "AND withdrawal_status LIKE '%" . $post["withdrawal_status"] . "%' ";
  }
  if (isset($post["username"]) && strlen($post["username"]) > 0)
  {
    $sql .= "AND username LIKE '%" . $post["username"] . "%' ";
  }
  if (isset($post["withdrawal_number"]) && strlen($post["withdrawal_number"]) > 0)
  {
    $sql .= "AND withdrawal_number LIKE '%" . $post["withdrawal_number"] . "%' ";
  }
  if (isset($post["amount"]) && strlen($post["amount"]) > 0)
  {
    $sql .= "AND amount LIKE '%" . $post["amount"] . "%' ";
  }
}
	$sql .= "ORDER BY date_created ASC ";
	//echo $sql;exit;

	$result = $conn->query($sql);
	$output = array();

	if ($result->num_rows > 0)
	{
		// output data of each row
		while($row = $result->fetch_assoc())
		{
			$output[] = $row;
		}
	}

	return $output;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/withdrawalReport.php" />
    <meta property="og:title" content="Withdrawal Report | DCK Supreme" />
    <title>Withdrawal Report | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/withdrawalReport.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <!-- <h1 class="h1-title h1-before-border shipping-h1">Withdrawal Report</h1> -->
    <h1 class="h1-title h1-before-border shipping-h1"><a href="adminBonusReport.php" class="white-text title-tab-a">Bonus Report</a> | Withdrawal Report</h1>
        <div class="clear"></div>
    	<div class="search-container0 payout-search">
    		<form action="adminWithdrawalReport.php" type="post">
                <div class="shipping-input clean smaller-text2 ow-shipping-input-with">
                    <p>Withdrawal Number</p>
                    <input class="shipping-input2 clean normal-input same-height-with-date" type="number" name="withdrawal_number" placeholder="Withdrawal Number"  value="<?php echo $withdrawalNumber; ?>">
                </div>
                <div class="shipping-input clean smaller-text2 ow-shipping-input-with">
                    <p>Name</p>
                    <input class="shipping-input2 clean normal-input same-height-with-date" type="text" name="username" placeholder="Name"  value="<?php echo $username; ?>">
                </div>
                <div class="shipping-input clean smaller-text2 ow-shipping-input-with">
                    <p>Start Date</p>
                    <input class="shipping-input2 clean normal-input" name="start_date" type="date" value="<?php echo $start_date; ?>">
                </div>
                <div class="shipping-input clean smaller-text2 ow-shipping-input-with">
                    <p>End Date</p>
                    <input class="shipping-input2 clean normal-input" name="end_date" type="date" value="<?php echo $end_date; ?>">
                </div>
                <div class="shipping-input clean smaller-text2 ow-shipping-input-with">
                    <p>Status</p>
                    <select class="shipping-input2 clean normal-input same-height-with-date" type="text"name="withdrawal_status">
                        <option value="">Choose</option>
                        <option value="ACCEPTED" name="PENDING">ACCEPTED</option>
                        <option value="REJECTED" name="REJECT">REJECTED</option>
                        <option value="PENDING" name="REJECT">PENDING</option>
                    </select>
                </div>
                <!-- <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                    <p>Bonus</p>
                    <input class="shipping-input2 clean normal-input" name="amount" type="number" placeholder="Bonus" value="<?php echo $amount; ?>">
                </div> -->



                <button type="submit" class="clean black-button shipping-search-btn second-shipping same-height-with-date2 ow-with-btn ow-with-btn2 ow-with-btn3">Search</button><br><br>
                <button type="submit" name="reset" class="clean black-button shipping-search-btn second-shipping same-height-with-date2 ow-with-btn ow-with-btn4">Reset</button>
    			</form>
        </div>

        <div class="clear"></div>

    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest</option>
    	<option class="filter-option">Oldest</option>
    </select> -->




    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>WITHDRAW NO.</th>
                        <th>NAME</th>
                        <th>DATE</th>
                        <!-- <th>TIME</th> -->
                        <th>REQUEST AMOUNT (RM)</th>
                        <th>GIVEN AMOUNT (RM)</th>
                        <th>NOTE</th>
                        <th>STATUS</th>
                        <th>DETAILS</th>


                    </tr>
                </thead>
                <tbody>
								<?php if ($list): ?>
                  <?php $index=0; ?>
									<?php foreach ($list AS $ls):
                     $index++;?>
										<tr>
											<td><?php echo $index; ?></td>
											<td><?php echo $ls["withdrawal_number"]; ?></td>
                      <td><?php echo $ls["username"]; ?></td>
                      <td><?php echo $ls["date_created"]; ?></td>
                      <td><?php echo $ls["amount"]; ?></td>
											<td><?php echo $ls["withdrawal_amount"]; ?></td>
                      <td><?php echo $ls["withdrawal_note"]; ?></td>
                      <td><?php echo $ls["withdrawal_status"]; ?></td>
                      <td>
                        <?php

                        $statusWithdraw = $ls["withdrawal_status"];

                        if ($ls["withdrawal_status"] == 'ACCEPTED') {?>
                            <form action="withdrawalDetails.php" method="POST">
                                <button class="clean edit-anc-btn hover1" type="submit" name="withdrawal_number" value="<?php echo $ls["withdrawal_number"];?>">
                                    <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="UpdateShipping" title="View Details">
                                    <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="UpdateShipping" title="View Details">
                                </button>
                            </form>
                        </td>

                      <?php } ?>
										</tr>
									<?php endforeach; ?>
								<?php else: ?>
									<tr>
										<td colspan="9">No result</td>
								</tr>
								<?php endif; ?>
                  </tbody>
            </table>
        </div>
    </div>

    <div class="clear"></div>


</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php


// function totalPending(){
//   $conn = connDB();
//   $result1 = mysqli_query($conn,"SELECT count(withdrawal_status) AS totalWithdrawalStatus FROM withdrawal  WHERE withdrawal_status = 'PENDING'");
//
//   if (mysqli_num_rows($result1) > 0) {
//   ?>
              <?php
//               $i=0;
//               while($row = mysqli_fetch_array($result1)) {
//               ?>
             <?php //echo $row["totalWithdrawalStatus"]; ?>

               <?php
//               $i++;
//               }
//
//               ?>
          <?php
//         }
//         else{
//             echo "0";
//         }
//
// }

 ?>

</body>
</html>
