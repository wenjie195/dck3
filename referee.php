<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/GroupCommission.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';


$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){

}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$userRows2 = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/referee.php" />
    <meta property="og:title" content="Referee | DCK Supreme" />
    <title>Referee | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK,DCK Supreme, supreme, DCK®, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/referee.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
<!--function to display profile picture-->
<?php include 'profilePictureDispalyPartA.php'; ?>
    <div class="right-profile-div">
    	<div class="profile-tab width100">
        	<a href="profile.php" class="profile-tab-a "><?php echo _MAINJS_PROFILE_ABOUT ?></a>
            <a href="#" class="profile-tab-a active-tab-a"><?php echo _MAINJS_PROFILE_MY_REFEREE ?></a>    
        </div>

        <div class="width100 oveflow">
        <h1 class="username"><?php echo _MAINJS_PROFILE_MY_REFEREE ?></h1>
            <?php
                $conn = connDB();
                    if($getWho){
                        // echo "<br/>";echo "<br/>";echo "<br/>";echo "<br/>";echo "<br/>";echo "<br/>";echo "<br/>";echo "<br/>";


                          echo '<ul>';
                          $lowestLevel = $getWho[0]->getCurrentLevel();
                          foreach($getWho as $thisPerson){
                              $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                              $thisTempUser = $tempUsers[0];

                              if($thisPerson->getCurrentLevel() == $lowestLevel){

                                  // echo '<li id="'.$thisPerson->getReferralId().'">'.$thisPerson->getReferralId().'</li>';
                                  echo '<li id="'.$thisPerson->getReferralId().'">'.$thisTempUser->getUsername().'</li>';

                              }
                            }

                          if (isset($_POST["mybutton"]))
                          {


                        echo '<ul>';
                        $lowestLevel = $getWho[0]->getCurrentLevel();
                        foreach($getWho as $thisPerson){
                            $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                            $thisTempUser = $tempUsers[0];


                                echo '
                                    <script type="text/javascript">
                                        var div = document.getElementById("'.$thisPerson->getReferrerId().'");

                                        div.innerHTML += "<ul name=\'ul-'.$thisPerson->getReferrerId().'\'><li id=\''.$thisPerson->getReferralId().'\'>'.$thisTempUser->getUsername().'</li></ul>";
                                    </script>
                                ';

                        }
                        echo '</ul>';

                    }elseif (isset($_POST["mybutton2"])) {


                    }
                }
            ?>
       </div>
       <form action="" method="POST">
         <button input type="submit" class="confirm-btn text-center white-text clean black-button anc-ow-btn" name="mybutton"><?php echo _MAINJS_REFEREE_EXPAND_ALL ?></button>
         <button input type="submit" class="confirm-btn text-center white-text clean black-button anc-ow-btn" name="mybutton2"><?php echo _MAINJS_REFEREE_COLLAPSE_ALL ?></button>





	</div>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>
