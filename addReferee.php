<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$products = getProduct($conn);
// $products = getProduct($conn," WHERE display = 1",array("display"),array(1),"i");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/addReferee.php" />
    <meta property="og:title" content="Add Referee | DCK Supreme" />
    <title>Add Referee | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/referee.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="username"><?php echo _MAINJS_ADD_REFEREE_TITLE ?></h1>
    <h3 class="left-h3"><?php echo _MAINJS_ADD_REFEREE_POINT ?> : <?php echo $userRefereeLimit = $userDetails->getUserPoint(); ?>Pts</h3> <h3 class="right-h3"><?php echo _MAINJS_ADD_REFEREE_BONUS ?> : RM<?php echo $userRefereeLimit = $userDetails->getBonus(); ?></h3>

    <?php
    $conn = connDB();
    $uid = $_SESSION['uid'];
    $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    $userDetails = $userRows[0];
    $userRefereeLimit = $userDetails->getUserPoint();

    if ($userRefereeLimit >=300) {
    ?>

    <!-- <form  id="addRefereeForm" class="edit-profile-div2" onsubmit="return registerNewMemberAccountValidation();" action="utilities/addNewRefereeFunction.php" method="POST"> -->
    <form name="checkboxLimited" method="POST" id="addRefereeForm" class="edit-profile-div2  add-referee-form" onsubmit="doPreview(this.submited); return false;">

    <table class="edit-profile-table password-table">
        	<tr class="profile-tr">
            	<td class=""><?php echo _MAINJS_ADD_REFEREE_USERNAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input class="clean edit-profile-input" type="text" placeholder="<?php echo _MAINJS_ADD_REFEREE_USERNAME ?>" id="register_username" name="register_username" required>
                    <!-- <input class="clean edit-profile-input" type="text" placeholder="Username" id="register_username" name="register_username" required> -->
                </td>
            </tr>
            <tr class="profile-tr">
              	<td class=""><?php echo _MAINJS_ADD_REFEREE_FULLNAME ?></td>
                  <td class="profile-td2">:</td>
                  <td class="profile-td3">
                    <input class="clean edit-profile-input" type="text" placeholder="<?php echo _MAINJS_ADD_REFEREE_FULLNAME ?>" id="register_fullname" name="register_fullname" required>
                    <!-- <input class="clean edit-profile-input" type="text" placeholder="Fullname" id="register_fullname" name="register_fullname" required> -->
                  </td>
              </tr>
        	<tr class="profile-tr">
            	<td><?php echo _MAINJS_ADD_REFEREE_IC_NO ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input class="clean edit-profile-input" type="text" placeholder="<?php echo _MAINJS_ADD_REFEREE_IC_NO ?>" id="register_ic_no" name="register_ic_no" required>
                    <!-- <input class="clean edit-profile-input" type="text" placeholder="IC Number" id="register_ic_no" name="register_ic_no" required> -->
                </td>
            </tr>
            <tr class="profile-tr">
            	<td><?php echo _MAINJS_ADD_REFEREE_EMAIL ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input class="clean edit-profile-input" type="text" placeholder="<?php echo _MAINJS_ADD_REFEREE_EMAIL ?>" id="register_email_user" name="register_email_user">
                    <!-- <input class="clean edit-profile-input" type="text" placeholder="Email" id="register_email_user" name="register_email_user"> -->
                </td>
            </tr>

            <tr class="profile-tr">
            	<td><?php echo _MAINJS_ADD_REFEREE_REFERRER_USERNAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input class="clean edit-profile-input" type="text" placeholder="<?php echo _MAINJS_ADD_REFEREE_REFERRER_USERNAME ?>" id="register_username_referrer" name="register_username_referrer" required>
                    <!-- <input class="clean edit-profile-input" type="text" placeholder="Referral's Username" id="register_username_referrer" name="register_username_referrer" required> -->
                </td>
            </tr>

            <tr class="profile-tr">
                <td><?php echo _MAINJS_ADD_REFEREE_PRODUCT ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <?php
                    if ($products)
                    {
                        $id = 1;
                        for ($cnt=0; $cnt < count ($products) ; $cnt++)
                        {
                            $idnew = $id++;
                            $query = $conn->query("SELECT images,name FROM product WHERE id = '$idnew'");

                            if($query->num_rows > 0)
                            {
                                while($row = $query->fetch_assoc())
                                {
                                $imageURL = 'ProductImages/'.$row["images"];
                                    if ($row["images"] != null)
                                    {

                                    ?>
                                    <div class="label-div">
                                        <label>
                                            <input type="checkbox" name="product" value="<?php echo $row["name"] ?>">
                                            <img src="<?php echo $imageURL;?>" alt="<?php echo $row["name"] ?>" title="<?php echo $row["name"] ?>" width="80">
                                            <p class="checkbox-product-p">Motorcycle Oil Booster 15pcs (1 set)</p>
                                        </label>
									</div>
                                    <?php
                                    }
                                }
                            }
                            ?><?php

                        }
                    }?>
                </td>
            </tr>

        </table>

        <!-- <input required class="login-input password-input clean" type="hidden" id="register_password" name="register_password" value="123321">
        <input required class="login-input password-input clean" type="hidden" id="register_retype_password" name="register_retype_password" value="123321"> -->

        <div class="clear"></div>

        <!-- <button class="confirm-btn text-center white-text clean black-button"name="refereeButton"><?php //echo _MAINJS_ADD_REFEREE_ADD_REFEREE_BUTTON ?></button> -->

        <!-- <button class="confirm-btn text-center white-text clean black-button"name="refereeButton">Self Collect</button>
        <button class="confirm-btn text-center white-text clean black-button"name="refereeButton">Courier</button> -->

        <!-- <input onclick="this.form.submited=this.value;"  type="submit" name="SELF COLLECT" value="SELF COLLECT" class="refund-btn-a white-button three-btn-a">
        <input onclick="this.form.submited=this.value;"  type="submit" name="COURIER" value="COURIER" class="shipout-btn-a black-button three-btn-a"> -->

        <input onclick="this.form.submited=this.value;"  type="submit" name="SELF COLLECT" value="SELF COLLECT" class="confirm-btn text-center white-text clean black-button same-size-btn same-size-btn1">
        <input onclick="this.form.submited=this.value;"  type="submit" name="COURIER" value="COURIER" class="confirm-btn text-center white-text clean black-button same-size-btn same-size-btn2">


    </form>

    <?php }

    else
    { ?>
        <center>
            <div class= "width100 oveflow">
            <div class="clear"></div>
                <div class="width20">
                    <div class="white50div">
                        <?php echo "*You Can't Register New Referral Because Your Current Point Is Less Than 300Pts." ?>
                    </div>
                </div>
            </div>
        </center>
    <?php
    $conn->close();
    } ?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';
}
?>

<script type="text/javascript">checkBoxLimit()
function checkBoxLimit()
{
	var checkBoxGroup = document.forms['checkboxLimited']['product'];
    var val = "1";
	var limit = val;
    
	for (var i = 0; i < checkBoxGroup.length; i++)
    {
		checkBoxGroup[i].onclick = function()
        {
			var checkedcount = 0;
			for (var i = 0; i < checkBoxGroup.length; i++) {
				checkedcount += (checkBoxGroup[i].checked) ? 1 : 0;
			}
			if (checkedcount > limit)
            {
            console.log("You can only select " + limit + " product ! ");
            alert("You can only select " + limit + " product !");
            this.checked = false;
            }
            // else if(checkedcount = limit2)
            // {
            // console.log("You need to select at least " + limit + " product ! ");
            // alert("You need to select at least " + limit + " product !");
            // this.checked = false;
            // }
		}
	}
}
</script>

<script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType){
            case 'SELF COLLECT':
                form=document.getElementById('addRefereeForm');
                // form.action='shippingReject.php';
                form.action='addRefereeSelfCollect.php';
                form.submit();
            break;
            case 'COURIER':
                form=document.getElementById('addRefereeForm');
                // form.action='utilities/updateShippingFunction.php';
                form.action='addRefereeCourier.php';
                form.submit();
            break;
        }

    }
</script>

</body>
</html>
