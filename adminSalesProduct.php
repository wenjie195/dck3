<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/SignUpProduct.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),"s");
$userDetails = $userRows[0];

//$products = getSignUpProduct($conn);
$products = getSignUpProduct($conn, "ORDER BY date_created DESC");

$productA = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Oil Booster 3pcs (1 set)'),"s");
$productB = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Motorcycle Oil Booster 15pcs (1 set)'),"s");
$productC = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Synthetic Plus Engine Oil 5w-30 (1 set)'),"s");
$productD = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Synthetic Plus Engine Oil 10w-40 (1 set)'),"s");
$productE = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Oil Booster (1pcs) + Motorcycle Oil Booster (10pcs)'),"s");
$productF = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Oil Booster (2pcs) + Motorcycle Oil Booster (5pcs)'),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminSalesProduct.php" />
    <meta property="og:title" content="Sales (By Products) | DCK Supreme" />
    <title>Sales (By Products) | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminSalesProduct.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <!-- <h1 class="h1-title h1-before-border"><?php //echo _MAINJS_ADMSALES_SALES ?></h1> -->
    <h1 class="h1-title h1-before-border">Sales (By Products)</h1>

    <?php
    if($productA)
        {   //echo count($aaa);
            $totalProductA = count($productA);
        }
    else    {   $totalProductB = 0 ;  }
    ?>
    <?php
    if($productB)
        {   $totalProductB = count($productB);  }
    else    {   $totalProductB = 0 ;  }
    ?>
    <?php
    if($productC)
        {   $totalProductC = count($productC);  }
    else    {   $totalProductC = 0 ;  }
    ?>
    <?php
    if($productD)
        {   $totalProductD = count($productD);  }
    else    {   $totalProductD = 0 ;  }
    ?>
    <?php
    if($productE)
        {   $totalProductE = count($productE);  }
    else    {   $totalProductE = 0 ;  }
    ?>
    <?php
    if($productF)
        {   $totalProductF = count($productF);  }
    else    {   $totalProductF = 0 ;  }
    ?>

    <?php
    if($products)
    {   $productSold = count($products);  }

    // if($productF)
    // {   
    //     $totalProductSell = $productA + $productB + $productC + $productD + $productE + $productF ;  
    // }
    ?>

    <div class="border-top100 four-div-container admin-dash admin-sales">
        
    	<a href="#" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/sales.png" class="four-img hover1a" alt="Total Sales" title="Total Sales">
                <img src="img/sales2.png" class="four-img hover1b" alt="Total Sales" title="Total Sales">
                <!-- Just call out the number will do, please remove all the html table etc--> 
                <p class="four-div-p four-div-p1"><b>Total Sales</b></p>
                <p class="four-div-p four-div-p2"><b><?php echo $productSold; ?></b></p>
            </div>
        </a>

        <!-- <a href="adminShpping.php" class="black-text"> -->
        <a href="#" class="black-text">
            <div class="four-white-div hover1 four-middle-div1 white-div-yellow">
                <img src="img/motor-oil1.png" class="four-img hover1a" alt="Oil Booster 3pcs (1 set)" title="Oil Booster 3pcs (1 set)">
                <img src="img/motor-oil2.png" class="four-img hover1b" alt="Oil Booster 3pcs (1 set)" title="Oil Booster 3pcs (1 set)">
                <p class="four-div-p four-div-p1"><b>Oil Booster 3pcs (1 set)</b></p>
                <p class="four-div-p four-div-p2"><b><?php echo $totalProductA; ?></b></p>
            </div>
        </a>
        <a href="#" class="black-text">
            <div class="four-white-div hover1 four-middle-div2 white-div-yellow">
                <img src="img/product1.png" class="four-img hover1a" alt="Motorcycle Oil Booster 15pcs (1 set)" title="Motorcycle Oil Booster 15pcs (1 set)">
                <img src="img/product2.png" class="four-img hover1b" alt="Motorcycle Oil Booster 15pcs (1 set)" title="Motorcycle Oil Booster 15pcs (1 set)">
                <p class="four-div-p four-div-p1"><b>Motorcycle Oil Booster 15pcs (1 set)</b></p>
                <p class="four-div-p four-div-p2"><b><?php echo $totalProductB; ?></b></p>
            </div>
        </a>
        <a href="#" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/engine-oil1.png" class="four-img hover1a" alt="Synthetic Plus Engine Oil 5w-30 (1 set)" title="Synthetic Plus Engine Oil 5w-30 (1 set)">
                <img src="img/engine-oil2.png" class="four-img hover1b" alt="Synthetic Plus Engine Oil 5w-30 (1 set)" title="Synthetic Plus Engine Oil 5w-30 (1 set)">
                <p class="four-div-p four-div-p1"><b>Synthetic Plus Engine Oil 5w-30 (1 set)</b></p>
                <p class="four-div-p four-div-p2"><b><?php echo $totalProductC; ?></b></p>
            </div>
        </a>

    	<a href="#" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/oil-booster-a1.png" class="four-img hover1a" alt="Synthetic Plus Engine Oil 10w-40 (1 set)" title="Synthetic Plus Engine Oil 10w-40 (1 set)">
                <img src="img/oil-booster-a2.png" class="four-img hover1b" alt="Synthetic Plus Engine Oil 10w-40 (1 set)" title="Synthetic Plus Engine Oil 10w-40 (1 set)">
                <p class="four-div-p four-div-p1"><b>Synthetic Plus Engine Oil 10w-40 (1 set)</b></p>
                <p class="four-div-p four-div-p2"><b><?php echo $totalProductD; ?></b></p>
            </div>
        </a>
        <a href="#" class="black-text">
            <div class="four-white-div hover1 four-middle-div1 white-div-yellow">
                <img src="img/product5.png" class="four-img hover1a" alt="Oil Booster (1pcs) + Motorcycle Oil Booster (10pcs)" title="Oil Booster (1pcs) + Motorcycle Oil Booster (10pcs)">
                <img src="img/product5-2.png" class="four-img hover1b" alt="Oil Booster (1pcs) + Motorcycle Oil Booster (10pcs)" title="Oil Booster (1pcs) + Motorcycle Oil Booster (10pcs)">
                <p class="four-div-p four-div-p1"><b>Oil Booster (1pcs) + Motorcycle Oil Booster (10pcs)</b></p>
                <p class="four-div-p four-div-p2"><b><?php echo $totalProductE; ?></b></p>
            </div>
        </a>
        <a href="#" class="black-text">
            <div class="four-white-div hover1 four-middle-div2 white-div-yellow">
                <img src="img/product6.png" class="four-img hover1a" alt="Oil Booster (2pcs) + Motorcycle Oil Booster (5pcs)" title="Oil Booster (2pcs) + Motorcycle Oil Booster (5pcs)">
                <img src="img/product6-2.png" class="four-img hover1b" alt="Oil Booster (2pcs) + Motorcycle Oil Booster (5pcs)" title="Oil Booster (2pcs) + Motorcycle Oil Booster (5pcs)">
                <p class="four-div-p four-div-p1"><b>Oil Booster (2pcs) + Motorcycle Oil Booster (5pcs)</b></p>
                <p class="four-div-p four-div-p2"><b><?php echo $totalProductF; ?></b></p>
            </div>
        </a>        

    </div>

    <div class="clear"></div>

    <!-- <h1 class="h1-title extra-mtop2">Sales of This Week</h1> -->
        <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table">
                    <thead>
                        <tr>
                            <th>NO.</th>
                            <th>USERNAME</th>
                            <th>FULLNAME</th>
                            <th>PRODUCT</th>
                            <th>QUANTITY</th>
                            <th>DATE</th>
                            <th>RECEIPT</th>
                        </tr>
                    </thead>

                    <tbody>

                    <?php
                    if($products)
                    {   
                        // echo count($userSignUpProduct);
                        for($cnt = 0;$cnt < count($products) ;$cnt++)
                        {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $products[$cnt]->getReferralName();?></td>
                                <td><?php echo $products[$cnt]->getReferralFullname();?></td>
                                <td><?php echo $products[$cnt]->getProduct();?></td>
                                <td><?php echo $products[$cnt]->getQuantity();?></td>
                                <td>
                                    <?php $dateCreated = date("Y-m-d",strtotime($products[$cnt]->getDateCreated()));echo $dateCreated;?>
                                </td>
                                <td>
                                    <form action="adminSignUpProductDetails.php" method="POST">
                                        <button class="clean edit-anc-btn hover1" type="submit" name="signupproduct_id" value="<?php echo $products[$cnt]->getId();?>">
                                            <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="Shipping Out" title="Details">
                                            <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="Shipping Out" title="Details">
                                        </button>
                                    </form>
                                </td>
        
                        <?php
                        }?>
                            </tr>
                            <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>