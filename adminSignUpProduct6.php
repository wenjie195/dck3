<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/SignUpProduct.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

// $memberList = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"i");

$userSignUpProduct = getSignUpProduct($conn);

$aaa = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Oil Booster 3pcs (1 set) RM300'),"s");
$bbb = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Motorcycle Oil Booster 15pcs (1 set) RM300'),"s");
$ccc = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Synthetic Plus Engine Oil 5w-30 (1 set) RM300'),"s");
$ddd = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Synthetic Plus Engine Oil 10w-40  (1 set) RM300'),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminSignUpProduct6.php" />
    <meta property="og:title" content="Sign Up Product DCK Oil Booster (1pcs) + DCK Motorcycle Oil Booster (10pcs) | DCK Supreme" />
    <title>Sign Up Product DCK Oil Booster (1pcs) + DCK Motorcycle Oil Booster (10pcs) | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminSignUpProduct6.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="details-h1">User Sign Up Product</h1>

    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest</option>
    	<option class="filter-option">Oldest</option>
    </select> -->
    <!-- End of Filter -->

    <!-- <div class="clear"></div> -->

	<!-- <div class="search-container0">

            <div class="shipping-input clean smaller-text2">
                <p>Name</p>
                <input class="shipping-input2 clean normal-input" type="text" placeholder="Name">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>Contact</p>
                <input class="shipping-input2 clean normal-input" type="number" placeholder="Contact">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p>Email</p>
                <input class="shipping-input2 clean normal-input" type="email" placeholder="Email">
            </div>
            <div class="shipping-input clean smaller-text2 second-shipping">
                <p>Start Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="Start Date">
            </div>


            <div class="shipping-input clean middle-shipping-div smaller-text2">
                <p>End Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="End Date">
            </div>

            <button class="clean black-button shipping-search-btn second-shipping">Search</button>

    </div>     -->

    <?php
    if($aaa)
        {   //echo count($aaa);
            $aaaa = count($aaa);
        }else {
          $aaaa = 0;
        }
    ?>
    <?php
    if($bbb)
        {   //echo count($bbb);
            $bbbb = count($bbb);

        }else {
          $bbbb = 0;
        }
    ?>
    <?php
    if($ccc)
        {   //echo count($ccc);
            $cccc = count($ccc);

        }else {
          $cccc = 0;
        }
    ?>
    <?php
    if($ddd)
        {   //echo count($ddd);
            $dddd = count($ddd);

        }else {
          $dddd = 0;
        }
    ?>
    <div class="border-top100 four-div-container admin-dash admin-sales">
    	<a href="adminSignUpProduct.php" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/sales.png" class="four-img hover1a" alt="<?php echo _MAINJS_ADMSALES_TOTAL ?>" title="<?php echo _MAINJS_ADMSALES_TOTAL ?>">
                <img src="img/sales2.png" class="four-img hover1b" alt="<?php echo _MAINJS_ADMSALES_TOTAL ?>" title="<?php echo _MAINJS_ADMSALES_TOTAL ?>">
                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMSALES_TOTAL ?></b></p>
                <!-- Just call out the number will do, please remove all the html table etc-->
                <p class="four-div-p four-div-p2"><b><?php //echo totalProductDashboard(); ?>30</b></p>
            </div>
        </a>
        <!-- <a href="adminShpping.php" class="black-text"> -->
     	<a href="adminSignUpProduct2.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div1 white-div-yellow">
                <img src="img/motor-oil1.png" class="four-img hover1a" alt="<?php echo _MAINJS_ADMSALES_PRODUCT ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT ?>">                
                <img src="img/motor-oil2.png" class="four-img hover1b" alt="<?php echo _MAINJS_ADMSALES_PRODUCT ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT ?>">                
                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMSALES_PRODUCT ?></b></p>
                <p class="four-div-p four-div-p2"><b><?php echo $aaaa;?></b></p>
            </div>
	 	</a>
        <a href="adminSignUpProduct3.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div2 white-div-yellow">
                <img src="img/product1.png" class="four-img hover1a" alt="<?php echo _MAINJS_ADMSALES_PRODUCT2 ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT2 ?>">
                <img src="img/product2.png" class="four-img hover1b" alt="<?php echo _MAINJS_ADMSALES_PRODUCT2 ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT2 ?>">
                
                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMSALES_PRODUCT2 ?></b></p>
                <p class="four-div-p four-div-p2"><b><?php echo $cccc;?></b></p>
            </div>
        </a>
        <a href="adminSignUpProduct4.php" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/engine-oil2.png" class="four-img hover1a" alt="<?php echo _MAINJS_ADMSALES_PRODUCT3 ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT3 ?>">
                <img src="img/engine-oil2.png" class="four-img hover1b" alt="<?php echo _MAINJS_ADMSALES_PRODUCT3 ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT3 ?>">
                
                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMSALES_PRODUCT3 ?></b></p>
                <p class="four-div-p four-div-p2"><b><?php echo $dddd;?></b></p>
            </div>
        </a>
    	<a href="adminSignUpProduct5.php" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/oil-booster-a1.png" class="four-img hover1a" alt="<?php echo _MAINJS_ADMSALES_PRODUCT4 ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT4 ?>">
                <img src="img/oil-booster-a2.png" class="four-img hover1b" alt="<?php echo _MAINJS_ADMSALES_PRODUCT4 ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT4 ?>">
                
                <p class="four-div-p four-div-p1 p3-p"><b><?php echo _MAINJS_ADMSALES_PRODUCT4 ?></b></p>
                <!-- Just call out the number will do, please remove all the html table etc-->
                <p class="four-div-p four-div-p2"><b><?php echo $bbbb;?></b></p>
            </div>
        </a>
        <!-- <a href="adminShpping.php" class="black-text"> -->
        <!--<a href="adminSignUpProduct6.php" class="black-text">-->
            <div class="four-white-div hover1 four-middle-div1 white-div-yellow active-black-div">
                <!--<img src="img/product5.png" class="four-img hover1a" alt="<?php echo _MAINJS_ADMSALES_PRODUCT5 ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT5 ?>">-->
                <img src="img/product5-2.png" class="four-img" alt="<?php echo _MAINJS_ADMSALES_PRODUCT5 ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT5 ?>">
                
                <p class="four-div-p four-div-p1 p3-p"><b><?php echo _MAINJS_ADMSALES_PRODUCT5 ?></b></p>
                <p class="four-div-p four-div-p2"><b><?php //echo shippingRequest() ?>10</b></p>
            </div>
        <!--</a>-->
        <a href="adminSignUpProduct7.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div2 white-div-yellow">
                <img src="img/product6.png" class="four-img hover1a" alt="<?php echo _MAINJS_ADMSALES_PRODUCT6 ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT6 ?>">
                <img src="img/product6-2.png" class="four-img hover1b" alt="<?php echo _MAINJS_ADMSALES_PRODUCT6 ?>" title="<?php echo _MAINJS_ADMSALES_PRODUCT6 ?>">
                
                <p class="four-div-p four-div-p1 p3-p"><b><?php echo _MAINJS_ADMSALES_PRODUCT6 ?></b></p>
                <p class="four-div-p four-div-p2"><b><?php //echo totalMemberJoined()?>10</b></p>
            </div>
        </a>        
        
    </div>


    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>NAME</th>
                        <th>PRODUCT</th>
                        <th>QUANTITY</th>
                        <th>SIGN UP BY</th>
                        <th>DATE</th>
                        <th>DETAILS</th>
                    </tr>
                </thead>

                <tbody>

                <?php
                if($userSignUpProduct)
                {
                    // echo count($userSignUpProduct);
                    for($cnt = 0;$cnt < count($userSignUpProduct) ;$cnt++)
                    {?>
                        <tr>
                            <!-- <td><?php //echo ($cnt+1)?></td> -->
                            <!-- <td><?php //echo $userSignUpProduct[$cnt]->getReferralUid();?></td> -->
                            <td><?php echo $userSignUpProduct[$cnt]->getId();?></td>
                            <td><?php echo $userSignUpProduct[$cnt]->getReferralName();?></td>
                            <td><?php echo $userSignUpProduct[$cnt]->getProduct();?></td>
                            <td><?php echo $userSignUpProduct[$cnt]->getQuantity();?></td>
                            <td><?php echo $userSignUpProduct[$cnt]->getReferrerName();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($userSignUpProduct[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>

                            <td>
                                <form action="adminSignUpProductDetails.php" method="POST">
                                    <button class="clean edit-anc-btn hover1" type="submit" name="signupproduct_id" value="<?php echo $userSignUpProduct[$cnt]->getId();?>">
                                        <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="Shipping Out" title="Details">
                                        <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="Shipping Out" title="Details">
                                    </button>
                                </form>
                            </td>

                    <?php
                    }?>
                        </tr>
                        <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script>
function goBack()
{
  window.history.back();
}
</script>

</body>
</html>
