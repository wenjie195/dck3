-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2019 at 04:06 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_oilxag`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `address_line_3`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_date`, `payment_time`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `reject_reason`, `refund_method`, `refund_amount`, `refund_note`, `refund_reason`, `receipt`, `date_created`, `date_updated`) VALUES
(192, '3e072a0ea0c47d4429499685c4464afd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 05:18:55', '2019-11-05 05:18:55'),
(193, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'CIMB BANK BERHAD', NULL, 234567, 'mus', '111', NULL, 'ytrew', 'hgfds', NULL, 'asfdgh', '11111', 'iujh', 'jhgfd', '600', '630', 'CDM', 630, '23456', '0002-01-02', '21:09', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-07', NULL, NULL, NULL, 600, NULL, 'Running Out Of Stock', NULL, '2019-11-05 05:18:56', '2019-11-12 03:55:32'),
(194, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, NULL, '01155670433', NULL, '25 kg tok khamis, west, west, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '750', '780', 'Online Banking', 780, '23456', '2019-11-14', '10:40', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-14 02:38:35', '2019-11-14 02:40:17'),
(195, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Capture4.PNG', '01155670433', NULL, '25 kg tok khamis, west, west, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '750', '780', 'CDM', 780, '432', '2019-11-14', '10:42', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Capture4.PNG', '2019-11-14 02:42:26', '2019-11-14 02:44:02'),
(196, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Mustari Shafiq', '01155670433', NULL, '25 kg tok khamis, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '1650', '1680', 'CDM', 1680, '23456', '2019-11-14', '10:46', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Capture.PNG', '2019-11-14 02:46:01', '2019-11-14 02:46:42'),
(197, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Severus', '01155670433', NULL, '25 kg tok khamis, west, west, west, west, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '250', '280', 'Online Banking', 280, '23456', '2019-11-14', '10:49', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'backup.PNG', '2019-11-14 02:49:10', '2019-11-14 02:49:40'),
(198, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Mustari Shafiq', '01155670433', NULL, '25 kg tok khamis, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '550', '580', 'CDM', 550, '87654', '2019-11-14', '10:54', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-14', 'desa', NULL, NULL, NULL, NULL, NULL, 'Capture2.PNG', '2019-11-14 02:54:08', '2019-11-14 03:01:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
