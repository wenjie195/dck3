-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2019 at 03:55 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_oilxag`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `announce_id` int(255) NOT NULL,
  `announce_message` varchar(25000) NOT NULL,
  `announce_showThis` int(2) NOT NULL,
  `announce_dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `announce_lastUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`announce_id`, `announce_message`, `announce_showThis`, `announce_dateCreated`, `announce_lastUpdated`) VALUES
(1, 'Thanks for joining us, We welcome u to our journey', 1, '2019-08-14 04:17:07', '2019-08-14 06:58:37'),
(2, 'Have A nice day ! Fizo', 1, '2019-08-14 04:29:46', '2019-08-14 06:58:21'),
(3, 'Welcome To Oilxag People', 1, '2019-08-14 06:24:43', '2019-08-15 02:01:14'),
(4, 'Mun cun  eeedsafafasdfasd', 0, '2019-08-15 02:01:49', '2019-08-15 02:24:18'),
(5, 'asdsdaasdccc vvvvvvvv', 0, '2019-08-15 02:26:06', '2019-08-15 02:26:17'),
(6, 'lyon is noob team', 1, '2019-08-15 02:26:24', '2019-08-28 06:27:42'),
(7, 'GuangZhou Evergrande BIG BIG!!', 0, '2019-08-28 06:27:16', '2019-10-17 09:59:26'),
(8, 'lalala', 0, '2019-09-10 09:23:39', '2019-09-10 09:23:47'),
(9, 'dadada', 0, '2019-09-10 09:24:04', '2019-09-10 09:24:10'),
(10, 'Manchester United Vs Arsenal', 0, '2019-10-17 10:02:40', '2019-10-18 01:20:51');

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `id` int(255) NOT NULL,
  `referral_name` varchar(100) NOT NULL,
  `referral_id` varchar(255) NOT NULL,
  `referrer_name` varchar(100) NOT NULL,
  `referrer_id` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL,
  `date_created` timestamp(3) NOT NULL DEFAULT current_timestamp(3) ON UPDATE current_timestamp(3),
  `register_downline_no` int(100) NOT NULL,
  `amount` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonus`
--

INSERT INTO `bonus` (`id`, `referral_name`, `referral_id`, `referrer_name`, `referrer_id`, `current_level`, `top_referrer_id`, `date_created`, `register_downline_no`, `amount`) VALUES
(65, 'test', '77db4bbd658104a4df91db8bd8e052ba', 'ronaldo', '31749902888327c9815060fc08a347ad', 3, '3e072a0ea0c47d4429499685c4464afd', '2019-11-08 06:29:42.373', 2, 150),
(66, 'testt', 'fc701905a110ceb14fa6c2e0298af389', 'ronaldo', '31749902888327c9815060fc08a347ad', 3, '3e072a0ea0c47d4429499685c4464afd', '2019-11-08 06:31:58.911', 3, 50);

-- --------------------------------------------------------

--
-- Table structure for table `cash_to_point`
--

CREATE TABLE `cash_to_point` (
  `id` int(11) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `point` int(255) NOT NULL,
  `date_create` timestamp(3) NOT NULL DEFAULT current_timestamp(3) ON UPDATE current_timestamp(3),
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_to_point`
--

INSERT INTO `cash_to_point` (`id`, `uid`, `name`, `point`, `date_create`, `status`) VALUES
(1, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 100, '2019-10-23 06:25:27.618', 'COMPLETED'),
(2, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 500, '2019-10-31 09:16:11.301', 'COMPLETED'),
(3, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 400, '2019-10-31 09:20:12.414', 'COMPLETED'),
(4, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 500, '2019-10-31 09:23:05.838', 'COMPLETED'),
(5, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 500, '2019-10-31 09:25:26.502', 'COMPLETED'),
(6, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 500, '2019-10-31 09:33:06.940', 'COMPLETED'),
(7, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 250, '2019-10-31 09:34:31.852', 'COMPLETED'),
(8, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 500, '2019-10-31 09:36:10.868', 'COMPLETED'),
(9, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 500, '2019-10-31 09:43:23.052', 'COMPLETED'),
(10, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 400, '2019-10-31 09:44:34.559', 'COMPLETED'),
(11, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 500, '2019-10-31 09:47:13.272', 'COMPLETED'),
(12, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 400, '2019-10-31 09:48:27.111', 'COMPLETED'),
(13, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 499, '2019-10-31 09:51:31.413', 'COMPLETED'),
(14, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 491, '2019-10-31 09:54:09.941', 'COMPLETED'),
(15, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 500, '2019-10-31 09:57:24.814', 'COMPLETED'),
(16, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 600, '2019-10-31 09:59:40.867', 'COMPLETED'),
(17, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 300, '2019-10-31 10:16:32.657', 'COMPLETED');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 672),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `group_commission`
--

CREATE TABLE `group_commission` (
  `id` int(255) NOT NULL,
  `commission` decimal(30,2) NOT NULL,
  `level` int(3) DEFAULT NULL,
  `count` int(3) DEFAULT NULL COMMENT 'This is for number of referrals that the user has referred and has purchased a product',
  `description` varchar(5000) DEFAULT NULL,
  `transaction_type_id` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_commission`
--

INSERT INTO `group_commission` (`id`, `commission`, `level`, `count`, `description`, `transaction_type_id`, `date_created`, `date_updated`) VALUES
(1, '8.00', 1, NULL, 'in percent', 2, '2019-07-25 09:17:37', '2019-07-25 09:25:28'),
(2, '8.00', 2, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:13'),
(3, '6.00', 3, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:15'),
(4, '6.00', 4, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:17'),
(5, '4.00', 5, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:19'),
(6, '4.00', 6, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:20'),
(7, '2.00', 7, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:31'),
(8, '2.00', 8, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:33'),
(9, '1.00', 9, NULL, 'in percent', 2, '2019-07-25 09:18:43', '2019-07-25 09:25:36'),
(10, '5000.00', NULL, 1, 'in points (RM1 = 100).first referral.', 1, '2019-07-25 09:50:48', '2019-07-25 09:50:58'),
(11, '10000.00', NULL, 2, 'in points (RM1 = 100).second referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:36'),
(12, '15000.00', NULL, 3, 'in points (RM1 = 100).third referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:39'),
(13, '5000.00', NULL, 0, 'in points (RM1 = 100).subsequent referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:43'),
(14, '1.00', 10, NULL, 'in percent', 2, '2019-08-08 06:06:46', '2019-08-08 06:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `pid` int(255) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`pid`, `filename`, `uploaded`, `status`) VALUES
(124, 'messi1568963506.jpg', '2019-09-20 07:11:46', '1'),
(125, 'Martial1569385433.jpg', '2019-09-25 04:23:53', '1'),
(126, 'atlet0081571210133.jpg', '2019-10-16 07:15:33', '1'),
(127, 'atlet0081571282175.jpg', '2019-10-17 03:16:15', '1'),
(128, 'atlet0081571282337.jpg', '2019-10-17 03:18:57', '1'),
(129, 'atlet0081572849623.jpg', '2019-11-04 06:40:23', '1'),
(130, 'atlet0081573639008.jpg', '2019-11-13 09:56:48', '1');

-- --------------------------------------------------------

--
-- Table structure for table `money_type`
--

CREATE TABLE `money_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `money_type`
--

INSERT INTO `money_type` (`id`, `name`, `description`, `date_created`, `date_updated`) VALUES
(1, 'user_real', 'this is user\'s real money. can withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00'),
(2, 'voucher', 'this money is gotten from certain commissions. cannot withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `address_line_3`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_date`, `payment_time`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `reject_reason`, `refund_method`, `refund_amount`, `refund_note`, `refund_reason`, `receipt`, `date_created`, `date_updated`) VALUES
(194, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, NULL, '01155670433', NULL, '25 kg tok khamis, west, west, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '750', '780', 'Online Banking', 780, '23456', '2019-11-14', '10:40', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-14 02:38:35', '2019-11-14 06:38:55'),
(195, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Capture4.PNG', '01155670433', NULL, '25 kg tok khamis, west, west, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '750', '780', 'CDM', 780, '432', '2019-11-14', '10:42', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-14', NULL, NULL, NULL, NULL, NULL, NULL, 'Capture4.PNG', '2019-11-14 02:42:26', '2019-11-14 03:48:38'),
(196, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Mustari Shafiq', '01155670433', NULL, '25 kg tok khamis, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '1650', '1680', 'CDM', 1680, '23456', '2019-11-14', '10:46', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-14', NULL, NULL, NULL, NULL, NULL, NULL, 'Capture.PNG', '2019-11-14 02:46:01', '2019-11-14 03:48:59'),
(197, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Severus', '01155670433', NULL, '25 kg tok khamis, west, west, west, west, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '250', '280', 'Online Banking', 280, '23456', '2019-11-14', '10:49', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-14', NULL, NULL, NULL, NULL, NULL, NULL, 'backup.PNG', '2019-11-14 02:49:10', '2019-11-14 03:48:01'),
(198, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Mustari Shafiq', '01155670433', NULL, '25 kg tok khamis, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '550', '580', 'CDM', 550, '87654', '2019-11-14', '10:54', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-14', 'desa', NULL, NULL, NULL, NULL, NULL, 'Capture2.PNG', '2019-11-14 02:54:08', '2019-11-14 03:01:53'),
(199, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Mustari Shafiq', '01155670433', NULL, '25 kg tok khamis, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '850', '880', 'Online Banking', 880, '23456', '2019-11-14', '12:00', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-14', NULL, NULL, NULL, NULL, NULL, NULL, 'backup.PNG', '2019-11-14 03:59:59', '2019-11-14 04:00:44'),
(200, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Mustari Shafiq', '01155670433', NULL, '25 kg tok khamis, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '900', '930', 'Online Banking', 930, '23456', '2019-11-14', '12:02', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-14 04:02:01', '2019-11-14 04:02:43'),
(201, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Mustari Shafiq', '01155670433', NULL, '25 kg tok khamis, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '1700', '1730', 'Online Banking', 1730, '23456', '2019-11-14', '13:17', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-14 05:16:53', '2019-11-14 05:17:48'),
(202, 'f66af1f432639aab4f1ff2f6daad96b3', 'messi', 'ALLIANCE BANK MALAYSIA BERHAD', 'Lional Messi', 2147483647, 'Mustari Shafiq', '01155670433', NULL, '25 kg tok khamis, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '850', '880', 'Online Banking', 880, '23456', '2019-11-14', '14:27', 'ACCEPTED', 'SHIPPED', 'POSLAJU', '2019-11-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-14 06:27:40', '2019-11-14 06:28:20'),
(203, '3e072a0ea0c47d4429499685c4464afd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-15 01:21:54', '2019-11-15 01:21:54'),
(204, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Mustari Shafiq', '01155670433', NULL, '25 kg tok khamis, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '250', '280', '-', 280, '23456', '2019-11-15', '10:10', 'ACCEPTED', 'PENDING', NULL, '2019-11-15', NULL, NULL, NULL, NULL, NULL, NULL, 'Capture.PNG', '2019-11-15 02:10:08', '2019-11-15 02:10:54'),
(205, '3e072a0ea0c47d4429499685c4464afd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-15 02:15:39', '2019-11-15 02:15:39'),
(206, '3e072a0ea0c47d4429499685c4464afd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-15 02:21:21', '2019-11-15 02:21:21'),
(207, '3e072a0ea0c47d4429499685c4464afd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-15 02:21:55', '2019-11-15 02:21:55'),
(208, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Mustari Shafiq', '01155670433', NULL, '25 kg tok khamis, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '300', '330', 'CDM', 330, '23456', '2019-11-15', '10:38', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-15 02:37:21', '2019-11-15 02:38:41'),
(209, '3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'AFFIN BANK BERHAD', 'Severus', 234567, 'Severus', '01155670433', NULL, '25 kg tok khamis, west, west, west, west, west, west', 'west', NULL, 'Sg petani', '08000', 'Kedah', 'Malaysia', '300', '330', 'Online Banking', 330, '23456', '2019-11-15', '10:51', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-15 02:51:03', '2019-11-15 02:51:19');

-- --------------------------------------------------------

--
-- Table structure for table `payout_history`
--

CREATE TABLE `payout_history` (
  `ph_id` bigint(20) NOT NULL,
  `record_type_id` int(11) NOT NULL,
  `ref_id` varchar(20) NOT NULL,
  `username` varchar(100) NOT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `amount` decimal(11,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `date_create` datetime NOT NULL,
  `is_payout` int(11) NOT NULL,
  `date_payout` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payout_history`
--

INSERT INTO `payout_history` (`ph_id`, `record_type_id`, `ref_id`, `username`, `remark`, `amount`, `status`, `date_create`, `is_payout`, `date_payout`) VALUES
(1, 1, '34', 'leonardthong', '', '50.00', 1, '2019-11-01 08:06:30', 1, '2019-11-01 08:06:30'),
(2, 1, '35', 'leonardthong', '', '50.00', 1, '2019-11-01 08:09:11', 1, '2019-11-01 08:09:11'),
(3, 1, '36', 'leonardthong', '', '50.00', 1, '2019-11-01 08:12:29', 1, '2019-11-01 08:12:29'),
(4, 1, '37', 'leonardthong', '', '50.00', 1, '2019-11-01 08:38:33', 1, '2019-11-01 08:38:33'),
(5, 2, '144', 'leonardthong', '', '40.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(6, 2, '145', 'leonardthong', '', '40.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(7, 2, '146', 'leonardthong', '', '40.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(8, 2, '147', 'leonardthong', '', '40.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(9, 2, '148', 'leonardthong', '', '40.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(10, 2, '149', 'leonardthong', '', '40.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(11, 2, '150', 'leonardthong', '', '80.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(12, 2, '151', 'leonardthong', '', '80.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(13, 2, '152', 'leonardthong', '', '80.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(14, 2, '153', 'leonardthong', '', '80.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(15, 2, '154', 'leonardthong', '', '80.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(16, 2, '155', 'leonardthong', '', '80.00', 1, '2019-11-01 10:12:48', 1, '2019-11-01 10:12:48'),
(17, 2, '144', 'leonardthong', '', '40.00', 1, '2019-11-01 10:15:35', 1, '2019-11-01 10:15:35'),
(18, 2, '144', 'leonardthong', '', '40.00', 1, '2019-11-01 10:16:05', 1, '2019-11-01 10:16:05'),
(19, 2, '144', 'leonardthong', '', '40.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(20, 2, '145', 'leonardthong', '', '40.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(21, 2, '146', 'leonardthong', '', '40.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(22, 2, '147', 'leonardthong', '', '40.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(23, 2, '148', 'leonardthong', '', '40.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(24, 2, '149', 'leonardthong', '', '40.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(25, 2, '150', 'leonardthong', '', '80.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(26, 2, '151', 'leonardthong', '', '80.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(27, 2, '152', 'leonardthong', '', '80.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(28, 2, '153', 'leonardthong', '', '80.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(29, 2, '154', 'leonardthong', '', '80.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(30, 2, '155', 'leonardthong', '', '80.00', 1, '2019-11-01 10:16:30', 1, '2019-11-01 10:16:30'),
(31, 2, '155', 'leonardthong', NULL, '80.00', 1, '2019-11-01 10:40:58', 1, '2019-11-01 10:40:58');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(50,0) NOT NULL COMMENT 'With MYR as base and is in point system (RM1 = 100 point)',
  `stock` int(255) NOT NULL,
  `buy_stock` int(255) NOT NULL DEFAULT 0,
  `total_price` int(255) NOT NULL DEFAULT 0,
  `display` int(11) NOT NULL DEFAULT 1,
  `type` int(255) NOT NULL DEFAULT 1 COMMENT '1 = normal product',
  `description` varchar(10000) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `images` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `stock`, `buy_stock`, `total_price`, `display`, `type`, `description`, `date_created`, `date_updated`, `images`) VALUES
(1, 'DCK Engine Oil Booster', '300', 483, 17, 5100, 1, 1, 'MAIN PRODUCT ALREADY BEING EDIT', '2019-07-24 09:17:04', '2019-11-14 06:28:20', 'DCK-Engine-Oil-Booster.png'),
(2, 'DCK Synthetic Plus Motor Oil (SAE 5w – 30)', '300', 489, 11, 3300, 1, 1, 'VERY GOOD PRODUCT', '2019-07-25 04:11:21', '2019-11-14 06:28:20', '5w.png'),
(5, 'DCK Synthetic Plus Motor Oil (SAE 10w – 40)', '250', 231, 19, 4750, 1, 1, 'Cars Oil', '2019-10-24 02:40:55', '2019-11-14 06:38:55', '10w.png');

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` int(10) NOT NULL,
  `final_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `original_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `discount_given` decimal(50,0) NOT NULL COMMENT 'in points',
  `totalProductPrice` decimal(50,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_orders`
--

INSERT INTO `product_orders` (`id`, `product_id`, `order_id`, `quantity`, `final_price`, `original_price`, `discount_given`, `totalProductPrice`, `date_created`, `date_updated`) VALUES
(75, 1, 192, 2, '300', '300', '0', '0', '2019-11-05 05:18:55', '2019-11-05 05:18:55'),
(76, 2, 192, 0, '300', '300', '0', '0', '2019-11-05 05:18:55', '2019-11-05 05:18:55'),
(77, 5, 192, 0, '250', '250', '0', '0', '2019-11-05 05:18:55', '2019-11-05 05:18:55'),
(78, 1, 193, 2, '300', '300', '0', '600', '2019-11-05 05:18:56', '2019-11-05 05:18:56'),
(79, 5, 194, 3, '250', '250', '0', '750', '2019-11-14 02:38:35', '2019-11-14 02:38:35'),
(80, 5, 195, 3, '250', '250', '0', '750', '2019-11-14 02:42:26', '2019-11-14 02:42:26'),
(81, 2, 196, 3, '300', '300', '0', '900', '2019-11-14 02:46:01', '2019-11-14 02:46:01'),
(82, 5, 196, 3, '250', '250', '0', '750', '2019-11-14 02:46:01', '2019-11-14 02:46:01'),
(83, 5, 197, 1, '250', '250', '0', '250', '2019-11-14 02:49:10', '2019-11-14 02:49:10'),
(84, 1, 198, 1, '300', '300', '0', '300', '2019-11-14 02:54:08', '2019-11-14 02:54:08'),
(85, 5, 198, 1, '250', '250', '0', '250', '2019-11-14 02:54:08', '2019-11-14 02:54:08'),
(86, 1, 199, 1, '300', '300', '0', '300', '2019-11-14 03:59:59', '2019-11-14 03:59:59'),
(87, 2, 199, 1, '300', '300', '0', '300', '2019-11-14 03:59:59', '2019-11-14 03:59:59'),
(88, 5, 199, 1, '250', '250', '0', '250', '2019-11-14 03:59:59', '2019-11-14 03:59:59'),
(89, 1, 200, 3, '300', '300', '0', '900', '2019-11-14 04:02:01', '2019-11-14 04:02:01'),
(90, 1, 201, 2, '300', '300', '0', '600', '2019-11-14 05:16:53', '2019-11-14 05:16:53'),
(91, 2, 201, 2, '300', '300', '0', '600', '2019-11-14 05:16:53', '2019-11-14 05:16:53'),
(92, 5, 201, 2, '250', '250', '0', '500', '2019-11-14 05:16:53', '2019-11-14 05:16:53'),
(93, 1, 202, 1, '300', '300', '0', '300', '2019-11-14 06:27:40', '2019-11-14 06:27:40'),
(94, 2, 202, 1, '300', '300', '0', '300', '2019-11-14 06:27:40', '2019-11-14 06:27:40'),
(95, 5, 202, 1, '250', '250', '0', '250', '2019-11-14 06:27:40', '2019-11-14 06:27:40'),
(96, 5, 203, 1, '250', '250', '0', '250', '2019-11-15 01:21:54', '2019-11-15 01:21:54'),
(97, 5, 204, 1, '250', '250', '0', '250', '2019-11-15 02:10:08', '2019-11-15 02:10:08'),
(98, 1, 205, 1, '300', '300', '0', '300', '2019-11-15 02:15:39', '2019-11-15 02:15:39'),
(99, 1, 206, 1, '300', '300', '0', '300', '2019-11-15 02:21:21', '2019-11-15 02:21:21'),
(100, 1, 207, 1, '300', '300', '0', '300', '2019-11-15 02:21:55', '2019-11-15 02:21:55'),
(101, 1, 208, 1, '300', '300', '0', '300', '2019-11-15 02:37:21', '2019-11-15 02:37:21'),
(102, 2, 209, 1, '300', '300', '0', '300', '2019-11-15 02:51:03', '2019-11-15 02:51:03');

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(255) NOT NULL,
  `referral_bonus` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `conversion_point` int(11) NOT NULL,
  `charges_withdraw` int(11) NOT NULL,
  `point_voucher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `referral_bonus`, `commission`, `conversion_point`, `charges_withdraw`, `point_voucher`) VALUES
(1, 20, 20, 20, 10, 100);

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp(3) NOT NULL DEFAULT current_timestamp(3),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `register_downline_no` int(11) NOT NULL,
  `amount` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`, `register_downline_no`, `amount`) VALUES
(66, '3e072a0ea0c47d4429499685c4464afd', 'f66af1f432639aab4f1ff2f6daad96b3', 'messi', 1, '3e072a0ea0c47d4429499685c4464afd', '2019-07-01 03:18:02.000', '2019-11-13 09:22:34', 0, 0),
(68, 'ea74ab1650f585ff4b87b1177d0cd52f', '72bd9cc7f2fcff39d3774ae89118739c', 'ozil', 1, 'ea74ab1650f585ff4b87b1177d0cd52f', '2019-10-17 03:22:51.000', '2019-10-17 03:22:51', 0, 0),
(69, '72bd9cc7f2fcff39d3774ae89118739c', '59b012d375cafb3a7719d9d67e1f5a2c', 'lahm', 2, 'ea74ab1650f585ff4b87b1177d0cd52f', '2019-10-17 03:53:04.000', '2019-10-17 03:53:04', 0, 0),
(70, 'f66af1f432639aab4f1ff2f6daad96b3', '31749902888327c9815060fc08a347ad', 'ronaldo', 2, '3e072a0ea0c47d4429499685c4464afd', '2019-10-17 04:24:44.000', '2019-10-17 04:24:44', 0, 0),
(205, '31749902888327c9815060fc08a347ad', '77db4bbd658104a4df91db8bd8e052ba', 'test', 3, '3e072a0ea0c47d4429499685c4464afd', '2019-11-08 06:29:42.367', '2019-11-08 06:29:42', 0, 0),
(206, '31749902888327c9815060fc08a347ad', 'fc701905a110ceb14fa6c2e0298af389', 'testt', 3, '3e072a0ea0c47d4429499685c4464afd', '2019-11-08 06:31:58.905', '2019-11-08 06:31:58', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE `transaction_history` (
  `id` bigint(20) NOT NULL,
  `money_in` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'in points',
  `money_out` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'in points',
  `uid` varchar(255) NOT NULL,
  `target_uid` varchar(255) DEFAULT NULL COMMENT 'this is the uid for the targeted user. it can be a downline''s uid because upline can get commissions from his downline. therefore this target_uid shall be downline''s uid while the uid field above will be upline''s uid',
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'The percentage assigned to this transaction. Usually for commissions use only',
  `original_value` decimal(50,0) DEFAULT NULL COMMENT '(in point)the original value that has been given before calculating the percentage. Usually for commissions use only',
  `status` int(3) DEFAULT NULL COMMENT '1 = pending, 2 = accepted/completed, 3 = rejected, NULL = nothing',
  `level` int(10) DEFAULT NULL COMMENT 'can be group sales commission given by a downline at a level',
  `order_id` bigint(20) DEFAULT NULL COMMENT 'refers to a particular order in order table and it refers to another table that lists out all the products and quantity purchased in that table with order_id as foreign key',
  `transaction_type_id` int(255) NOT NULL,
  `money_type_id` int(255) DEFAULT NULL,
  `source_transaction_id` bigint(20) DEFAULT NULL COMMENT 'this is referring to this table''s own id. Might be needed when some previous transaction is triggering new transactions',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE `transaction_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'Percentage of the transaction, for example if transaction is withdrawal type, user need give 0.5% of total',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`id`, `name`, `description`, `percentage`, `date_created`, `date_updated`) VALUES
(1, 'referral', 'user can get commission by referring people and the referred person must at least buy a product for the referral to get this commission', NULL, '2019-07-25 08:46:07', '2019-07-25 08:46:07'),
(2, 'group_sales', 'This is when the user\'s downline bought a product, he will get a percentage from it. up to 9 levels with each level having different percentage. please refer to group_commission table', NULL, '2019-07-25 09:15:08', '2019-07-25 09:15:08'),
(3, 'buy_product', 'This is when user purchase a product', NULL, '2019-07-26 01:35:16', '2019-07-26 01:35:16'),
(4, 'transfer', 'point transfer between members', '0.25', '2019-07-31 02:46:22', '2019-07-31 02:46:22'),
(5, 'withdraw', 'withdraw money and transfer to bank', '0.50', '2019-07-31 02:46:22', '2019-07-31 02:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_point`
--

CREATE TABLE `transfer_point` (
  `id` int(11) NOT NULL,
  `send_uid` varchar(100) NOT NULL,
  `send_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `amount` int(11) NOT NULL,
  `receive_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `receive_uid` varchar(100) NOT NULL,
  `create_date` timestamp(3) NOT NULL DEFAULT current_timestamp(3),
  `status` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `uploadedimage`
--

CREATE TABLE `uploadedimage` (
  `id` int(11) NOT NULL,
  `imagename` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uploadedimage`
--

INSERT INTO `uploadedimage` (`id`, `imagename`) VALUES
(29, 0x3336352e6a7067);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `epin` char(64) DEFAULT NULL,
  `salt_epin` char(64) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `downline_accumulated_points` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'RM1 = 100 point',
  `can_send_newsletter` tinyint(1) NOT NULL DEFAULT 0,
  `is_referred` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `car_model` varchar(200) DEFAULT NULL,
  `car_year` int(20) DEFAULT NULL,
  `picture_id` int(12) DEFAULT NULL,
  `register_downline_no` int(255) NOT NULL,
  `bonus` int(255) NOT NULL,
  `final_amount` int(255) NOT NULL DEFAULT 0,
  `point` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `country_id`, `full_name`, `epin`, `salt_epin`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `login_type`, `user_type`, `downline_accumulated_points`, `can_send_newsletter`, `is_referred`, `date_created`, `date_updated`, `address`, `birthday`, `gender`, `bank_name`, `bank_account_holder`, `bank_account_no`, `car_model`, `car_year`, `picture_id`, `register_downline_no`, `bonus`, `final_amount`, `point`) VALUES
('31749902888327c9815060fc08a347ad', 'ronaldo', 'r@g.com', '5bfa998f597e8ab292c1ce4202e51a489d2053062cef06609dbb7da22ab703c0', '98b6139b8de6adc2e2779fc2b996ec9f16d93404', NULL, '5678', NULL, NULL, 'aa7b494493202fa0ea4ddd44a09ab7987b0bf3db3fbe95be26d35583dad6bf7a', '9ba703f2e9edadff69075d998af44dd9a7645455', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-17 04:24:44', '2019-11-08 06:31:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 350, 350, 0),
('3e072a0ea0c47d4429499685c4464afd', 'atlet008', 'a@g.com', 'ea973dac74dd81228a2294b9673da8ac0057a5b98db41b36c940bafc59a18918', '3deebb50bf38922d0f7af5ffe8fb8ad689bc4fb4', '65432', '232132', NULL, 'Severus', 'b9064036f38e55d4987686b6a1f4a4f46801170aec98ac40d2dd347198ce5f88', '68a93002f267fd701167fd63bdb85277a113f3cf', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-16 16:00:00', '2019-11-14 01:36:46', '25 kg tok khamis, west, west, west, west, west, west, west', '2019-10-20', 'Female', 'AFFIN BANK BERHAD', 'Severus', 234567, 'asfd', 2134, 130, 6, 150, 10, 0),
('72bd9cc7f2fcff39d3774ae89118739c', 'ozil', 'o@g.com', '91be04ea7f3ad57e85909b22e27fbc4c98537c4abd80a115ab900190d9a7fd39', 'f80e7b4d316e19f5d2ba9d55fe40734432b4ddb7', NULL, '999', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-17 03:22:51', '2019-11-08 01:19:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
('77db4bbd658104a4df91db8bd8e052ba', 'test', NULL, '1aa74d2c698822adda5846a3128334ce0e023a11760e79cee5d48c1e3842ddd6', '6ce34b103fec1a21da4439c0ca05738ec3716972', NULL, '123321', NULL, 'testtest', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-08 06:29:42', '2019-11-08 06:29:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
('ea74ab1650f585ff4b87b1177d0cd52f', 'Jones', 'j@g.com', '268c2e6705066216663b029163dec21d57d3542fb82c36711c5a8edd39760729', '61584db4cdfe6f8cdb7a2071eaff76d011909b0b', NULL, '4343', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, '0', 0, 1, '2019-10-16 01:23:45', '2019-10-18 09:46:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 0, 650, 0),
('f66af1f432639aab4f1ff2f6daad96b3', 'messi', 'mm@g.com', '1b1467e7bdd2a9a973c332e8e0045439cdbcd45ea382d06c81d141cde4750cd3', '40d690a9acaaf784efa4a897f66852d5a1e102f0', '5432', '1234', NULL, 'Lional Messi', 'eda6e6a70533747c07f66f2ee93f3795fc1116bb889eac62f12a793f11a8852d', '0df889da63163ffb5b39de9eedc06a9d851dd8c3', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-10-17 03:18:01', '2019-11-08 08:13:26', NULL, NULL, 'Female', 'ALLIANCE BANK MALAYSIA BERHAD', 'Lional Messi', 2147483647, NULL, NULL, NULL, 2, 300, -50, 994),
('fc701905a110ceb14fa6c2e0298af389', 'testt', NULL, '23c41795509891573f2db60fd9e730cf18bad5fe0d6d74ad4ed9b1be8900f94d', '96ec88e7a8f5bb4ecc84862307897a7289016b9b', NULL, '1233321', NULL, 'testttestt', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-11-08 06:31:58', '2019-11-08 06:31:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `uid` varchar(255) CHARACTER SET latin1 NOT NULL,
  `withdrawal_number` int(255) NOT NULL,
  `withdrawal_status` text CHARACTER SET latin1 NOT NULL,
  `contact` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `amount` int(255) NOT NULL,
  `final_amount` int(255) NOT NULL,
  `withdrawal_method` varchar(255) CHARACTER SET latin1 NOT NULL,
  `withdrawal_amount` int(255) DEFAULT NULL,
  `withdrawal_note` varchar(255) CHARACTER SET latin1 NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `bank_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `acc_number` int(255) NOT NULL,
  `point` int(255) NOT NULL,
  `owner` varchar(255) CHARACTER SET latin1 NOT NULL,
  `receipt` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(11) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `withdrawal`
--

INSERT INTO `withdrawal` (`uid`, `withdrawal_number`, `withdrawal_status`, `contact`, `date_created`, `date_updated`, `amount`, `final_amount`, `withdrawal_method`, `withdrawal_amount`, `withdrawal_note`, `username`, `bank_name`, `acc_number`, `point`, `owner`, `receipt`, `name`) VALUES
('3e072a0ea0c47d4429499685c4464afd', 173, 'PENDING', 65432, '2019-11-08 07:56:12', '2019-11-08 07:56:12', 90, 150, '', NULL, '', '', 'CIMB BANK BERHAD', 234567, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 174, 'PENDING', 65432, '2019-11-08 07:59:08', '2019-11-08 07:59:08', 10, 50, '', NULL, '', '', 'CIMB BANK BERHAD', 234567, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 175, 'PENDING', 65432, '2019-11-08 07:59:22', '2019-11-08 07:59:22', 10, 30, '', NULL, '', '', 'CIMB BANK BERHAD', 234567, 0, 'atlet008', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 176, 'PENDING', 5432, '2019-11-08 08:00:49', '2019-11-08 08:00:49', 90, 1000, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 177, 'PENDING', 5432, '2019-11-08 08:00:57', '2019-11-08 08:00:57', 90, 900, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 178, 'PENDING', 5432, '2019-11-08 08:01:03', '2019-11-08 08:01:03', 90, 800, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 179, 'PENDING', 5432, '2019-11-08 08:01:11', '2019-11-08 08:01:11', 40, 700, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 180, 'ACCEPTED', 5432, '2019-11-08 08:01:17', '2019-11-08 08:55:11', 40, 650, 'iPay88', 40, 'PLSS', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 181, 'PENDING', 5432, '2019-11-08 08:01:24', '2019-11-08 08:01:24', 40, 600, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 182, 'PENDING', 5432, '2019-11-08 08:01:24', '2019-11-08 08:01:24', 40, 550, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 183, 'PENDING', 5432, '2019-11-08 08:01:35', '2019-11-08 08:01:35', 40, 500, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 184, 'PENDING', 5432, '2019-11-08 08:01:47', '2019-11-08 08:01:47', 40, 450, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 185, 'PENDING', 5432, '2019-11-08 08:02:01', '2019-11-08 08:02:01', 40, 400, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 186, 'PENDING', 5432, '2019-11-08 08:02:07', '2019-11-08 08:02:07', 40, 350, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 187, 'PENDING', 5432, '2019-11-08 08:02:15', '2019-11-08 08:02:15', 90, 300, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 188, 'PENDING', 5432, '2019-11-08 08:02:22', '2019-11-08 08:02:22', 40, 200, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 189, 'ACCEPTED', 5432, '2019-11-08 08:02:29', '2019-11-08 08:02:29', 40, -50, 'iPay88', 40, 'HAVE A GOOD DAYS FELLA', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, ''),
('f66af1f432639aab4f1ff2f6daad96b3', 190, 'PENDING', 5432, '2019-11-08 08:02:36', '2019-11-08 08:02:36', 90, 100, '', NULL, '', 'Lional Messi', 'ALLIANCE BANK MALAYSIA BERHAD', 2147483647, 0, 'messi', NULL, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`announce_id`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_to_point`
--
ALTER TABLE `cash_to_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactionTypeIdGroupCommission_relateTo_transactionTypeId` (`transaction_type_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `money_type`
--
ALTER TABLE `money_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payout_history`
--
ALTER TABLE `payout_history`
  ADD PRIMARY KEY (`ph_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productIdTransactionHistory_relateTo_productId` (`product_id`),
  ADD KEY `orderIdProductOrders_relateTo_orderId` (`order_id`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdReferralHistory_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdReferralHistory_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdReferralHistory_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uidTransactionHistory_relateTo_userId` (`uid`),
  ADD KEY `targetUidTransactionHistory_relateTo_userId` (`target_uid`),
  ADD KEY `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` (`transaction_type_id`),
  ADD KEY `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` (`money_type_id`),
  ADD KEY `sourceTransactionIdTransactionHistory_relateTo_self` (`source_transaction_id`),
  ADD KEY `orderIdTransactionHistory_relateTo_orderId` (`order_id`);

--
-- Indexes for table `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_point`
--
ALTER TABLE `transfer_point`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Sff` (`send_uid`),
  ADD KEY `ssd` (`receive_uid`);

--
-- Indexes for table `uploadedimage`
--
ALTER TABLE `uploadedimage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `countryIdUser_relateTo_countryId` (`country_id`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`withdrawal_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `announce_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `cash_to_point`
--
ALTER TABLE `cash_to_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `group_commission`
--
ALTER TABLE `group_commission`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `pid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `money_type`
--
ALTER TABLE `money_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;

--
-- AUTO_INCREMENT for table `payout_history`
--
ALTER TABLE `payout_history`
  MODIFY `ph_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9223372036854775807;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT for table `transaction_history`
--
ALTER TABLE `transaction_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction_type`
--
ALTER TABLE `transaction_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transfer_point`
--
ALTER TABLE `transfer_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `uploadedimage`
--
ALTER TABLE `uploadedimage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `withdrawal_number` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD CONSTRAINT `transactionTypeIdGroupCommission_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD CONSTRAINT `orderIdProductOrders_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `productIdTransactionHistory_relateTo_productId` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD CONSTRAINT `referralIdReferralHistory_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD CONSTRAINT `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` FOREIGN KEY (`money_type_id`) REFERENCES `money_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orderIdTransactionHistory_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sourceTransactionIdTransactionHistory_relateTo_self` FOREIGN KEY (`source_transaction_id`) REFERENCES `transaction_history` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `targetUidTransactionHistory_relateTo_userId` FOREIGN KEY (`target_uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `uidTransactionHistory_relateTo_userId` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `transfer_point`
--
ALTER TABLE `transfer_point`
  ADD CONSTRAINT `Sff` FOREIGN KEY (`send_uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ssd` FOREIGN KEY (`receive_uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `countryIdUser_relateTo_countryId` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
