-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2019 at 05:22 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_oilxag`
--

-- --------------------------------------------------------

--
-- Table structure for table `signup_product`
--

CREATE TABLE `signup_product` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referrer_name` varchar(255) DEFAULT NULL,
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `referral_fullname` varchar(255) NOT NULL,
  `product` varchar(255) NOT NULL,
  `price` int(255) NOT NULL DEFAULT 300,
  `quantity` int(255) NOT NULL DEFAULT 1,
  `total` int(255) NOT NULL DEFAULT 300,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `signup_product`
--

INSERT INTO `signup_product` (`id`, `referrer_id`, `referrer_name`, `referral_id`, `referral_name`, `referral_fullname`, `product`, `price`, `quantity`, `total`, `date_created`, `date_updated`) VALUES
(26, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '40b580f0e5493b63a674870e8e3d288a', 'varane', 'Raphel Varane', 'Oil Booster 3pcs (1 set) RM300', 300, 1, 300, '2019-11-27 02:50:47', '2019-11-27 02:50:47'),
(27, '40b580f0e5493b63a674870e8e3d288a', 'varane', 'febe2222b0bc5b16871fe827112e4dbe', 'kroos', 'Toni Kroos', 'Motorcycle Oil Booster 15pcs (1 set) RM300', 300, 1, 300, '2019-11-27 02:52:39', '2019-11-27 02:52:39'),
(28, '40b580f0e5493b63a674870e8e3d288a', 'varane', '9118348d64a9a7b4189b2d850b5669bb', 'hazard', 'Eden Hazard', 'Synthetic Plus Engine Oil 5w-30 (1 set) RM300', 300, 1, 300, '2019-11-27 02:54:09', '2019-11-27 02:54:09'),
(29, '40b580f0e5493b63a674870e8e3d288a', 'varane', '437c3d453283c501db559d2268c8b399', 'benzema', 'Karim Benzema', 'Synthetic Plus Engine Oil 10w-40  (1 set) RM300', 300, 1, 300, '2019-11-27 02:54:39', '2019-11-27 02:54:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `signup_product`
--
ALTER TABLE `signup_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referrerIdSignUpProduct_To_userId` (`referrer_id`),
  ADD KEY `referralIdSignUpProduct_To_userId` (`referral_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `signup_product`
--
ALTER TABLE `signup_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `signup_product`
--
ALTER TABLE `signup_product`
  ADD CONSTRAINT `referralIdSignUpProduct_To_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdSignUpProduct_To_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
